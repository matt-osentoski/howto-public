# Personal lab using an Intel NUC Cluster
This document contains instructions for creating a cluster using Intel NUCs as servers.  
Author: Matt Osentoski  
E-mail: matt.osentoski@gmail.com

## Hardware requirements
>(NOTE: The part number information below will become obsolete quickly.
>You should investigate the different Nuc model numbers, looking for the features
>and specifications for your specific cluster. For example, # of cores, max memory, # of hard drives, etc.)

- 1+ Intel NUC servers.  (I'm using a combination of NUC7PJYH1 and NUC8i5BEH servers)
- Memory.  I use 8GB and 32GB per server (The max allowed).  
>(NOTE: Be sure to use the right number of memory sticks for
your particular NUC server.  For example, the NUC7PJYH1 is dual channel and requires 2 DIMMs.)

- SSD Hard drives.  I'm using SanDisk SSD PLUS 120GB Solid State Drive - SDSSDA-120G-G27 for each NUC. 
This should provide a combination of good speed and capacity at a low price point.
- Gigabit Ethernet Switch.  Any inexpensive Gigabit Ethernet switch will work.  Just make sure to get one
with enough ports for the number of servers you plan to use.  
- Ethernet cables.  Buy enough cables for all your servers.  I chose very short cables for my install (~12" cables)

## Internet Routing considerations
Assuming your NUC cluster lives on its own private network (this is how I'm running it) the servers will need access to 
the internet for software downloads, updates, etc. To set this up, you will have to point the 'default gateway' of each
NUC device to a router that can route packets to the Internet.  (Ex: Your Internet router address provided by your ISP, etc)
  
Another option is to setup one of your NUC servers as a gateway server.  This isn't required, but outlined in the
document called `optional-nuc-gateway/ubuntu-gateway-setup.md`

>(NOTE: For a less expensive option, you can also use a Raspberry PI instead of a Nuc. Check
out the instructions at [optional-nuc-gateway/raspberryPI-gateway-setup.md](optional-nuc-gateway/raspberryPI-gateway-setup.md))

## Setup instructions
1. `/nuc-cluster-setup/nuc-hardware.md` - This file explains the hardware setup of the NUC, including preparing Ubuntu
for install
2. `/nuc-cluster-setup/ubuntu-image-install.md` Describes the process of creating an Ubuntu boot image onto a USB drive and install.
3. `/nuc-cluster-setup/ubuntu-setup.md` - This file describes the install process for ubuntu.
4. (OPTIONAL) `/nuc-cluster-setup/optional-nuc-gateway/ubuntu-gateway-setup.md` - Instructions for setting up an Internet gateway router
using a Raspberry Pi or Nuc. This will allow your Nuc cluster to live on its own private network
separate from your home WIFI network, but still access the Internet.

