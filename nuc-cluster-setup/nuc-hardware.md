# NUC Hardware setup
This document describes the process for setting up your NUC. It assumes you purchased the NUC kit (without memory
or hard drive included).  

Author: Matt Osentoski  
E-mail: matt.osentoski@gmail.com

## Hardware setup
1. Unscrew the four phillips-head screws at the bottom of the NUC. 
2. The bottom plate is where you will install your SSD Hard Drive.  Simply slide the drive in so that
it meets the SATA connectors. I'm using SanDisk SSD PLUS 120GB Solid State Drive - SDSSDA-120G-G27 for each NUC. 
3. Install your memory sticks.  **NOTE: Be sure to use the right number of memory sticks for
your particualr NUC server.  For example, the NUC7PJYH1 is dual channel and requires 2 DIMMs. **
4. Put the NUC back together and tighten the four screws from step 1.

With your NUC assembled, plug in your power supply, keyboard, mouse, and HDMI monitor. At this point
you can go to the `/nuc-cluster-setup/ubuntu-image-install.md` document to continue.
