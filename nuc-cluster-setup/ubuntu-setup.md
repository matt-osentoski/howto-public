# Ubuntu setup after installation
This document walks through the steps to setup your Ubuntu Operating System (OS) settings, 
now that the OS has been installed.  

Author: Matt Osentoski  
E-mail: matt.osentoski@gmail.com

Login to the server to get started.  The username should be 'hduser' and the password you configured when setting
up Ubuntu.  The following steps assume you're already logged in and at a terminal prompt. 

### Install SSH Server
>(NOTE: If you installed SSH server during the installation of your server, you can skip this step)

```
sudo apt-get update
sudo apt-get install openssh-server
```
At this point, you should be able to SSH into the NUC server and run it headless.

### Configure SSH
```
cd ~
mkdir ~/.ssh
ssh-keygen -t rsa -P ""
cat ~/.ssh/id_rsa.pub > ~/.ssh/authorized_keys
```
  
>(NOTE: If you can't ssh to a remote machine without being prompted for a password,
copy /home/hduser/.ssh/id_rsa.pub on node1 to 
/home/hduser/.ssh/authorized_keys on the node that you try to connect to.)

### (Optional) Fixing the default LVM partitions
> (NOTE: This step only applies if you installed Ubuntu with the 'Use An Entire Disk And Set Up LVM' partition
option)

> (WARNING!!!: You should NOT be using LVM partitions if you plan to install Rook/Ceph on the cluster. The Ceph
OSD's will not be created, since they require a raw device and can't use an LVM partition. If you used LVM
to partition the disk, reinstall using the default partitioner)

There seems to be a bug where the default LVM partition settings in Ubuntu aren't using the full drive space
to verify this, run the following command:

```
sudo lsblk -o NAME,FSTYPE,SIZE,MOUNTPOINT,LABEL

NAME                      FSTYPE        SIZE MOUNTPOINT      LABEL
loop0                     squashfs       91M /snap/core/6350
loop1                     squashfs     89.1M /snap/core/8268
sda                                   111.8G
├─sda1                    vfat          512M /boot/efi
├─sda2                    ext4            1G /boot
└─sda3                    LVM2_member 110.3G
  └─ubuntu--vg-ubuntu--lv ext4            4G /

```

You can see above, only 4GB is actually allocated to the root (/) mount point, even though
110GB is available.  To fix this issue, run the following commands on all nodes with this issue:

```
sudo lvm
lvm> lvextend -l +80%FREE /dev/ubuntu-vg/ubuntu-lv
lvm> exit

sudo resize2fs /dev/ubuntu-vg/ubuntu-lv
```

To verify that you now have sufficient disk space, run the 'lsblk' command again or:
```
df -h
```

### (OPTIONAL BUT RECOMMENDED) Install the latest Ubuntu security updates
If you skipped the security update portion of the installation, you can install the latest
security updates using the following commanded:

```shell 
sudo apt-get dist-upgrade
```

### (OPTIONAL) Install Open Java SDK 1.8
```
sudo apt-get update
sudo apt-get install openjdk-8-jre-headless
```
To change the default Java JDK, use the 'alternatives' tool:
```
sudo update-alternatives --config java
```

### (OPTIONAL) Set the JAVA_HOME environment variable
```
sudo vi /etc/bash.bashrc
```
Add the following lines:  
```
JAVA_HOME=/usr/lib/jvm/java-1.8.0-openjdk-amd64
export JAVA_HOME
```

### (OPTIONAL) Install Python 3 and pip
```
sudo apt-get update
sudo apt-get install python3
sudo apt-get install python3-pip
```
 
Your server should now be setup and ready to install Hadoop. At this point you can go to 
the `required/hadoop-install.md` document to continue. 

## Troubleshooting

### Ethernet interface isn't showing up
Perform the following commands to see if your network card is working:
```
ifconfig -a

sudo lshw -C network
```

If you don't see an ethernet port listed and the ethernet lights are not 
on, on the back of the NUC, try unplugging the NUC and plugging it back it.

For some reason, performing a soft reboot won't bring the network interface
back online.  You have to actually unplug the device.  It may also be necessary to change ports
on your hub or switch.  My guess is a BIOS update might fix the issue.