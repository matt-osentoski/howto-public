# Setup a NUC server as an Internet Gateway
The NUC servers will require Internet access for software updates, etc.  This document explains how to
configure one of the servers as an internet gateway.
>(NOTE: The NUC must have both a WIFI and ethernet
interface for this to work)

Author: Matt Osentoski  
E-mail: matt.osentoski@gmail.com

### Turn on wifi
`sudo vi /etc/wpa_supplicant/wpa_supplicant.conf`  
Add the following lines to the file:  
```
ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
update_config=1

network={
        ssid="ROUTER-SSID-GOES-HERE"
        psk="WIFI-PASSWORD-HERE"
}
```
reboot the server for the changes to take effect

### Setup the interfaces
`sudo vi /etc/network/interfaces`  
The file should have the following lines and assumes this NUC server has an IP address of 192.168.6.1:  
```
auto lo
iface lo inet loopback

auto eth0
allow-hotplug eth0
iface eth0 inet static
address 192.168.6.1
netmask 255.255.255.0

auto wlan0
allow-hotplug wlan0
iface wlan0 inet manual
wpa-conf /etc/wpa_supplicant/wpa_supplicant.conf
```

### Turn on IP pass-through
```
sudo vi /etc/sysctl.conf
```
Uncomment the following line:  
`net.ipv4.ip_forward = 1`  
Restart the pass-through using the folowing command:  
```
sudo sysctl -p /etc/sysctl.conf
```

### Setup NAT
```
sudo iptables -t nat -A POSTROUTING -j MASQUERADE
```
Save the changes  
```
sudo /sbin/iptables-save
sudo apt-get install iptables-persistent
```

At this point, your NUC server should be acting as a router.  You can configure your other NUCs
to use this server's IP address as their default gateway.