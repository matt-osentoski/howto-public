# This document contains a list of common Kubernetes kubectl commands:

## Common Commands
Show kubelet logs to troubleshoot problems
```
journalctl -xeu kubelet
```
Show all nodes
```
kubectl get nodes
```
Show all nodes and IP addresses:
```
kubectl get po --all-namespaces -o wide
```
Show all pods for all namespaces:
```
kubectl get pods --all-namespaces
```
Show all services
```
kubectl get services --all-namespaces
```
Show all namespaces
```
kubectl get namespaces
```
Show all events for the cluster
```
kubectl get events
```
Restart all pods in a namespace
```
kubectl rollout restart deploy -n <namespace>
```
Restart pods from a StatefulSet
```
kubectl rollout restart statefulset <statefulset name> --namespace <namespace>
```
Show pod logs
```
kubectl logs -n kube-system <pod name>
```
Show pod logs (tail follow)
```
kubectl logs -f <pod name>
```
Show all pods logs by pod label
```
kubectl logs -f -l app=nginx-ingress --all-containers --max-log-requests=6 --namespace default --since=5m
```
Show all deployments
```
kubectl get deployment --all-namespaces
```
View all Docker image versions in a namespace (in this example the 'rook-ceph' namespace)
```
kubectl -n rook-ceph describe pods | grep "Image:.*" | sort | uniq
```
Remove the 'claimRef' section in a PersistentVolume after changing the state from 'Released' to 'Available'.  This will make the PV available to
use again:
```
kubectl patch PersistentVolume my-pv-name --type json -p='[{"op": "remove", "path": "/spec/claimRef"}]'
```

Show resources used by a namespace.  This is useful if a namespace
becomes stuck in `Terminating` during delete:
```
kubectl api-resources --verbs=list --namespaced -o name | xargs -n 1 kubectl get -n myNamespace
```

Show all resources used by namespace, with their location (better output than the above command)
```
kubectl api-resources --verbs=list --namespaced -o name | xargs -n 1 kubectl get --show-kind --ignore-not-found -n myNamespace
```

Mark a storageClass as default
```
kubectl patch storageclass myStorageClassName -p '{"metadata": {"annotations":{"storageclass.kubernetes.io/is-default-class":"true"}}}'
```

Mark a storageClass as Non-default
``` 
kubectl patch storageclass myStorageClassName -p '{"metadata": {"annotations":{"storageclass.kubernetes.io/is-default-class":"false"}}}'
```

### K8s local DNS
References:
- [https://kubernetes.io/docs/tasks/run-application/access-api-from-pod](https://kubernetes.io/docs/tasks/run-application/access-api-from-pod)
- [https://kubernetes.io/docs/concepts/services-networking/dns-pod-service/](https://kubernetes.io/docs/concepts/services-networking/dns-pod-service/)

```
## Cluster API name
kubernetes.default.svc
-or-
kubernetes.default.svc.cluster.local

## Service Name
<SERVICE_NAME>.<NAMESPACE>.svc.cluster.local

# For example `my-service.example-ns.svc.cluster.local`

## Pod Name
<IP_ADDRESS_WITH_DASHES>.<NAMESPACE>.pod.cluster.local

# For example `172-16-1-5.example-ns.pod.cluster.local`
# (NOTE: That the IP address of the pod in this case is 172.16.1.6, but we use dashes for the name)
```

### Map a pod port to a port on your local machine using a secure tunnel
```
kubectl -n myNamespace port-forward myPodName 8080:8080
# (NOTE: In this example 'myNamespace' is the name of the naamespace of the pod. 'myPodName' would be the name of the
# kubernetes pod.  The first port number is the exposed node port and the second port number will be mapped to your PC)
```

### Expose a pod using the IP address of master server
```
kubectl expose deployment/<NAME_OF_THE_POD> --external-ip=<IP_ADDRESS_OF_THE_SERVER>
# For example, the application name in the pod manifest is 'k8s-example-ws'
kubectl expose deployment/k8s-example-ws --external-ip=192.168.6.2
```

### Delete a pod
```
kubectl delete pod <pod name>
# example
kubectl delete pod nginx
```
-OR-
To delete a pod use 'delete' instead of 'create' with the file used to create the pod. For example:
```
kubectl delete -f https://raw.githubusercontent.com/kubernetes/dashboard/master/src/deploy/recommended/kubernetes-dashboard.yaml
```
You can then recreate the pod using the 'create' command:
```
kubectl create -f https://raw.githubusercontent.com/kubernetes/dashboard/master/src/deploy/recommended/kubernetes-dashboard.yaml
```
-OR- IF the pod isn't a part of a deployment (see below) and refused to delete, try removing the replica set
```
# First find the replica set name
kubectl get deploy,rc,rs

# Next, delete the replica set
kubectl delete <REPLICA_SET_NAME>
## For example:
kubectl delete replicaset.extensions/k8s-example-ws-7d8578dbb4
```
### Delete all pods and services in a namespace
kubectl -n my-namespace delete pod,svc --all

### Delete a deployment
Deleting a pod will cause it to restart.  If you want the app removed permanently, delete the deployment instead:
```
# First, find the name/namespace of the deployment
kubectl get deployments --all-namespaces

# Next delete the deployments
kubectl delete -n <NAMESPACE> deployment <DEPLOYMENT>
## For example:
kubectl delete -n default deployment k8s-example-ws
```

### Scale a Daemonset down to zero then bring it back up
Reference: [https://stackoverflow.com/a/57533340](https://stackoverflow.com/a/57533340)

```shell
## Scale a daemonset down to zero
kubectl -n <NAMESPACE_NAME> patch daemonset <DAEMON_SET_NAME> -p '{"spec": {"template": {"spec": {"nodeSelector": {"non-existing": "true"}}}}}'
## For example:
 kubectl -n example patch daemonset fluentd-elasticsearch -p '{"spec": {"template": {"spec": {"nodeSelector": {"non-existing": "true"}}}}}'

## Bring it backonline
kubectl -n <NAMESPACE_NAME> patch daemonset <DAEMON_SET_NAME> --type json -p='[{"op": "remove", "path": "/spec/template/spec/nodeSelector/non-existing"}]'
## For example:
 kubectl -n example patch daemonset fluentd-elasticsearch --type json -p='[{"op": "remove", "path": "/spec/template/spec/nodeSelector/non-existing"}]'
```

## To shutdown a node then bring it back up
```
# To release the node's resources and remove it from scheduling
kubectl drain <node name>

# To rejoin the node to the scheduler
kubectl uncordon <node name>
```

## Use an init container to wait for one pod before launching another
For example, if you have an application that is dependant on Mysql, but want to 
wait until mysql is running before starting the pod
```
## Put this init container in your Deployment / Statefulset, etc.

### Example 1: Wait for a port

initContainers:
- name: init-waitfor-mysql
  image: busybox:1.28
  command: ['sh', '-c', 'until nc -vz {{ .Values.waitforit.host }} {{ .Values.waitforit.port }}; do echo "Waiting for Mysql service"; sleep 2; done;']

## In this case, it's assumed you're using Helm where the values.yaml would contain something like:
waitforit:
  host: "mysql-binlog.mysql.svc.cluster.local"
  port: "3306"

### Example 2: Sleep for a period of time (60 seconds in this case)
initContainers:
- name: init-waitfor-delay
  image: busybox:1.28
  command: ['sh', '-c', "sleep 60;"]
```

## Switch kubectl to use a different Kubernetes cluster
Ref: https://stackoverflow.com/questions/36306904/configure-kubectl-command-to-access-remote-kubernetes-cluster-on-azure

If you are on your local PC using Kubernetes for docker and want to connect to a remote Kubernetes cluster, you can switch
over by performing the following steps:

- Locate your `~/.kube/config` file
- Make the following updates to the config file:

>(NOTE: In my case it was easier to just copy the `config` file over from my remote cluster, then
modify it to include the local cluster for kubernetes running in docker, for my PC):

```yaml
apiVersion: v1
clusters:
- cluster:
    server: http://<master-ip>:<port>
  name: remote-cluster
contexts:
- context:
    cluster: remote-cluster
    user: remote-cluster
  name: remote-cluster
```
- Run the following command

>(NOTE: swap out 'remote-cluster' for the cluster name):

```
kubectl config use-context remote-cluster
```

## Switch kubectl to use a different Kubernetes cluster without changing a config file
Run the following commands to quickly switch kubernetes clusters:
```
### The easiest way is to just change the `KUBECONFIG` environment variable
export KUBECONFIG=/home/someuser/my-custom-kubeconfig

## - OR -
### You can specify the cluster using the `kubectl` command
kubectl config set-cluster remote-cluster --server=http://<master-ip>:<port> --api-version=v1
kubectl config use-context remote-cluster
```

### Extra Configmap data contents that contains newlines
Sometimes a configmap like coredns can contain newlines in the yaml manifest making it difficult to update
using the `kubectl` 'edit' command. Run the following commands to get a more readable output and/or redirect into a file:
```shell
kubectl get configmap coredns --namespace kube-system -o json | jq -r .data.Corefile 
# Redirect output to a file:
kubectl get configmap coredns --namespace kube-system -o json | jq -r .data.Corefile > coredns_output.json

### Another option using GoLang templates as the output type
kubectl get configmap coredns --namespace kube-system  -o go-template={{.data.Corefile}}
# Redirect output to a file:
kubectl get configmap coredns --namespace kube-system  -o go-template={{.data.Corefile}} > coredns_output.json
```

# Troubleshooting

## Delete a Namespace (force)
Sometimes a namespace gets stuck and will not delete.  If this happens you might have to perform the following operations
to force the delete:

- Verify that all objects associated with the namespace are deleted:
```
kubectl get -n some-example-ns all

## Or 
kubectl api-resources --verbs=list --namespaced -o name | xargs -n 1 kubectl get --show-kind --ignore-not-found -n some-example-ns

## Or

kubectl api-resources --verbs=list --namespaced -o name | xargs -n 1 kubectl get -n some-example-ns
```

### Deleting Custom resource Objects that are stuck
Sometimes CRD Objects can get stuck.  First to find objects still running from a namespace run this
command:

``` 
kubectl api-resources --verbs=list --namespaced -o name | xargs -n 1 kubectl get --show-kind --ignore-not-found -n some-example-ns
```

Typically the `Finalizer` metadata block element is stuck.  You need to clear it out to an empty array `[]`. This
can be a challenge with CRD's because you may not know what `Kind` or `Type` the Object is. To view all `names` and `shortnames`
for all objects, run this command:

``` 
kubectl api-resources
```
Find the `name` or `shortname` for the object you want to remove. Then run this command:

```
kubectl patch kingress helloworld-go --namespace kn-apps -p '{"metadata":{"finalizers":null}}' --type=merge

## Example where the name is 'kingress' (A KNavite Ingress object) running in the 'kn-apps' namespace
kubectl patch kingress helloworld-go --namespace kn-apps -p '{"metadata":{"finalizers":null}}' --type=merge

## Then
kbuectl delete kingress helloworld-go --namespace kn-apps
```

### Create the namespace manifest without the 'finalizers' block set
This should produce the json file of the namespace with the finalizers block as an empty array '[]'
```
kubectl get ns some-example-ns -o json | jq '.spec.finalizers=[]' > ns-without-finalizers.json
cat ns-without-finalizers.json
```

### Apply the namespace file via kubectl proxy
This should be performed on one of the Kubernetes nodes with the kubectl command availabe.
```
kubectl proxy &
PID=$!
curl -X PUT http://localhost:8001/api/v1/namespaces/some-example-ns/finalize -H "Content-Type: application/json" --data-binary @ns-without-finalizers.json
kill $PID
```

### Delete the namespace
At this point, you can delete the namespace:

>(NOTE: The namespace may have already deleted itself via the last step)

```
kubectl delete namespace some-example-ns
```

### Service discovery / DNS issues
If you run into issues where internal DNS is not resolving, or the DNS is working
only 15% of the time, check the nodes for the following settings:
Update `/etc/sysctl.conf`

Change value from ‘0’ to ‘1’
```
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
net.bridge.bridge-nf-call-arptables = 1
```

### Adding static DNS entries to k8s
Pod may not be able to resolve DNS entries that exists in the /etc/hosts file
of each node.  This can happen if you're running an installation that doesn't
use a DNS server, but instead relies on static /etc/hosts file entries.  To
get around this problem, you can add static entries to the ConfigMap of CoreDNS
in k8s.

Ref: [https://hjrocha.medium.com/add-a-custom-host-to-kubernetes-a06472cedccb](https://hjrocha.medium.com/add-a-custom-host-to-kubernetes-a06472cedccb)

First, Modify the configmap.  In this case, we will be adding a static entry for
`192.168.6.2   docker.jaroof.local`

``` 
data: 
  Corefile: |
      .:53 {
          errors
          health
          ready
          kubernetes cluster.local in-addr.arpa ip6.arpa {
            pods insecure
            fallthrough in-addr.arpa ip6.arpa
          }
          prometheus :9153
          forward . 8.8.8.8 8.8.4.4
          cache 30
          loop
          reload
          loadbalance
          ### The following 4 lines are what will be updated!! ####
          hosts custom.hosts docker.jaroof.local {
            192.168.6.2 docker.jaroof.local
            fallthrough
          }
       }
       ....### more configs
```

Once this is complete, restart all the coredns pods in the `kube-system` namespace.
Your pods should now have access to the static DNS entry!

You can add multiple DNS/IP entries like this:
>(IMPORTANT!!!!! Make sure to add the domain name also on the line starting with `hosts custom...`)
``` 
hosts custom.hosts docker.jaroof.local other.jaroof.local {
    192.168.6.2 docker.jaroof.local
    192.168.6.5 other.jaroof.local
    fallthrough
}
```

## Expired node certs!  When trying to uncordon the nodes, you get an error message. 
When trying to uncordon the nodes, you get the following error even though all nodes
are online and you can access the port using netcat:

``` 
The connection to the server 192.168.6.2:6443 was refused - did you specify the right host or port?
```

Most likely the certs are expired for the nodes. Run the following commands
to check the cert status:

```shell 
### Sudo as the root user
sudo su -
kubeadm certs check-expiration
```

To renew all certs run the following command:

```shell 
### Make sure you are still Sudo'd as root
kubeadm certs renew all
```

Then recheck to verify that the certs have been updated:
```shell 
### Make sure you are still Sudo'd as root
kubeadm certs check-expiration
```

Update the API server kube config certificates
```shell
### Make sure you are still Sudo'd as root
##
## NOTE: Change the IP address below to the address of your api server address
kubeadm init phase certs all --apiserver-advertise-address 192.168.6.2

cp /etc/kubernetes/admin.conf /home/hduser/.kube/config

chown hduser /home/hduser/.kube/config
```

Finally, restart all the nodes.

## Remove and re-add a node
If your Node is having a lot of trouble going into a Ready state or something
has been corrupted (certs, etc.) You can remove and re-add the Node to the
cluster using the following commands:

```shell 
## First, remove the node from the cluster:
kubectl delete node <NODE_NAME>

## From the master node (NOTE!!! Copy the 'join' commadn that is output:
kubeadm token create --print-join-command

## From the worker node you want to add:
kubeadm join 192.168.6.2:6443 --token 12abcd.qwerty1234asdf --discovery-token-ca-cert-hash sha256:11111111111111122222222222222233333333333aaaaaaaaaaaabbbbbbbbbbb
```

You should now be able to see the node in the cluster (`kubectl get nodes`) added after a few minutes.  It should
eventually enter a 'ready' state.  If not run the following command from the worker node as root:

```shell 
### For troubleshooting the Kubelet process
journalctl -u kubelet
```

## Add Self signed Certificate Authorities (CA) to Ubuntu
### Copy CA certs to all cluster nodes
SCP the certificate to all servers in the k8s cluster.  Change the
host names where appropriate.

>(NOTE: In this example, I'm using a 'bundled' cert that contains the CA inline)

```shell
scp /home/hduser/certs/bundle-jaroof.local.crt hduser@nuc-worker1:/home/hduser/bundle-jaroof.local.crt
scp /home/hduser/certs/bundle-jaroof.local.crt hduser@nuc-worker2:/home/hduser/bundle-jaroof.local.crt
scp /home/hduser/certs/bundle-jaroof.local.crt hduser@nuc-worker3:/home/hduser/bundle-jaroof.local.crt
scp /home/hduser/certs/bundle-jaroof.local.crt hduser@nuc-worker4:/home/hduser/bundle-jaroof.local.crt
```

### Copy the Certificate to the correct directory
>(IMPORTANT!!! This step has to be performed on every server in the k8s cluster.)
Also, the directory that you are copying to below is Ubuntu specific>
```shell
sudo cp /home/hduser/bundle-jaroof.local.crt /usr/local/share/ca-certificates
```

### Update the CA certs on all nodes
Run the following command on all nodes once the certificate is copied to the
appropriate directory.

```shell
sudo update-ca-certificates
```

## Multi Tool Install - For network troubleshooting within k8s
This pod contains a lot of useful networking tools, for example `nslookup, curl, nc, ip` etc.

```shell
kubectl create deployment multitool --image=praqma/network-multitool
```