# ArgoCD Instructions
ArgoCD deploys applications into k8s using Gitops.

## Install the CLI
Install the CLI for your workstation using the URL below:

[https://github.com/argoproj/argo-cd/blob/master/docs/cli_installation.md](https://github.com/argoproj/argo-cd/blob/master/docs/cli_installation.md)

## Install ArgoCD into Kubernetes
Run the following commands to install ArgoCD into Kubernetes. 

>(NOTE: These instructions are based on the following URL: [https://github.com/argoproj/argo-cd/blob/master/docs/getting_started.md#1-install-argo-cd](https://github.com/argoproj/argo-cd/blob/master/docs/getting_started.md#1-install-argo-cd))

```shell
kubectl create namespace argocd
kubectl apply -n argocd -f https://raw.githubusercontent.com/argoproj/argo-cd/stable/manifests/install.yaml
```

## (OPTIONAL) Install a Load-Balancer for external access. 
>(NOTE: An ingress (mentioned below) is probably a better option here.)
```shell 
kubectl patch svc argocd-server -n argocd -p '{"spec": {"type": "LoadBalancer"}}'
```

## (Optional) Install Ingress for external access
Reference:
- [https://github.com/argoproj/argo-cd/blob/master/docs/operator-manual/ingress.md](https://github.com/argoproj/argo-cd/blob/master/docs/operator-manual/ingress.md)

Argo requires both HTTPS and gRPC endpoints to function. Since the default NGINX ingress only allows
one protocol per ingress, you will have to create two ingresses and two secrets

### Create ingress secrets for HTTPS and gRPC
>(NOTE: View the README file under /kubernetes/intel-nuc-cluster/certificate/authority/README.md for more
> details in the setup of a self-signed cert with included (bundled) CA cert.)

```shell
### 'bundle-jaroof.local.crt' and 'jaroof.local.key' should have been created during the certificate
### creation process mentioned in the note above.

# Create the HTTPS secret 
kubectl create secret tls argocd-ingress-http --cert=bundle-jaroof.local.crt --key=jaroof.local.key --namespace argocd

# Create the gRPC secret
kubectl create secret tls argocd-ingress-grpc --cert=bundle-jaroof.local.crt --key=jaroof.local.key --namespace argocd

```

### Create the HTTPS and gRPC Ingresses
>(NOTE: You should modify the `grpc-ingress.yaml` and `https-ingress.yaml` files to match your domain
> name for Argo. In this example, I'm using *.jaroof.local domains.)

```shell
kubectl apply -f grpc-ingress.yaml --namespace argocd

kubectl apply -f https-ingress.yaml --namespace argocd
```

### Update a ConfigMap setting to allow the ingresses to work correctly
Update the `ConfigMap` called: `argocd-cmd-params-cm`

Add a data section that looks like this:
```yaml
apiVersion: v1
data:
  server.insecure: "true"   ### <--- This is the important line.
kind: ConfigMap       
```

Once the ConfigMap is updated and saved, restart all the ArgoCD pods.  At this point, you should be able
to access Argo via the dashboard URL. In my example, that will be `https://argocd.jaroof.local`

### Login to the Argo Dashboard
In a web browser, go to the ArgoCD dashboard URL that you setup in the previous Ingress steps.

The default user will be `admin`. To get the password, run the following command:

```shell
kubectl get secrets/argocd-initial-admin-secret --namespace argocd --template={{.data.password}} | base64 -d
```

### Login to Argo using the CLI
>(NOTE: Install the ArgoCD CLI before running this command)

From a command prompt, run the following command. (NOTE: In this example, I'm using the domain name of my
ArgoCD URL (https://argocd.jaroof.local). You should change this with your URL.)

```shell
argocd login <DOMAIN_OF_ARGOCD>

# For example:
argocd login argocd.jaroof.local
```

### Add a self-signed Git repo certificate
If you're using a private Git repository with a self-signed cert, you'll have to add the certificate to
Argo to avoid X.509 errors when accessing the Git Repo. Run the following command:

>(NOTE: This assumes you've already created the bundled cert in a previous step)

```shell
argo cert add-tls <DOMAIN_OF_GIT_REPO> --from <LOCATION_OF_CERT>

# For example:
argo cert add-tls gitlab.jaroof.local --from bundle-jaroof.local.crt
```

## Create an application
An application can be a Git Repo with Manifest files like, "Deployments", "StatefulSets", etc.
or Helm charts.  ArgoCD will watch this repo and ensure that the state is maintained by comparing
the Git repo configuration files with the state of the k8s cluster.

Applications can be created with either a CLI or UI.

### Application creation using the CLI

```shell 
argocd app create guestbook --repo https://github.com/argoproj/argocd-example-apps.git --path guestbook --dest-server https://kubernetes.default.svc --dest-namespace default
```

### Application creation using the UI
Login to the UI using either the Load Balancer IP address, Ingress URI, or via Port Forwarding to the 
`argocd-server` pod.

>(IMPORTANT: If you're using a private Git repository and have created your own TLS certificates and CA Cert bunlde
> you should load that using the UI under:  settings -> Certificates.  You should load a bundled
> certificate with both the Domain name and CA cert together. To learn more about this process, 
> take a look at the README file in this git Repo, under: `/kubernetes/intel-nuc-cluster/certificate-authority/README.md`)

>(IMPORTANT #2: If you're using a private docker registry with login credentials, make sure to follow
> the instructions in this Git repo listed here: `/kubernetes/intel-nuc-cluster/docker-registry/secure-registry-with-tls/README.md`
> specifically the section called `(Option 2) Use the default service account`)

Create a new Application and point to a Git repo or subpath within the repo that contains a Helm chart.
With the certs and Docker username/password in place (as listed in the 'IMPORTANT' sections above) ArgoCD
should deploy your application.  As you make changes to the repo, you can perform a `hard sync` to update
all the k8s objects using Argo.

