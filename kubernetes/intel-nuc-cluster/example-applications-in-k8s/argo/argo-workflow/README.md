# Argo Workflow Howto
Argo Workflow allows docker containers to be orchestrated in a workflow using Kubernetes.

## Setup
### Install the CLI
Install the CLI for your workstation using the URL below:

[https://github.com/argoproj/argo-workflows/releases/latest](https://github.com/argoproj/argo-workflows/releases/latest)

### Install Argo Workflow into Kubernetes
Run the following commands to install Argo Workflows into Kubernetes.  For more advanced installations
you will probably want to install your own Postgres and Minio installations.

>(NOTE: These instructions are based on the following URL: [https://argoproj.github.io/argo-workflows/quick-start/](https://argoproj.github.io/argo-workflows/quick-start/))

```shell
kubectl create ns argo
kubectl apply -n argo -f https://raw.githubusercontent.com/argoproj/argo-workflows/master/manifests/quick-start-postgres.yaml
```

### Advanced Installation
(IMPORTANT!!!!: If you installed using the commands above you don't have to run these commands)
To run Argo Workflow with your own database and distributed file store (Minio, S3, etc.) You should
use the helm chart.  Follow the instructions in the URL below, changing database and file store parameters to
match your environment:

[https://github.com/argoproj/argo-helm/tree/master/charts/argo-workflows](https://github.com/argoproj/argo-helm/tree/master/charts/argo-workflows)

## View the Dashboard
To view the Argo dashboard, you can either setup a Load-balancer, Ingress, or use port forwarding
like the example below:

```shell
kubectl -n argo port-forward deployment/argo-server 2746:2746
```

## Use the CLI to submit a workflow example:

```shell
argo submit -n argo --watch https://raw.githubusercontent.com/argoproj/argo-workflows/master/examples/hello-world.yaml
argo list -n argo
argo get -n argo @latest
argo logs -n argo @latest
```

## Tear Down (Simple)
To tear down the cluster using the simple installation method, run the following command:

>(NOTE: If you installed using Helm, you can perform a `helm uninstall` to cleanup the installation)

```shell
kubectl apply -n argo -f https://raw.githubusercontent.com/argoproj/argo-workflows/master/manifests/quick-start-postgres.yaml
```

