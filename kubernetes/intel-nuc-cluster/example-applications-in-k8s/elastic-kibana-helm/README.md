# Install ElasticSearch and Kibana using Helm

## Install the ElasticSearch Chart
```shell
kubectl create namespace elastic

helm repo add elastic https://helm.elastic.co

helm upgrade --install --namespace elastic --values custom-elastic-value-overrides.yaml elasticsearch elastic/elasticsearch
```

## Install Kibana
```shell 
helm upgrade --install --namespace elastic --values custom-kibana-values.yaml kibana elastic/kibana
```

## Remove the charts
```shell
helm uninstall kibana --namespace elastic
helm uninstall elasticsearch --namespace elastic

kubectl delete namespace elastic
```