# DEPRECATED!!! Install the Elastic Stack using Helm
>(NOTE: The `stable` version of the Elastic helm charts are now deprecated.  Use the charts created and maintained by
> ElasticSearch Corp. [https://github.com/elastic/helm-charts/tree/main/elasticsearch](https://github.com/elastic/helm-charts/tree/main/elasticsearch))

## Install the Elastic Stack Chart
```
helm upgrade --install --namespace elastic --values values.yaml elastic-stack stable/elastic-stack
```

## Remove the chart
```
helm del --purge elastic-stack
kubectl delete namespace elastic
```