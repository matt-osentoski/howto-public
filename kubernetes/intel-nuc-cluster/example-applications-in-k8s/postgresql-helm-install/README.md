# Install Postgresql using dynamic volume provisioning
This document describes the steps to install a Postgresql database using Helm and a dynamic volume provisioning as the
persistent store.

Author: Matt Osentoski  
E-mail: matt.osentoski@gmail.com

## Dependencies
### Storage Class
It's assumed you already have a Kubernetes Storage class setup.  This example will use the
storage class created using Rook.io / Ceph as the storage system.  It is also assumed that
the storage class name that was created is called `rook-ceph-block`.

See the following README file for details around installing Rook.io and Ceph:
[https://bitbucket.org/matt-osentoski/howto-public/src/master/kubernetes/intel-nuc-cluster/dynamic-volume-provisioning/storage-classes/rook.io/README.md]()

### Helm
Helm should already be setup on your cluster.  If not, take a look at the following README:
[https://bitbucket.org/matt-osentoski/howto-public/src/master/kubernetes/intel-nuc-cluster/helm/README.md]()

## Make changes to the values.yaml file
The values.yaml file in this directory allow for custom settings to the Postgres database.  Note that the following
changes have been made from the default file located here:

[https://github.com/helm/charts/blob/master/stable/postgresql/values.yaml]()

```yaml
service:
  ## PosgresSQL service type
  type: LoadBalancer

persistence:
  storageClass: "rook-ceph-block"
  
# NOTE: This setting is required to sync the database using things like Kafka Connect using Debezium
postgresqlExtendedConf: {"wal_level": "logical"}

postgresqlPassword: postgres
```

## Install Postgresql using Helm.
>(NOTE: These instructions are derived from: https://github.com/helm/charts/tree/master/stable/postgresql)

Install the helm chart, pointing to the Storage class:
```
helm install --name=postgresql --namespace postgres -f values.yaml stable/postgresql
```

>(NOTE: The `values.yaml` is located in the same directory as this README file)

