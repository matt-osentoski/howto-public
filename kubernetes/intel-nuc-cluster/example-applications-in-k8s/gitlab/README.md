# Gitlab installation
This document describes the process of installing GitLab using Helm. Gitlab is an open source Git server.

## Dependencies
- A Default storageClass should be setup before beginning
- A domain for GitLab should be determined ahead of time.  For this example, I will use `gitlab.jaroof.local` using a wildcard cert created for `*.jaroof.local`

## Create a self-signed certificate
Follow the instructions in this repo under: 

`/kubernetes/intel-nuc-cluster/certificate-authority/README.md`

You should have a `.crt` containing the wildcard certificate and also including the CA cert.  You will also have
a private key file for the wildcard cert ending with `.key`

## Installation and setup
### Installation dependencies
Run the following command to install Gitlab using Helm

>(NOTE: The certificate instructions are derived from: [https://docs.gitlab.com/charts/installation/tls.html](https://docs.gitlab.com/charts/installation/tls.html) )

```shell 
helm repo add gitlab https://charts.gitlab.io/
helm repo update

kubectl create namespace gitlab

### Install the cert and key as a secret.  
### This should be done at the location where the cert and key reside on the file system.
kubectl create secret tls gitlab-self-cert --cert=full-chain-somedomain.local.crt --key=somedomain.local.key --namespace gitlab

```

### Create a Cert for the Git Runner
>(IMPORTANT!!!!! This is different than the secret in the previous steps. That was a 'server-side' cert,
> this will be a 'client-side' cert that will be used by the Git Runner. DO NOT SKIP THIS STEP!!!
> Also, mentioned below, the Cert you use must follow this naming pattern `<gitlab.hostname.crt>`
> or use the format: --from-file=gitlab.jaroof.local.crt=my-fullchain-cert.crt, where 'gitlab.jaroof.local' is
> your hostname. and 'my-fullchain-cert.crt' is the name of the cert file on your local machine.)

This certificate is required if you're using a self-signed cert. The runner uses this to validate
the gitlab URL endpoints against a known Certificate Authority (CA). This will be used later in the 
helm installation via the following setting: `--set gitlab-runner.certsSecretName=gitlab-runner-cert`

>(NOTE: In the )

```shell 
### Make sure --from-file=....  starts with the domain name of the gitlab server!!!!
### Otherwise, name the `crt` file with the following format: `<gitlab.hostname.crt>`
### For example, in my case it would be `gitlab.jaroof.local.crt`
kubectl create secret generic gitlab-runner-cert --namespace gitlab --from-file=gitlab.jaroof.local.crt=full-chain-somedomain.local.crt
```

### Install Gitlab using Helm
```shell
helm upgrade --install gitlab gitlab/gitlab \
  --namespace gitlab \
  --timeout 600s \
  --set global.hosts.domain=jaroof.local \
  --set certmanager.install=false \
  --set global.ingress.class=nginx \
  --set nginx-ingress.enabled=false \
  --set global.ingress.configureCertmanager=false \
  --set global.ingress.tls.secretName=gitlab-self-cert \
  --set gitlab-runner.certsSecretName=gitlab-runner-cert
```

>(NOTE: References to the charts values are below. Also note that we're assuming an existing
> nginx ingress controller is already installed on the cluster, therefore `nginx-ingress.enabled=false`
> Since we are using an existing nginx ingress, in this example we also need to set`global.ingress.class=nginx` to
> correspond with the class in our preinstalled Ingress controller. (NOTE: Your ingress class name may vary)
> - [https://gitlab.com/gitlab-org/charts/gitlab/-/blob/master/values.yaml](https://gitlab.com/gitlab-org/charts/gitlab/-/blob/master/values.yaml)
> - [https://gitlab.com/gitlab-org/charts/gitlab-runner/blob/main/values.yaml](https://gitlab.com/gitlab-org/charts/gitlab-runner/blob/main/values.yaml)
>)



## Connecting to the GitLab Dashboard
### Find the GitLab URL
Once the pods have started, you can initially login to Gitlab. To do this, first obtain the 
name of the URL.  In this example, that URL would be `gitlab.jaroof.local`. 

To find the external IP address of this ingress controller, run the following command from a shell prompt:

```shell
kubectl get service  -o wide --namespace gitlab
```

Now that you have the external IP address, setup DNS to point to this entry. For example, in your `/etc/hosts` file 
you might have an entry that looks like this:

```
### NOTE: This assumes that the external IP of the ingress controller is 192.168.6.151
### and that your domain name is gitlab.jaroof.local
###
192.168.6.151   gitlab.jaroof.local
```

### Setup DNS entries in all worker nodes
Make sure to setup DNS entries in all the k8s worker nodes. If you're using an external DNS server this will
be even easier.

### Login to the Gitlab Dashboard

>(NOTE: If you're using a self-signed certificate, you may have to load the Certificate Authority (CA) into
> your OS or browser cert manager.)

Once DNS is setup, open a Web Browser and point to the URL. In my case that will be `https://gitlab.jaroof.local`.

### Login credentials
When you first login, you will use the `root` username. To find the password, run the following command to
obtain the password from the `gitlab-gitlab-initial-root-password` secret in the `gitlab` namespace.

```shell
kubectl get secrets/gitlab-gitlab-initial-root-password --namespace gitlab --template={{.data.password}} | base64 -d
```

At this point, you can change the root user's password to something else.

## Creating an example repo
In the Gitlab dashboard, click the `New Project` button from the `Projects` section.

The project will generate a URL based on the user/group and the project name. In this example, my
repo url would be: `https://gitlab.jaroof.local/root/test.git`

### Clone the repo from your workstation.
>(NOTE: This workstation should already have DNS setup to access the Gitlab cluster)

```shell
git clone https://gitlab.jaroof.local/root/test.git
```

>(IMPORTANT!!!! If you run into an error that looks like this: `fatal: unable to access 'https://gitlab.jaroof.local/root/test.git/': SSL certificate problem: self signed certificate in certificate chain`
> You will have to add the Certificate Authority to Git, or in the worst case scenario, ignore the cert. )

To use Certificate Authorities already installed on your OS (Windows example) run the following command:
```shell
git config --global http.sslbackend schannel
```

(OPTIONAL) To Ignore the certs, run this command:  
>(NOTE: This is less desirable than linking with the OS certs)
```shell
git config --global http.sslVerify false
```

## Troubleshooting
### The Gitlab runner won't start (DNS Issue)
If you see the following error in your Gitlab runner, you may need to manually add a DNS entry to your k8s cluster:
```
ERROR: Registering runner... failed runner=RyQ0R9Fe status=couldn't execute POST against https://gitlab.jaroof.local/api/v4/runners: Post "https://gitlab.jaroof.local/api/v4/runners" ││ PANIC: Failed to register the runner. 
```

This error is most likely caused by the pod not being able to access the Gitlab server, because you aren't
using a DNS server to resolve the name, but are manually adding DNS records (ex: `gitlab.jaroof.local` )

To solve this problem you will have to manually add DNS entries into the `CoreDNS` sub sytsem. To do this,
read the section `Adding static DNS entries to k8s` in the following document in this Git repo: [../../COMMON_COMMANDS.md](../../COMMON_COMMANDS.md)


## Uninstall Gitlab
Run the following commands to uninstall GitLab

```bash 
helm uninstall gitlab --namespace gitlab
kubectl delete namespace gitlab
```