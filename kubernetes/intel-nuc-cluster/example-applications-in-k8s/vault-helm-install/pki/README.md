# Public Key Infrastructure (PKI) Using Vault
This document explains how to setup Root, intermediate, and leaf certificates
using Vault's PKI subsystem.

>(NOTE: This document assumes you already have a Vault server up and running. View the README file
> one level up for Vault installation instructions in k8s.)

>(REFERENCES: This document is based on the following URLs:
> - [https://developer.hashicorp.com/vault/tutorials/secrets-management/pki-engine](https://developer.hashicorp.com/vault/tutorials/secrets-management/pki-engine)
> - [https://developer.hashicorp.com/vault/tutorials/kubernetes/kubernetes-cert-manager](https://developer.hashicorp.com/vault/tutorials/kubernetes/kubernetes-cert-manager)
> - [https://developer.hashicorp.com/vault/tutorials/policies/policies](https://developer.hashicorp.com/vault/tutorials/policies/policies)
>)

## Configure PKI in Vault
### Enable the PKI Secrets Engine
Terminal into your running Vault Server and run the following commands:

```shell
### First login to Vault using your root token
vault login

### Next enable the PKI Secret Engine
vault secrets enable pki
```

### Set PKI secret expiration for a maximum of 10 years
```shell
### Again, this should be performed within the Vault server.
vault secrets tune -max-lease-ttl=87600h pki
```

### Create a Vault policy
```shell
### Change to the temp directory to create a new file in your Vault server
cd /tmp

### Create a policy file with PKI permissions.
cat << EOF > pki-admin-policy.hcl
path "sys/mounts/*" {
  capabilities = [ "create", "read", "update", "delete", "list" ]
}

# List enabled secrets engine
path "sys/mounts" {
  capabilities = [ "read", "list" ]
}

# Work with pki secrets engine
path "pki*" {
  capabilities = [ "create", "read", "update", "delete", "list", "sudo", "patch" ]
}
EOF

### Create the policy using the file created in the last step.
vault policy write pki-admin pki-admin-policy.hcl
```

### (OPTIONAL) Review the Policy
To validate the policy you can run the following commands:

```shell
### List all policies
vault policy list

### View the policy we just created
vault policy read pki-admin
```

## Root Certificate Authority (CA)
### Create a Root certificate

```shell
vault write -field=certificate pki/root/generate/internal \
     common_name="example.local" \
     issuer_name="root-ca-2024" \
     country="US" \
     province="Michigan" \
     locality="Detroit" \
     ou="Root Issuer" \
     organization="Jaroof LLC" \
     street_address="123 Main St." \
     postal_code="90210" \
     ttl=87600h > root_2024_ca.crt
```

>(NOTE: The certificate is saved as a file which can be downloaded or copied)

### Create a Role for the Root CA

```shell
vault write pki/roles/root-ca-2024 allow_any_name=true
```

### Configure the CA and CRL URLs

>(NOTE: You should replace the Domain of the URL with your own domain. 
> (Ex: Replace `vault.jaroof.local` with your own domwain.))

```shell
vault write pki/config/urls \
     issuing_certificates="https://vault.jaroof.local/v1/pki/ca" \
     crl_distribution_points="https://vault.jaroof.local/v1/pki/crl"
```

## Create an Intermediate Certificate.
### Enable the PKI secrets engine at a Path used for Intermediate Certs.

```shell
vault secrets enable -path=pki_int pki
```

### Set the Timeout of Intermediate Certificates to a maximum of 5 years

```shell
vault secrets tune -max-lease-ttl=43800h pki_int
```

### Create the Certificate Signing Request (CSR) for the Intermediate cert.
>(IMPORTANT!!!! This command will output JSON with the CSR embedded in the 
> `.data.csr` element. The certificate will contain newline characters that will
> have to be cleaned up!  The JQ command mentioned below handles this step.)
```shell
vault write -format=json pki_int/intermediate/generate/internal \
     common_name="example.local Intermediate Authority" \
     country="US" \
     province="Michigan" \
     locality="Detroit" \
     ou="Intermediate Issuer" \
     organization="Jaroof LLC" \
     street_address="123 Main St." \
     postal_code="90210" \
     issuer_name="example-local-intermediate"  > intermediate-csr.json
     
## IMPORTANT: You should use JQ to retrieve the Certificate Signing Request (CSR) from the JSON produced by Vault then 
## redirect the output into a file (.csr). NOTE that the `-r` option removes newline characters.
cat intermediate-csr.json | jq -r '.data.csr' > pki_intermediate.csr
```

### Issue the Intermediate Cert using the Root Certificate Authority (CA)
>(IMPORTANT!!!! This command will output JSON with the Intermediate Cert embedded in the
> `.data.certificate` element. The certificate will contain newline characters that will
> have to be cleaned up!  The JQ command mentioned below handles this step.)
```shell
vault write -format=json pki/root/sign-intermediate \
     issuer_ref="root-ca-2024" \
     csr=@pki_intermediate.csr \
     country="US" \
     province="Michigan" \
     locality="Detroit" \
     ou="Intermediate Issuer" \
     organization="Jaroof LLC" \
     street_address="123 Main St." \
     postal_code="90210" \
     format=pem_bundle ttl="43800h"  > intermediate-cert.json
     
## IMPORTANT: You should use JQ to retrieve the Certificate from the JSON produced by Vault then 
## redirect the output into a file (.pem). NOTE that the `-r` option removes newline characters.
cat intermediate-cert.json | jq -r '.data.certificate' > pki_intermediate.pem
```

### Write the Intermediate Cert into Vault
```shell
vault write pki_int/intermediate/set-signed certificate=@pki_intermediate.pem
```

### Create a Role to allow the Intermediate cert to issue Leaf Certs
>(NOTE: Ideally, you want to set the `max_ttl` to a lower duration. In this example, we
> will use a year. A value of `max_ttl="720"` (one month) would be better though.)

```shell
vault write pki_int/roles/example-dot-local \
     issuer_ref="$(vault read -field=default pki_int/config/issuers)" \
     allowed_domains="example.local" \
     allow_subdomains=true \
     allow_bare_domains=true \
     allow_wildcard_certificates=true \
     max_ttl="8760h"

```

### Request a Leaf Certificate (Single sub-domain)
>(NOTE: Again, try to use a `ttl` that is shorter in duration. We'll use a year again, in this example.)

```shell
vault write pki_int/issue/example-dot-local \
  common_name="test.example.local" \
  ttl="8760h" > test.example.local-cert.txt
```

The output file will contain the cert chain and private key

### Request a Leaf Certificate (Wildcard Cert)
>(NOTE: Again, try to use a `ttl` that is shorter in duration. We'll use a year again, in this example.)

```shell
vault write pki_int/issue/example-dot-local \
  common_name="*.example.local" \
  ttl="8760h" > wildcard.example.local-cert.txt
```

The output file will contain the cert chain and private key

## Vault Commands
- Source [https://developer.hashicorp.com/vault/api-docs](https://developer.hashicorp.com/vault/api-docs)

### General
- Source [https://developer.hashicorp.com/vault/docs/commands/operator](https://developer.hashicorp.com/vault/docs/commands/operator)

```shell
## Init (First time initialization only!!!!)
vault operator init

## Login
vault login

## Unseal
vault operator unseal
```

### KV 
- Source [https://developer.hashicorp.com/vault/api-docs/secret/kv/kv-v2](https://developer.hashicorp.com/vault/api-docs/secret/kv/kv-v2)

```shell
## Create a secret
vault kv put secret/example key1="hello world"

## Retrieve a secret
vault kv get secret/example

## Delete a secret (soft delete)
vault kv delete secret/example

## Destroy (hard delete)
vault kv destroy secret/example
```

### PKI
- Source [https://developer.hashicorp.com/vault/api-docs/secret/pki#list-certificates](https://developer.hashicorp.com/vault/api-docs/secret/pki#list-certificates)

```shell
## List PKI issuers
vault list pki/issuers

## Delete a specific issuer
vault delete pki/issuer/<ISSUER_ID>

## Delete ALL issuers and keys. WARNING!!! This will delete everything PKI Related!!!
vault delete pki/root

## List certs
vault list pki/certs

## Delte cert
vault delete pki/cert/<CERT_ID>
```