# Install Vault using Helm

## Modfiy the values.yaml file
For my install, I changed the following `value-overrides.yaml` settings:
```

```

## Install the Vault Chart
First, check out the Vault chart using Git
>(IMPORTANT!!!!! This installs Vault in `dev` mode. In this mode all data is transient
> on server restart. To run in `production` mode, see the `Production Mode` section below in
> this document)

```shell
kubectl create namespace hashicorp
helm repo add hashicorp https://helm.releases.hashicorp.com

helm upgrade --install --namespace hashicorp --values value-overrides.yaml vault hashicorp/vault
```

## (OPTIONAL) Expose Vault as Nodeports
```
kubectl expose service vault --type=NodePort --name=vault-service-nodeport --namespace hashicorp
kubectl expose service vault-ui --type=NodePort --name=vault-ui-service-nodeport --namespace hashicorp
```

## Set Environment variables
Terminal into the vault pod using VSCode.  Then create the following file:
`/tmp/certs.env`.  Populate the variables using the following commands to determine
the values.  

>(NOTE: These will have to be run on a machine with kubectl access to the cluster)
```
export VAULT_SA_NAME=$(kubectl --namespace=examples get sa vault-auth -o jsonpath="{.secrets[*]['name']}")
export SA_JWT_TOKEN=$(kubectl  --namespace=examples get secret $VAULT_SA_NAME -o jsonpath="{.data.token}" | base64 --decode; echo)
export SA_CA_CRT=$(kubectl  --namespace=examples get secret $VAULT_SA_NAME -o jsonpath="{.data['ca\.crt']}" | base64 --decode; echo)
```

Once the variables in the `/tmp/certs.env` are set, you can run:
```
source certs.env
```

You can then setup Vault for Kubernetes, following the instructions at:
[kubernetes/intel-nuc-cluster/vault-integration/README.md](kubernetes/intel-nuc-cluster/vault-integration/README.md)


## Prod mode
In the previous instructions we were running Vault in a simple `dev` mode. For a more robust installation,
we will be disabling `dev` mode and using an Ingress with TLS for external access.

## Create a secret for Ingress with TLS
In this example, we will be using `vault.jaroof.local` as the URL for accessing Vault. We will have to 
add the `*.jaroof.local` wildcard cert as a secret to allow TLS termination at the ingress.

>(NOTE: View the README file under /kubernetes/intel-nuc-cluster/certificate/authority/README.md for more
> details in the setup of a self-signed cert with included (bundled) CA cert.)

```shell 
### 'bundle-jaroof.local.crt' and 'jaroof.local.key' should have been already created. (view the NOTE above)
###
kubectl create secret tls jaroof-cert-secret --cert=bundle-jaroof.local.crt --key=jaroof.local.key --namespace hashicorp
```

### Server Install via Helm
To run the cluster in prod mode, use the `value-overrides-prod.yaml` with dev mode disabled. 

```shell 
helm upgrade --install --namespace hashicorp --values value-overrides-prod.yaml vault hashicorp/vault
```

### Initialize the Server
Unlike `dev` mode there a few steps required to get the Vault server going in 
production mode.  First, let's initialize the server.  This will created keys required for
the next step.

Terminal / Shell into the running server and run the following command:

```shell
vault operator init
```

The output of this command will look something like this:
>(IMPORTANT!!!!! Save these keys as you will have to use three of them to unseal the
> Vault server after it has be restarted!)

``` 
Unseal Key 1: Q12+AJuTMzOM6mUbR7t6OLqdig5QOjAM66A2Ei/9dM0R
Unseal Key 2: tovEn2Jx7/WKZ43i3yPynR5Pq75THOP+L3PR1PYJRfTw
Unseal Key 3: Q14IVzNhHCFMcn+Ei933rMkiGcefIPrGO4wDMMdMpqMn
Unseal Key 4: FkjH++LBVQ8eDGPJ7XtaUUgvBG7lb3mF5iW3T5p0cHTv
Unseal Key 5: J0pIArbWpyVsX2xxoB0avTAsM5ZK+PX1Vr8ij+oiSrrm

Initial Root Token: hvs.HV6PTgLqFqwN0u85F5rQ2v1s
```

### Unseal the Server
Everytime you restart the server, you have to run the following command
THREE times using three different keys from the 'Unseal' keys shown in the previous
example.

Run the following command THREE times from a Terminal / Shell prompt:

```shell 
### Run this THREE times with THREE different key!
vault operator unseal
```

This command is a little confusing, it looks like it doesn't take effect. The
reason is that Vault wants you to run it three times with three different keys
before the server will unseal.

At this point, the server is ready for use.