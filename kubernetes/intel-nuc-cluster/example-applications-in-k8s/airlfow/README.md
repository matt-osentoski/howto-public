# Install Airflow and Papermill using Helm
Airflow is a workflow tool using Python.  Papermill is a plug-in for Airflow that 
allows execution of Jupyter notebooks within an Airflow workflow. 

## Modfiy the values.yaml file
For my install, I changed the following `value-overrides.yaml` settings:
```

```

## Install the Chart
```
helm repo add bitnami https://charts.bitnami.com/bitnami
helm upgrade --install --namespace airflow --values value-overrides.yaml airflow bitnami/airflow
```

## Remove the chart
```
helm del --purge airflow
kubectl delete namespace airflow
```