# Install MongoDB using the Bitnami Helm Chart
This document describes how to install MongoDB using Helm with a StorageClass as
the backing volume.

Author: Matt Osentoski  
E-mail: matt.osentoski@gmail.com

>(NOTE: This document is derived from: [https://github.com/bitnami/charts/tree/master/bitnami/mongodb](https://github.com/bitnami/charts/tree/master/bitnami/mongodb) )

## Install a standalone MongoDB using Helm
Install the helm chart, pointing to the Storage class:
```
helm repo add bitnami https://charts.bitnami.com/bitnami
kubectl create namespace mongodb
helm upgrade --install --namespace mongodb --values value-overrides-standalone-nodeports.yaml mongodb bitnami/mongodb
```

### Add a nodeport manually
Currently, this chart will NOT create a node port if you have the `architecture` set to `standalone`.

Run the following commands to setup the nodeport:

```
cd manifests
kubectl apply -f mongo-nodeport.yaml --namespace mongodb
```

>(NOTE: The `value-overrides-nodeports` is located in the same directory as this README file)

## Install a replica MongoDB cluster using Helm

>(IMPORTANT!!!! At this time, the Nodeports do NOT work!  Use the Loadbalancer configurations instead)
```
helm repo add bitnami https://charts.bitnami.com/bitnami
kubectl create namespace mongodb
helm upgrade --install --namespace mongodb --values value-overrides-replica-lb.yaml mongodb bitnami/mongodb
```

## Teardown
To remove all traces of MongoDB in Kubernetes, run the following commands:

```
helm uninstall mongodb --namespace mongodb
kubectl delete namespace mongodb
```