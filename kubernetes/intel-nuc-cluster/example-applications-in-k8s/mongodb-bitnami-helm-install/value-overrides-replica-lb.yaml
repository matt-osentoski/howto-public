## Global Docker image parameters
## Please, note that this will override the image parameters, including dependencies, configured to use the global value
## Current available global Docker image parameters: imageRegistry and imagePullSecrets
##
global:
  storageClass: rook-ceph-block
  namespaceOverride: mongodb

## Kubernetes Cluster Domain
##
clusterDomain: cluster.local

## MongoDB architecture. Allowed values: standalone or replicaset
##
architecture: replicaset

## Use StatefulSet instead of Deployment when deploying standalone
##
useStatefulSet: false

## MongoDB Authentication parameters
##
auth:
  ## Enable authentication
  ## ref: https://docs.mongodb.com/manual/tutorial/enable-authentication/
  ##
  enabled: true
  ## MongoDB root password
  ## ref: https://github.com/bitnami/bitnami-docker-mongodb/blob/master/README.md#setting-the-root-password-on-first-run
  ##
  rootPassword: "password"
  ## MongoDB custom user and database
  ## ref: https://github.com/bitnami/bitnami-docker-mongodb/blob/master/README.md#creating-a-user-and-database-on-first-run
  ##
  username: testuser
  password: password
  database: testdb

  ## Existing secret with MongoDB credentials
  ## NOTE: When it's set the previous parameters are ignored.
  ##
  # existingSecret: name-of-existing-secret

## Number of MongoDB replicas to deploy.
## Ignored when mongodb.architecture=standalone
##
replicaCount: 3

## MongoDB containers' resource requests and limits.
## ref: http://kubernetes.io/docs/user-guide/compute-resources/
##
resources:
  # We usually recommend not to specify default resources and to leave this as a conscious
  # choice for the user. This also increases chances charts run on environments with little
  # resources, such as Minikube. If you do want to specify resources, uncomment the following
  # lines, adjust them as necessary, and remove the curly braces after 'resources:'.
  limits:
    cpu: 2000m
    memory: 2Gi
  requests:
    cpu: 250m
    memory: 512Mi

## MongoDB pods' liveness and readiness probes. Evaluated as a template.
## ref: https://kubernetes.io/docs/concepts/workloads/pods/pod-lifecycle/#container-probes
##
livenessProbe:
  enabled: true
  initialDelaySeconds: 30
  periodSeconds: 10
  timeoutSeconds: 5
  failureThreshold: 6
  successThreshold: 1
readinessProbe:
  enabled: true
  initialDelaySeconds: 30
  periodSeconds: 10
  timeoutSeconds: 5
  failureThreshold: 6
  successThreshold: 1

## Enable persistence using Persistent Volume Claims
## ref: http://kubernetes.io/docs/user-guide/persistent-volumes/
##
persistence:
  enabled: true
  # storageClass: "-"
  accessModes:
    - ReadWriteOnce
  size: 10Gi

## Service parameters
##
service:
  ## Service type
  ##
  type: ClusterIP
  ## MongoDB service port
  ##
  port: 27017
  ## MongoDB service port name
  ##
  portName: mongodb
  ## Specify the nodePort value for the LoadBalancer and NodePort service types.
  ## ref: https://kubernetes.io/docs/concepts/services-networking/service/#type-nodeport
  ##
  nodePort: ""
  ## MongoDB service clusterIP IP
  ##
  # clusterIP: None
  ## Specify the externalIP value ClusterIP service type.
  ## ref: https://kubernetes.io/docs/concepts/services-networking/service/#external-ips
  ##
  externalIPs: []
  ## Specify the loadBalancerIP value for LoadBalancer service types.
  ## ref: https://kubernetes.io/docs/concepts/services-networking/service/#loadbalancer
  ##
  # loadBalancerIP:
  ## Specify the loadBalancerSourceRanges value for LoadBalancer service types.
  ## ref: https://kubernetes.io/docs/tasks/access-application-cluster/configure-cloud-provider-firewall/#restrict-access-for-loadbalancer-service
  ##
  loadBalancerSourceRanges: []
  ## Provide any additional annotations which may be required. Evaluated as a template
  ##
  annotations: {}

## External Access to MongoDB nodes configuration
##
externalAccess:
  ## Enable Kubernetes external cluster access to MongoDB nodes
  ##
  enabled: true
  ## External IPs auto-discovery configuration
  ## An init container is used to auto-detect LB IPs or node ports by querying the K8s API
  ## Note: RBAC might be required
  ##
  autoDiscovery:
    ## Enable external IP/ports auto-discovery
    ##
    enabled: false
    ## Bitnami Kubectl image
    ## ref: https://hub.docker.com/r/bitnami/kubectl/tags/
    ##
    image:
      registry: docker.io
      repository: bitnami/kubectl
      tag: 1.18.15-debian-10-r8
      ## Specify a imagePullPolicy
      ## Defaults to 'Always' if image tag is 'latest', else set to 'IfNotPresent'
      ## ref: http://kubernetes.io/docs/user-guide/images/#pre-pulling-images
      ##
      pullPolicy: IfNotPresent
      ## Optionally specify an array of imagePullSecrets (secrets must be manually created in the namespace)
      ## ref: https://kubernetes.io/docs/tasks/configure-pod-container/pull-image-private-registry/
      ## Example:
      ## pullSecrets:
      ##   - myRegistryKeySecretName
      ##
      pullSecrets: []
    ## Init Container resource requests and limits
    ## ref: http://kubernetes.io/docs/user-guide/compute-resources/
    ##
    resources:
      # We usually recommend not to specify default resources and to leave this as a conscious
      # choice for the user. This also increases chances charts run on environments with little
      # resources, such as Minikube. If you do want to specify resources, uncomment the following
      # lines, adjust them as necessary, and remove the curly braces after 'resources:'.
      limits: {}
      #   cpu: 100m
      #   memory: 128Mi
      requests: {}
      #   cpu: 100m
      #   memory: 128Mi
  ## Parameters to configure K8s service(s) used to externally access MongoDB
  ## A new service per broker will be created
  ##
  service:
    ## Service type. Allowed values: LoadBalancer or NodePort
    ##
    type: LoadBalancer
    ## Port used when service type is LoadBalancer
    ##
    port: 27017
    ## Array of load balancer IPs for each MongoDB node. Length must be the same as replicaCount
    ## Example:
    ## loadBalancerIPs:
    ##   - X.X.X.X
    ##   - Y.Y.Y.Y
    ##
    loadBalancerIPs:
      - 192.168.6.173
      - 192.168.6.174
      - 192.168.6.175
    ## Load Balancer sources
    ## ref: https://kubernetes.io/docs/tasks/access-application-cluster/configure-cloud-provider-firewall/#restrict-access-for-loadbalancer-service
    ## Example:
    ## loadBalancerSourceRanges:
    ## - 10.10.10.0/24
    ##
    loadBalancerSourceRanges: []
    ## Array of node ports used for each MongoDB nodes. Length must be the same as replicaCount
    ## Example:
    ## nodePorts:
    ##   - 30001
    ##   - 30002
    ##
    ## When service type is NodePort, you can specify the domain used for MongoDB advertised hostnames.
    ## If not specified, the container will try to get the kubernetes node external IP
    ##
    # domain: 192.168.6.3
    ## Provide any additional annotations which may be required. Evaluated as a template
    ##
    annotations: {}

arbiter:
  ## Enable deploying the MongoDB Arbiter
  ##   https://docs.mongodb.com/manual/tutorial/add-replica-set-arbiter/
  ##
  enabled: false