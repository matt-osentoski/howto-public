# Install Redis using Helm

## Install the Redis Chart
```
helm repo update
helm upgrade --install --namespace redis --values values.yaml redis stable/redis 
```

## Remove the chart
```
helm del --purge jhub
kubectl delete namespace jupyter
```