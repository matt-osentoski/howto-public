# Confluent Operator installation using Helm
This document describes the steps required to install the Confluent Operator using Helm charts.

>(NOTE: These instructions are derived from [https://docs.confluent.io/operator/current/co-deploy-cfk.html](https://docs.confluent.io/operator/current/co-deploy-cfk.html) )

>(NOTE2: Example config files for things like Kafka, Connect, etc can be found here: [https://github.com/confluentinc/confluent-kubernetes-examples/tree/master/general/cp-version](https://github.com/confluentinc/confluent-kubernetes-examples/tree/master/general/cp-version).
> Ideally, you would want to look at the KRAFT version instead of Zookeeper. For example: 
> [https://github.com/confluentinc/confluent-kubernetes-examples/blob/master/general/cp-version/confluent-platform-kraft-7.7.0.yaml](https://github.com/confluentinc/confluent-kubernetes-examples/blob/master/general/cp-version/confluent-platform-kraft-7.7.0.yaml))

## Create a namespace
```shell
kubectl create namespace confluent
```

## (OPTIONAL) Download the CFK bundle
This Tarball will contain a `helm` folder once it's been extracted. You can reference this
folder when deploying the chart instead of using the Remote repo. Again, this is optional.

```shell
curl -O https://packages.confluent.io/bundle/cfk/confluent-for-kubernetes-2.9.1.tar.gz
tar zxvf confluent-for-kubernetes-2.9.1.tar.gz
```

## Install the Operator
>(NOTE: If you exclude the `licenseKey` setting below, you will have a 30-day trial of the Confluent Operator)

```shell
helm repo add confluentinc https://packages.confluent.io/helm

helm upgrade --install --namespace confluent \
  --set licenseKey=<CFK license key> \
  --set kRaftEnabled=true \ 
  --set replicas=2 \
  confluent confluentinc/confluent-for-kubernetes
```

## Create a secret for Kafka (server) SASL/PLAIN authentication
Update the `plain-users.json` file in this directory with the username/passwords you would like to setup. Next,
create the secret by running the following command:

>(REFERENCE: [https://docs.confluent.io/operator/current/co-authenticate-kafka.html#server-side-sasl-plain-authentication-for-akk](https://docs.confluent.io/operator/current/co-authenticate-kafka.html#server-side-sasl-plain-authentication-for-ak))

```shell 
kubectl create secret generic kafka-auth-secret --from-file=plain-users.json=plain-users.json --namespace confluent
```

## Create a client secret to authenticate against the Kafka Server's SASL/PLAIN
This secret is used by other Kafka services (Connect, etc) that will connect to kafka, to allow them to authenticate
Update the `plain.txt` file in this directory with the username/passwords you would like to setup. Next,
create the secret by running the following command:

>(REFERENCE: [https://docs.confluent.io/operator/current/co-authenticate-kafka.html#client-side-sasl-plain-authentication-for-ak](https://docs.confluent.io/operator/current/co-authenticate-kafka.html#client-side-sasl-plain-authentication-for-ak))

```shell 
kubectl create secret generic kafka-client-secret --from-file=plain.txt=plain.txt --namespace confluent
```

## (OPTIONAL) Setup an Internal / External facing certificate
>(IMPORTANT!!!!! Make sure all the certificates and cert artifacts are created in Unix to avoid 
> carriage returns in the file)

First, create a CA certificate then a wildcard cert and private key.  See the instructions at
[wildcard-cert/README.md](wildcard-cert/README.md)

Next, create a secret using the certs and private key:

```shell 
kubectl create secret generic kafka-tls \
    --from-file=fullchain.pem=my_server.pem \
    --from-file=cacerts.pem=my_ca.pem \
    --from-file=privkey.pem=my_server-key.pem \
    --namespace confluent
```

In the `kafka.yaml` file add the following section, if it already doesn't exist:

```yaml 
spec:
  tls:
    secretRef: kafka-tls
```

## Install Kraft

```shell
kubectl apply -f manifest-files/kraft.yaml --namespace confluent
```

## Install Kafka using a Load Balancer
```shell
kubectl apply -f kafka.yaml --namespace confluent
```

### Advertised Listeners
It seems like b0, b1, b2, etc. are added after the domain name setup in Kafka.  For example,
if you setup `kafka.local` as the domain name under `kafka.loadbalancer.domain`, the brokers would
be advertised as:

- b0.kafka.local
- b1.kafka.local
- b2.kafka.local

These domains should then be set with the LoadBalancer IP address from k8s in your 'hosts' file. This IP address can be found using
the following command:
```
kubectl get service kafka-bootstrap-lb -o wide --namespace confluent
```

## Install Schema Registry (exposed using a Load Balancer)
```shell
kubectl apply -f schema-registry.yaml --namespace confluent
```

## Install Kafka Connect (exposed using a Load Balancer)
```shell
kubectl apply -f connect.yaml --namespace confluent
```

Verify the Plugins via this URL:

`https://<BASE_CONNECT_URL>/connector-plugins`

Install Connectors Via REST, for example:

```shell 
curl -X POST \
  -H "Content-Type: application/json" \
  --data '{ "name": "quickstart-jdbc-source", "config": { "connector.class": "io.confluent.connect.jdbc.JdbcSourceConnector", "tasks.max": 1, "connection.url": "jdbc:mysql://mysql:3306/connect_test?user=root&password=password", "mode": "incrementing", "incrementing.column.name": "id", "timestamp.column.name": "modified", "topic.prefix": "quickstart-jdbc-", "poll.interval.ms": 1000 } }' \
  https://<BASE_CONNECT_URL>/connectors/
```

Then, check the connector exists:

`https://<BASE_CONNECT_URL>/connectors`

### Troubleshooting Kafka Connect connector logs  
To view the Kafka Connect Connector logs, run the following command:

```shell  
kubectl logs -f connect-0 -c config-init-container --namespace confluent
```



## Client Setup with SASL/PLAIN
To run the command-line tools when the server is setup for SASL/PLAIN, you have to perform the following two steps:

### Step 1
Setup the `KAFKA_OPTS` environment variable to point to a Java Authentication and Authorization Service (jaas)
configuration file.  An example jaas config file is located in this directory, called `kafka_client_jaas.conf`
Assuming this file is located in the `/tmp/kafka_client_jaas.conf` location on your computer run the following command
to set the environment variable:

>(NOTE: We're using the `-Djava.security.auth.login.config` java option which takes the jaas config file as a value)

**Windows**
```
set KAFKA_OPTS=-Djava.security.auth.login.config=C:/tmp/kafka_client_jaas.conf
```

**UNIX / Mac**
```
export KAFKA_OPTS=-Djava.security.auth.login.config=C:/tmp/kafka_client_jaas.conf
```

### Step 2
Run the command using the `--command-config` option pointing to the `client.properties` file that defines
the security protocol and mechanism in use.  An example 'client.properties' file is located in this directory.
The example command below assumes it lives in the `/tmp/client.properties` folder on your computer.

**Topic CLI**
```
kafka-topics --bootstrap-server b0.kafka.local:9092 --create --topic test --partitions 5 --replication-factor 3 --command-config /tmp/client.properties
```

**Producer CLI**
```
kafka-console-producer --broker-list b0.kafka.local:9092 --topic test --producer.config /tmp/client.properties
```

**Consumer CLI**
```
kafka-console-consumer --bootstrap-server b0.kafka.local:9092 --topic test --consumer.config /tmp/client.properties
```

## Offset Explorer - Client setup for SASL/PLAIN with TLS/SSL network Encryption

### Create a Java keystore with the CA certificate as a Trust Store

>(VERY IMPORTANT!!!! If you're using Offset Explorer as the client, make sure to use the `keytool` in the 
> 'jre' folder of Offset Explorer.  There seems to be a subtle difference with this version and what may
> be installed on your Default JVM. You'll get an error like: `unable to open truststore with given password`
> even though the password for the truststore is correct!!!)

Locate your CA cert run the following command to create the Trust store.  You will be prompted for a password
during the creation process.  Make sure to write this down, as it will be used for the client setup:

```shell 
keytool -keystore client.truststore.jks -alias CARoot -import -file /some/path/rootCA.pem
```

### Configure Offset Explorer
Create a new connection in Offset Explorer and configure the following tabs:

- Properties
  - Bootstrap servers: <Enter the DNS names for the brokers that are bound to your LB> (Ex: kafka.somedomain.local:9092)

- Security Tab  
  - Type: SASL SSL
  - Truststore Location: <This will be the location of the client.truststore.jks file from the previous step>
  - Truststore Password: <This is the password you entered as part of the trust store creation process>

- Advanced Tab
  - 
  - SASL Mechanism: PLAIN

- Jaas Config Tab
  -  org.apache.kafka.common.security.plain.PlainLoginModule required username="kafkauser" password="mypassword";

## Troubleshooting
If the individual pods or other k8s objects won't delete, run a command similar
to the one below:
```
kubectl delete pod --grace-period=0 --force --namespace confluent kafka-0
```

If the namespace won't delete, try running this command to view all resources
for the Namespace
```
kubectl api-resources --verbs=list --namespaced -o name | xargs -n 1 kubectl get -n confluent
```

Next, view the objects that remain.  Try the 'force' delete command above.
If that doesn't work, you might have to copy the YAML of the object then clear
out anything in the `metadata - finalizers` section then run:
```
kubectl apply -f myobject.yaml --namespace confluent
```

## Teardown the cluster
To teardown the cluster, remove all items in reverse order.  

>(IMPORTANT!!! To avoid orphaned CRDs remove the Operator last!)

```shell
kubectl delete -f connect.yaml --namespace confluent
kubectl delete -f schema-registry.yaml --namespace confluent
kubectl delete -f kafka.yaml --namespace confluent
kubectl delete -f kraft.yaml --namespace confluent
helm uninstall confluent --namespace confluent
kubectl delete namespace confluent
```