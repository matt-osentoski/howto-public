# Create a multi-domain / wildcard cert for Confluent's CFK
This document describes how to create a certificate that can be used with Confluent's CFK.
The certificate will include external domains (external from k8s), as well as internal
k8s domains.

## Create the CA Root Certificate and Private Key
### Generate the Private Key
```bash 
openssl genrsa -des3 -out rootCA.key 2048
```

### Generate the Root Certificate
```bash 
openssl req -x509 -new -nodes -key rootCA.key -sha256 -days 1825 -out rootCA.pem
```

You will be prompted for a number of options:
``` 
Enter pass phrase for rootCA.key:
You are about to be asked to enter information that will be incorporated
into your certificate request.
What you are about to enter is what is called a Distinguished Name or a DN.
There are quite a few fields but you can leave some blank
For some fields there will be a default value,
If you enter '.', the field will be left blank.
-----
Country Name (2 letter code) [AU]:US
State or Province Name (full name) [Some-State]:MI
Locality Name (eg, city) []:SomeCity
Organization Name (eg, company) [Internet Widgits Pty Ltd]:SomeOrg LLC
Organizational Unit Name (eg, section) []:Org
Common Name (e.g. server FQDN or YOUR name) []:caroot.someorg.local
Email Address []:example@email.com
```
### Install the Certificate Authority on all your workstations
To prevent errors when using the certs generated below, make sure to install the CA certs into all
workstations that will be utilizing the certificates.


## Create a TLS Cert (Wildcard) from the CA Cert
A Wildcard certificate allows for multiple sub-domains to be used from the same certificate. (For example:
somedomain.com, www.somedomain.com, api.somedomain.com, etc.)

>(IMPORTANT!!!: Wildcards only work at a depth of one. For example: `*.somedomain.local` would
> work for `kafka.somedomain.local` b
> ut it would NOT work for `b1.kafka.somedomain.local`. To make
> this work you would either need an explicit Subject Alt Name (SAN) for `b1.kafka.somedomain.local` ' or another wildcard in
> the SAN with the correct depth, for example `*.kafka.somedomain.local`)

Reference: [https://tools.ietf.org/html/rfc2818#section-3.1](https://tools.ietf.org/html/rfc2818#section-3.1)

###  Create a TLS Wildcard cert with Multiple Domains
You can add additional domains to a `.cnf` file to give the certificate multiple Subject Alt Name (SAN) domains in the cert.
See the `multi-domain.local.cnf` file as an example.

### Create a Certificate Signing Request (CSR) 
To generate a certificate for our domain (with wildcard for subdomains) we will have to create a CSR
against the CA certificate.  Run the following commands:

```bash 
### First create the private key for the cert
openssl genrsa -out somedomain.local.key 2048

### Next create the CSR
openssl req -new -key somedomain.local.key -out somedomain.local.csr

### Finally, generate the wildcard cert.
openssl x509 -req -in somedomain.local.csr -CA rootCA.pem -CAkey rootCA.key -CAcreateserial \
    -out somedomain.local.crt -days 825 -sha256 -extfile somedomain.local.cnf    
```

Your certificate has been created as `somedomain.local.crt`,the private key you will use with your server is
`somedomain.local.key`.

### View the certificate
To view the certificate details, run the following command

```bash 
openssl x509 -in somedomain.local.crt -text -noout
```

### Chaining certificates
You can append the CA cert at the bottom of your generated domain cert to give the full cert chain. This
is sometimes required for some applications.

### View Chained Certificates
To view multiple certs chained together into one file, use a utility called `certtool`.  To install
and run, use the following commands:

```bash 
## If the tool isn't installed, run the following:
sudo apt install gnutls-bin

certtool -i < multiplecerts.pem
```