# Oracle Express Edition - Docker image creation and Helm deploy to k8s
This document will explain the steps required to:
- Build the Oracle Database as a Docker image
- Deploy the Oracle Database into Kubernetes using a Helm chart

These instructions are derived from the following URL:

[https://blogs.oracle.com/oraclemagazine/deliver-oracle-database-18c-express-edition-in-containers](https://blogs.oracle.com/oraclemagazine/deliver-oracle-database-18c-express-edition-in-containers)

## Create an Oracle DB Docker Image
### Download an Oracle RPM image
[https://www.oracle.com/database/technologies/xe-downloads.html](https://www.oracle.com/database/technologies/xe-downloads.html)

For example:
```
wget https://download.oracle.com/otn-pub/otn_software/db-express/oracle-database-xe-18c-1.0-1.x86_64.rpm
```

### Clone the Oracle docker image repo
```
git clone https://github.com/oracle/docker-images.git
```

### Copy the Oracle RPM image to the appropriate directory
Copy the Oracle image to the following directory from the repo you just cloned:
>(NOTE: You may have to change directories)

```
cp oracle-database-xe-18c-1.0-1.x86_64.rpm docker-images/OracleDatabase/SingleInstance/dockerfiles/18.4.0
```

### Build the Docker image using the build script
```
cd docker-images/OracleDatabase/SingleInstance/dockerfiles
./buildDockerImage.sh -v 18.4.0 -x
```

### Verify that the image built correctly
Run the following command and you should see your Oracle image listed
```
docker images
```

### (Optional) Push the Docker Image to a custom Registry
If your Kubernetes cluster uses private docker registry, tag the oracle image then push
the image to the registry. In this example, we'll assume the private docker registry address is
`192.168.6.2:5000`

```
docker tag oracle/database:18.4.0-xe 192.168.6.2:5000/oracle/database:18.4.0-xe
docker push 192.168.6.2:5000/oracle/database:18.4.0-xe
```

## Deploy Oracle using a Helm Chart

### Configure the Helm Chart
In the `oracle-xe` directory is a helm chart that is loosely based off the stable/mysql
chart and the default Helm 'create' templates. 

To configure the chart look in the `values.yaml` file, specifically at the Oracle
password and storage class settings.

### Deploy Oracle to Kubernetes using Helm
Run the following commands from this README file location:

```
kubectl create namespace oracle
helm upgrade --install --namespace oracle --values=oracle-xe/values.yaml oracle oracle-xe
```

## Test that the Database is running
To test the database from the Oracle pod, terminal into the pod then run
the following command:
```
sqlplus SYS/password@XE AS SYSDBA
```

Test the connection and you should now have access to the Oracle DB.

## Database Operations
An Oracle database has the following structure:

`Container Database (SID) => Pluggable Database (Service Name) -> Schema (User) -> Table`

The default SID created by the docker image is `XE`

### Create a new Schema
Oracle is structured with Container database containing one or more Pluggable databases:

Container Database (SID) => Pluggable Databases (Service Name)

To create a user, you will have to use a different login than mentioned above. Users should be created in the pluggable DB.
By default the pluggable database for our docker container should be `XEPDB1`. To find
the pluggable database name, run the following commands:

```sql
-- First login as SYS to the XE container database
sqlplus SYS/password@XE AS SYSDBA

-- Next, run the following commands:
COLUMN PDB_NAME FORMAT A15;
SELECT PDB_ID, PDB_NAME, STATUS FROM DBA_PDBS ORDER BY PDB_ID;

-- The output will look something like this:
    PDB_ID PDB_NAME        STATUS
---------- --------------- ----------
         2 PDB$SEED        NORMAL
         3 XEPDB1          NORMAL

-- In this case you can see that 'XEPDB1' is the pluggable database.
```

First, login to the Pluggable database using sqlplus:

```
sqlplus SYS/password@XEPDB1 AS SYSDBA
```

>(NOTE: Typically creating a schema is more complex than just creating a new user, in
>this example we will also be creating a table space and granting permissions.
>This is based on a stackoverflow answer: https://stackoverflow.com/a/23955869)

>(IMPORTANT!!!! Make sure all tablespaces are prefixed with the following directory or they
>will be transient when teh pod restarts `/opt/oracle/oradata/XE/XEPDB1/`)

```sql
-- First, to view existing Schemas, run the following command:
SELECT username FROM dba_users;

-- Next, let's view all tablespaces
SELECT tablespace_name FROM dba_tablespaces;

-- Create a new tablespace for our new schema
CREATE TABLESPACE myuser_tablespace DATAFILE '/opt/oracle/oradata/XE/XEPDB1/myuser_tablespace.dat' SIZE 50M AUTOEXTEND on;

-- Create a new temp. tablespace for our new schema
CREATE TEMPORARY TABLESPACE myuser_tablespace_temp TEMPFILE '/opt/oracle/oradata/XE/XEPDB1/myuser_tablespace_temp.dat' SIZE 10M AUTOEXTEND on;

-- Create a new user, referencing the table spaces we just created.
CREATE USER myuser IDENTIFIED BY password DEFAULT TABLESPACE myuser_tablespace TEMPORARY TABLESPACE myuser_tablespace_temp;

-- Grant permissions to the User
GRANT CREATE SESSION TO myuser;
GRANT CREATE TABLE TO myuser;
GRANT CREATE SEQUENCE TO myuser;
GRANT CREATE TRIGGER TO myuser;
GRANT CREATE VIEW TO myuser;
GRANT UNLIMITED TABLESPACE TO myuser;
```

Run the following command to create a table:
```sql
-- Create the table
CREATE TABLE books
(
    id INT NOT NULL,
    title VARCHAR2(50),
    description VARCHAR2(1000),
    author VARCHAR2(100),
    PRIMARY KEY (id)
);

-- View the Table
DESC books;

-- Insert a row
INSERT INTO books (id, title, author) VALUES (1, 'Fooled By Randomness', 'Nassim Taleb');
COMMIT;
```

### Login using the Schema/User account into the Pluggable database with SQL Plus
Run the following command to login using the user we created above:

```
sqlplus myuser/password@XEPDB1
```

### Login using the Schema/User account into the Pluggable database with a remote DB client
I use DBeaver to test the connection. Select `Oracle` as the connection type, then
enter the following params:

```
# Main tab
## Basic Sub tab
Host: localhost
Database: XEPDB1  (Service Name)
Authentication: Database Native
Username: myuser
Role: Normal
Password: password
```

## Upgrade the varchar2 from 4000 characters to 32k
>(NOTE: This operation requires that you login using `sqlplus / as sysdba` as the oracle user. However, the 
docker image runs as root. Use the following command to run the sqlplus command as the oracle user.)

```
su -p oracle -c "sqlplus / as sysdba"
ALTER SESSION SET CONTAINER=CDB$ROOT;
ALTER SYSTEM SET max_string_size=extended SCOPE=SPFILE;
SHUTDOWN IMMEDIATE;
STARTUP UPGRADE;
START $ORACLE_HOME/rdbms/admin/utl32k.sql
SHUTDOWN IMMEDIATE;
STARTUP;
```

### Validate that the varchar2 is now 32k
From a SQLPlus prompt run the following command:

```sql
SHOW PARAMETER max_string_size;

-- The Value should show EXTENDED
NAME                                 TYPE        VALUE
------------------------------------ ----------- ------------------------------
max_string_size                      string      EXTENDED
```

### Update the Pluggable databases to use varchar2 at 32k
Now that the container Database is updated to use varchar2 EXTENDED values, we can update all the pluggable
databases.

First, identify all the pluggable databases by running the following command:
```sql
SHOW pdbs;

-- The output should look something like this:
    CON_ID CON_NAME                       OPEN MODE  RESTRICTED
---------- ------------------------------ ---------- ----------
         2 PDB$SEED                       MOUNTED
         3 XEPDB1                         MOUNTED
```

Next, update the `PDB$SEED` database then the other Pluggable databases by running the following commands:

```sql
ALTER SESSION SET "_oracle_script"=true;
ALTER pluggable database pdb$seed open upgrade;
ALTER SESSION SET container=PDB$SEED;
@?/rdbms/admin/utl32k;
ALTER SESSION SET container=cdb$root;
ALTER pluggable database pdb$seed close;
ALTER pluggable database pdb$seed OPEN READ ONLY;
```

Finally, we can update the remaining pluggable databases to use varchar2 Extended values:

```sql
ALTER pluggable database XEPDB1 open upgrade;
ALTER SESSION SET container=XEPDB1;
@?/rdbms/admin/utl32k;
ALTER pluggable database XEPDB1 close;
ALTER pluggable database XEPDB1 open;
```

## Troubleshooting
### Container restart causes database startup issues
If the Pod restarts and the Database won't come up, shell into the pod's Oracle container and run 
the following commands:

Shell in as the Oracle user:
```
su -p oracle -c "sqlplus / as sysdba"
```

```sql
SHUTDOWN IMMEDIATE;
STARTUP;

-- Next, check to see if the pluggable databases came back online:
SHOW pdbs;
```