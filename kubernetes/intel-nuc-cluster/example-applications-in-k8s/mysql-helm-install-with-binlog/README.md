# Install Mysql using dynamic volume provisioning
>(NOTE: This now uses Bitnami instead of the Helm 'Stable' repo)

This document describes the steps to install a Mysql database using Helm and a dynamic volume provisioning as the
persistent store.  

Mysql will be setup to use and expose the binlog.  This is a
transaction log or write-ahead log that allows a system like Kafka Connect to synchronize this
database with other data stores.  

Author: Matt Osentoski  
E-mail: matt.osentoski@gmail.com

## Dependencies
### Storage Class
It's assumed you already have a Kubernetes Storage class setup.  This example will use the
storage class created using Rook.io / Ceph as the storage system.  It is also assumed that
the storage class name that was created is called `rook-ceph-block`.

See the following README file for details around installing Rook.io and Ceph:
[https://bitbucket.org/matt-osentoski/howto-public/src/master/kubernetes/intel-nuc-cluster/dynamic-volume-provisioning/storage-classes/rook.io/README.mdk](https://bitbucket.org/matt-osentoski/howto-public/src/master/kubernetes/intel-nuc-cluster/dynamic-volume-provisioning/storage-classes/rook.io/README.md)

### Helm
Helm should already be setup on your cluster.  If not, take a look at the following README:
[https://bitbucket.org/matt-osentoski/howto-public/src/master/kubernetes/intel-nuc-cluster/helm/README.md]()

### (Optional) Kafka Connect
Kafka connect will allow the binlog to synchronize this database with other datastores. For information about setting up Kafka Connect, look at the following
README file: [https://bitbucket.org/matt-osentoski/howto-public/src/master/big-data/intel-nuc-cluster/optional/kafka-connect-install.md]()


## Make changes to the values.yaml file
The values.yaml file in this directory allow for custom settings to the Mysql database.  Note that the following
changes have been made from the default file located here:

[https://github.com/bitnami/charts/blob/main/bitnami/mysql/values.yaml](https://github.com/bitnami/charts/blob/main/bitnami/mysql/values.yaml)


```yaml
mysqlRootPassword: password

persistence:
  enabled: true
  storageClass: rook-ceph-block
  
#### First 5 lines are related to the binlog  
primary:
  configuration: |-
    [mysqld]
    log-bin=bin.log
    log-bin-index=bin-log.index
    max_binlog_size=100M
    binlog_format=row
    default_authentication_plugin=mysql_native_password
    skip-name-resolve
    explicit_defaults_for_timestamp
    basedir=/opt/bitnami/mysql
    plugin_dir=/opt/bitnami/mysql/lib/plugin
    port=3306
    socket=/opt/bitnami/mysql/tmp/mysql.sock
    datadir=/bitnami/mysql/data
    tmpdir=/opt/bitnami/mysql/tmp
    max_allowed_packet=16M
    bind-address=*
    pid-file=/opt/bitnami/mysql/tmp/mysqld.pid
    log-error=/opt/bitnami/mysql/logs/mysqld.log
    character-set-server=UTF8
    collation-server=utf8_general_ci
    slow_query_log=0
    slow_query_log_file=/opt/bitnami/mysql/logs/mysqld.log
    long_query_time=10.0

    [client]
    port=3306
    socket=/opt/bitnami/mysql/tmp/mysql.sock
    default-character-set=UTF8
    plugin_dir=/opt/bitnami/mysql/lib/plugin
    
    [manager]
    port=3306
    socket=/opt/bitnami/mysql/tmp/mysql.sock
    pid-file=/opt/bitnami/mysql/tmp/mysqld.pid

service:
   type: LoadBalancer   
```

## Install Mysql using Helm.
>(NOTE: These instructions are derived from: [https://github.com/helm/charts/tree/master/stable/mysql](https://github.com/helm/charts/tree/master/stable/mysql))

Install the helm chart, pointing to the Storage class:
```
kubectl create namespace mysql
helm repo add bitnami https://charts.bitnami.com/bitnami
helm upgrade --install --namespace mysql --values values.yaml mysql-binlog bitnami/mysql
```

>(NOTE: The `values.yaml` is located in the same directory as this README file)

