# Install KeyCloakX into Kubernetes
>(NOTE: The difference between KeyCloak and KeyCloakX is that KeyCloakX is a rewrite using quarkus)
This document describes how to install the KeyCloak Identity server onto a Kubernetes cluster using Helm.

>(NOTE: KeyCloak.X is uses Quarkus, making a more natural fit for Cloud Native architectures. The 
> older KeyCloak used Wildfly and didn't have the option of native compilation)

>(SOURCES:
> - [https://www.keycloak.org/2020/12/first-keycloak-x-release.adoc](https://www.keycloak.org/2020/12/first-keycloak-x-release.adoc) 
> - [https://github.com/codecentric/helm-charts/tree/master/charts/keycloakx](https://github.com/codecentric/helm-charts/tree/master/charts/keycloakx)
>)

Author: Matt Osentoski  
E-mail: matt.osentoski@gmail.com

## Dependencies
### Install PostgreSQL
Run the following commands to setup the namespace and install the database. 
>(NOTE: You can modify the `postgresql-value-overrides.yaml` file to suit your needs)

```shell
kubectl create namespace keycloak

helm repo add bitnami https://charts.bitnami.com/bitnami
helm repo update

helm upgrade --install --namespace keycloak --values postgresql-value-overrides.yaml \
  keycloak-db bitnami/postgresql 
```

## Update the configuration file
The Helm chart project values for KeyCloak can be viewed here:
[https://github.com/codecentric/helm-charts/blob/master/charts/keycloakx/values.yaml](https://github.com/codecentric/helm-charts/blob/master/charts/keycloakx/values.yaml)

Modify the `values-overrides.yaml` to suit your needs.  In my case, I will be adding Ingress configurations.
See the section below about setting up a TLS secret.

### (Optional: Create a secret with TLS cert/private key, if you're using an Ingress with TLS)
>(NOTE: If you use this, set the ingress 'enabled' flag to 'true')

>(NOTE 2: View the README file under /kubernetes/intel-nuc-cluster/certificate/authority/README.md for more
> details in the setup of a self signed cert with included (bundled) CA cert.)

```shell 
kubectl create secret tls jaroof-cert-secret --cert=bundle-jaroof.local.crt --key=jaroof.local.key --namespace keycloak
```

## Install the Helm chart
Run the following commands to install Keycloak using Helm. I'm using the `value-overrides.yaml` settings
to customize the installation.
```
helm repo add codecentric https://codecentric.github.io/helm-charts
helm upgrade --install -f value-overrides.yaml --namespace keycloak --create-namespace \
    keycloak codecentric/keycloakx
```

## Verify the server is running
>(NOTE: This assumes we're using an ingress with the domain name `keycloak.jaroof.local` with TLS)

To access the admin console, go to the following URL:
https://keycloak.jaroof.local/auth/admin
username: admin
password: secret

Go to the `keycloak-configuration.md` file to learn how to setup the Identity server.

## Realm Login screen
Once a realm has been setup using the `keycloak-configuration.md` instructions, mentioned above, you can access the
realm's login page using the following URL

`http://192.168.6.154:8080/auth/realms/myRealmName/account`

Where 'myRealmName' is the name of the realm you created.

### View the keycloak user password
If you have trouble logging in, use the following command to view the password for the `keycloak` user:
```
kubectl get secret --namespace keycloak keycloak-http -o jsonpath="{.data.password}" | base64 --decode; echo
```

