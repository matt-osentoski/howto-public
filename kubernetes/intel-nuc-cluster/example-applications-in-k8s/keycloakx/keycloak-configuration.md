# Configure the KeyCloak server
This document is based on the following URL:
https://www.keycloak.org/docs/latest/getting_started/index.html

Author: Matt Osentoski  
E-mail: matt.osentoski@gmail.com

## Login to the admin console
http://192.168.6.154:8080
username: keycloak
password: password

## Create a Realm
(From Stackoverflow)
A security realm is a mechanism used for protecting Web application resources. It gives you the ability to protect a
resource with a defined security constraint and then define the user roles that can access the protected resource.

1. From the Master drop-down menu on the upper left-hand side, click `Add Realm`.
2. Type `example` in the Name field and click Create.

# Users and Roles
## Create a user
1. From the left-hand menu, select `Users`
2. Click the 'Add user' button on the right-hand side
3. Enter a 'Username' and click 'Save'
4. Click the 'Credentials' tab and enter a password
5. Slide the 'Temporary' button to 'off'
6. Press the 'Enter' button.  (NOTE: This is important, there is no save button on this tab you have to hit enter to save the changes)

## Verify the User can login
Go to the following URL:
(NOTE: Your IP address may be different)
http://192.168.6.154:8080/auth/realms/fhir/account

Login with the user you created in the last step.

## Create a role
1. From the left-hand menu, select `Roles`
2. Click the 'Add Role' button on the right-hand side
3. Enter a role name and description
4. Click 'Save'

## Add a user to a role
1. From the left-hand menu, select `Users`
2. Click 'View All Users' and select the user you created in the section above and click 'Edit'
3. Click the 'Role Mappings' tab
4. Select the role you want to add then click 'Add Selected'

# Clients
## Register a client application
1. From the left-hand menu, select `Clients`
2. Click the 'Create' button on the right-hand side
3. Enter an identifier the application under 'Client Id'  (ex: hapi-fhir)
4. For 'Client Protocol', select 'openid-connect'
5. In the 'Root URL' field enter the base path of your application. (ex: http://192.168.6.151:8080/fhir/)
6. Click 'Save'