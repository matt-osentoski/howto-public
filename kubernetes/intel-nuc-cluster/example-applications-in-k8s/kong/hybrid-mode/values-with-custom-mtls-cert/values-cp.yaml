deployment:
  kong:
    enabled: true
  serviceAccount:
    create: true

env:
  role: control_plane
  cluster_mtls: pki
  cluster_ca_cert: /etc/secrets/ca-heavymetalcloud-lan-tls/tls.crt
  cluster_cert: /etc/secrets/kong-cluster-cert/tls.crt
  cluster_cert_key: /etc/secrets/kong-cluster-cert/tls.key
  ### This should match the mTLS CN. By default, the value is 'kong_clustering'.
  cluster_server_name: kong-cluster.heavymetalcloud.lan
  cluster_listen: 0.0.0.0:8005
  cluster_telemetry_listen: 0.0.0.0:8006
  admin_gui_url: https://kong-manager.heavymetalcloud.lan
  admin_gui_host: kong-manager.heavymetalcloud.lan
  admin_gui_api_url: https://kong-admin.heavymetalcloud.lan
  database: "postgres"
  pg_host: "kong-cp-postgresql.kong.svc.cluster.local"
  pg_user:
    valueFrom:
      secretKeyRef:
        name: postgres-db-secrets
        key: user
  pg_database: kong
  pg_password:
    valueFrom:
      secretKeyRef:
        name: postgres-db-secrets
        key: password
  # Kong manager password
  password: password
  log_level: debug
  trusted_ips: "0.0.0.0/0,::/0"
  router_flavor: "traditional"
  nginx_worker_processes: "2"
  proxy_access_log: /dev/stdout
  admin_access_log: /dev/stdout
  admin_gui_access_log: /dev/stdout
  portal_api_access_log: /dev/stdout
  proxy_error_log: /dev/stderr
  admin_error_log: /dev/stderr
  admin_gui_error_log: /dev/stderr
  portal_api_error_log: /dev/stderr
  prefix: /kong_prefix/

image:
  repository: kong
  tag: "3.8"
  pullPolicy: IfNotPresent

# Specify Kong admin API service and listener configuration
admin:
  enabled: true
  type: NodePort
  annotations:
    konghq.com/protocol: https
  labels: {}
  http:
    enabled: false
    servicePort: 8001
    containerPort: 8001
    parameters: []
  tls:
    # Enable HTTPS listen for the admin API
    enabled: true
    servicePort: 8444
    containerPort: 8444
    parameters:
      - http2
  ingress:
    enabled: true
    ingressClassName: nginx
    # TLS secret name.
    tls: heavymetalcloud-lan-tls
    hostname: kong-admin.heavymetalcloud.lan
    annotations:
      nginx.ingress.kubernetes.io/backend-protocol: "HTTPS"
    path: /
    pathType: Prefix

# Specify Kong status listener configuration
# This listen is internal-only. It cannot be exposed through a service or ingress.
status:
  enabled: true
  http:
    enabled: true
    containerPort: 8100
  tls:
    enabled: false
    containerPort: 8543

# Name the kong hybrid cluster CA certificate secret
clusterCaSecretName: "ca-heavymetalcloud-lan-tls"

# Specify Kong cluster service and listener configuration
cluster:
  enabled: true
  annotations:
    konghq.com/protocol: https
  tls:
    enabled: true
    servicePort: 8005
    containerPort: 8005
    parameters: []
  type: LoadBalancer
  ingress:
    enabled: false

# Specify Kong proxy service configuration
#### NOTE: The proxy is only used for Data Planes
proxy:
  enabled: false

### These secrets will be mounted at: /etc/secrets/<SECRET_NAME>
### In the running Kong Gateway container.
secretVolumes:
  - kong-cluster-cert
  - heavymetalcloud-lan-tls
  - ca-heavymetalcloud-lan-tls
  - mtls-bundle-tls

# -----------------------------------------------------------------------------
# Postgres sub-chart parameters
# -----------------------------------------------------------------------------
postgresql:
  enabled: true
  auth:
    username: kong
    database: kong
    password: password
  image:
    # use postgres < 14 until is https://github.com/Kong/kong/issues/8533 resolved and released
    # enterprise (kong-gateway) supports postgres 14
    tag: 13.11.0-debian-11-r20
  service:
    ports:
      postgresql: "5432"

# Kong pod count.
# It has no effect when autoscaling.enabled is set to true
replicaCount: 1

# Enable autoscaling using HorizontalPodAutoscaler
# When configuring an HPA, you must set resource requests on all containers via
# "resources" and, if using the controller, "ingressController.resources" in values.yaml
autoscaling:
  enabled: true
  minReplicas: 2
  maxReplicas: 5
  behavior: {}
  ## targetCPUUtilizationPercentage only used if the cluster doesn't support autoscaling/v2 or autoscaling/v2beta
  targetCPUUtilizationPercentage:
  ## Otherwise for clusters that do support autoscaling/v2 or autoscaling/v2beta, use metrics
  metrics:
    - type: Resource
      resource:
        name: cpu
        target:
          type: Utilization
          averageUtilization: 80

manager:
  # Enable creating a Kubernetes service for Kong Manager
  enabled: true
  type: NodePort
  annotations:
    konghq.com/protocol: https
  http:
    enabled: false
    servicePort: 8002
    containerPort: 8002
    parameters: []
  tls:
    # Enable HTTPS listen for Kong Manager
    enabled: true
    servicePort: 8445
    containerPort: 8445
    parameters:
      - http2
  ingress:
    enabled: true
    ingressClassName: nginx
    tls: heavymetalcloud-lan-tls
    hostname: kong-manager.heavymetalcloud.lan
    annotations:
      nginx.ingress.kubernetes.io/backend-protocol: "HTTPS"
    path: /
    pathType: ImplementationSpecific

portal:
  # Enable creating a Kubernetes service for the Developer Portal
  enabled: true
  type: NodePort
  annotations:
    konghq.com/protocol: https
  http:
    enabled: true
    servicePort: 8003
    containerPort: 8003
    parameters: []
  tls:
    enabled: true
    servicePort: 8446
    containerPort: 8446
    parameters:
      - http2
  ingress:
    enabled: true
    ingressClassName: nginx
    tls: heavymetalcloud-lan-tls
    hostname: kong-portal.heavymetalcloud.lan
    annotations: {}
    path: /
    pathType: ImplementationSpecific

portalapi:
  # Enable creating a Kubernetes service for the Developer Portal API
  enabled: true
  type: NodePort
  annotations:
    konghq.com/protocol: https
  http:
    enabled: true
    servicePort: 8004
    containerPort: 8004
    parameters: []
  tls:
    enabled: true
    servicePort: 8447
    containerPort: 8447
    parameters:
      - http2
  ingress:
    enabled: true
    ingressClassName: nginx
    tls: heavymetalcloud-lan-tls
    hostname: kong-portalapi.heavymetalcloud.lan
    annotations: {}
    path: /
    pathType: ImplementationSpecific

clustertelemetry:
  enabled: true
  annotations:
    konghq.com/protocol: https
  labels: {}
  tls:
    enabled: true
    servicePort: 8006
    containerPort: 8006
    parameters: []
  type: ClusterIP
  ingress:
    enabled: true
    ingressClassName: nginx
    tls: heavymetalcloud-lan-tls
    hostname: kong-clustertelemetry.heavymetalcloud.lan
    annotations: {}
    path: /
    pathType: ImplementationSpecific
