# Install Kong in Hybrid Mode - With custom mTLS cert
>(NOTE: These instructions are a fork of [../README.md](../README.md) specifically for custom mTLS certs.)

Kong Hybrid mode splits the control plane and data plane(s) into separate clusters and
helm installs.

>(Kong REFERENCES:
> - https://github.com/Kong/charts/blob/main/charts/kong/README.md#hybrid-mode
> - https://github.com/Kong/charts/blob/main/charts/kong/README.md#control-plane-node-configuration
> - https://github.com/Kong/charts/blob/main/charts/kong/README.md#data-plane-node-configuration
> - https://github.com/Kong/charts/blob/main/charts/kong/values.yaml)

>(Other REFERNCES:
> - https://docs.konghq.com/gateway/latest/production/deployment-topologies/hybrid-mode/setup/
> - https://github.com/ambidextrous-dev/kong-helm/tree/develop/k8-manifests/test
> - https://ambidextrous-dev.medium.com/kong-gateway-helm-deployment-in-kubernetes-hybrid-mode-9c9ca9a76308
> - https://github.com/Kong/charts/blob/main/charts/kong/example-values/minimal-kong-hybrid-control.yaml
> - https://github.com/Kong/charts/blob/main/charts/kong/example-values/minimal-kong-hybrid-data.yaml)

## Set up the Control Plane
### Create a namespace
```shell
kubectl create namespace kong
```

### Create the mTLS cert and secret
Using a custom CA/Subordinate cert, create a new server (leaf) cert for mTLS

>(IMPORTANT!!! Since you are using a custom cert with its own Common Name (CN), you will have to change the Kong
> env setting `cluster_server_name` to whatever the new CN name is. Otherwise, Kong will assume that the CN is the default `kong_clustering`)

>(NOTE: It's assumed that the server cert is called `cluster.crt` with a key called `cluster.key`)
Next, create a secret in k8s
```shell
kubectl create secret tls kong-cluster-cert --cert=cluster.crt --key=cluster.key --namespace kong
```

### Create the Ingress secret
>(NOTE: `tls.crt` and `tls.key` should point to a wildcard cert for the `heavymetalcloud.lan` domain)
```shell
kubectl create secret tls heavymetalcloud-lan-tls --cert=tls.crt --key=tls.key --namespace kong
```

### Create the CA Cert
>(NOTE: `tls.crt` and `tls.key` should point to a wildcard cert for the `heavymetalcloud.lan` domain)
```shell
kubectl create secret generic ca-heavymetalcloud-lan-tls --namespace kong --from-file=tls.crt=cacert.crt
```

### (OPTIONAL) Create a Bundled cert with CA/subordinates
>(NOTE: This is a fully bundled cert with the server cert and all CA/subordinates. It isn't used now, but here for reference)
```shell
kubectl create secret generic mtls-bundle-tls --namespace kong --from-file=mtls-bundle.crt=mtls-bundle.crt
```

### Create the DB authentication secret
```shell
kubectl create secret generic postgres-db-secrets --from-literal=user=kong --from-literal=password=password --namespace kong
```

### Modify the Values file
Update the Control Plane values file. An example is located in this directory: [values-cp.yaml](values-cp.yaml)

### Install the Control Plane using Helm
```shell
helm repo add kong https://charts.konghq.com
helm repo update

helm upgrade --install --values values-cp.yaml kong-cp kong/kong --namespace kong
```


## Set up the Data Plane
### Create a namespace
```shell
kubectl create namespace kong
```

### Create the mTLS secret in the Data Plane
>(NOTE: You will use the same cert/key you created for the control plane)

```shell
kubectl create secret tls kong-cluster-cert --cert=cluster.crt --key=cluster.key --namespace kong
```

### Create the cluster secret
>(NOTE: `tls.crt` and `tls.key` should point to a wildcard cert for the `heavymetalcloud.lan` domain)
```shell
kubectl create secret tls heavymetalcloud-lan-tls --cert=tls.crt --key=tls.key --namespace kong
```

### Create the CA Cert
>(NOTE: `tls.crt` and `tls.key` should point to a wildcard cert for the `heavymetalcloud.lan` domain)
```shell
kubectl create secret generic ca-heavymetalcloud-lan-tls --namespace kong --from-file=tls.crt=cacert.crt
```

### Create a Bundled cert with CA/subordinates
>(NOTE: `tls.crt` and `tls.key` should point to a wildcard cert for the `heavymetalcloud.lan` domain)
```shell
kubectl create secret generic mtls-bundle-tls --namespace kong --from-file=mtls-bundle.crt=mtls-bundle.crt
```

### Modify the Values file
Update the Data Plane values file. An example is located in this directory: [values-dp.yaml](values-dp.yaml)

### Install the Data Plane using Helm
```shell
helm upgrade --install --values values-dp.yaml kong-dp kong/kong --namespace kong
```


## Smoke Test using an External endpoint
For a quick smoke test, we'll use an external mock service to test Kong. For a
real test, see the next section below, `Testing the Data plane with an internal app`

>(REFERENCE:
> - [https://docs.konghq.com/gateway/latest/kong-manager/get-started/services-and-routes/](https://docs.konghq.com/gateway/latest/kong-manager/get-started/services-and-routes/)
>)

### Set up the Gateway and Route in Kong Manager
From the Kong manager GUI, go to the following menu item: 

**Gateway Services**
- Click `New Gateway Service`
- **Name** - You can call the gateway anything, like `example gateway`
- **Full URL** - We'll use Kong's httpbin service in this example, `httpbin.konghq.com`
- Click the `Save` button

Next, let's create a Route that will be associated with the Gateway Service.  
From the Kong manager GUI, go to the following menu item:

**Routes**
- Click `New Route`
- **Name** - You can call the route anything, like `example route`
- **Service** - This is a dropdown that should contain the Gateway service we just created
- **Path** - We'll use `/mock` for this example. 
- Click the `Save` button

### Testing the Kong Ingress
First, let's find the IP address for the Data Plane service. With your KUBECONFIG pointing to
the data plane cluster, run this command to find the IP address:

```shell
kubectl get service --namespace kong
```

You should see an output that looks like this:
``` 
NAME                 TYPE           CLUSTER-IP      EXTERNAL-IP   PORT(S)         AGE
kong-dp-kong-proxy   LoadBalancer   10.43.245.186   172.16.1.81   443:31958/TCP   15h
```

The `EXTERNAL-IP` (172.16.1.81) will be the IP address exposed by Kong. 

You can now use curl to test the endpoint end-to-end:

```shell
curl -kvvv https://172.16.1.81/mock/
```

If you see HTML coming back, your Kong setup is working!

## Testing the Data plane with an internal app
Now it's time for a real test. We will be creating an application in the same
k8s cluster as the data plane. We'll then set up Kong to create an ingress to this application
using the Manager GUI. Finally, we'll test connectivity

### Set up a Test Client app in the Data Plane Cluster
First, let's install a simple Nginx container using Helm:

>(NOTE: This should be installed on the same cluster as the Kong Data Plane)

```shell
helm upgrade --install --namespace example --create-namespace --set service.type=ClusterIP my-nginx-test oci://registry-1.docker.io/bitnamicharts/nginx
```

This application will have an internal hostname of: `my-nginx-test.example.svc.cluster.local`

You will need this to configure the `Gateway Service` in Kong Manager.

### Set up DNS for the Test Client
Using the command `kubectl get service --namespace kong`, we found that the IP address that exposes the Kong Data Plane is
`172.16.1.81`

We will now set up a DNS record for our test app that points to this IP address. For my exmaple, I will use the following:

`kong-dp-client-example.heavymetalcloud.lan` points to `172.16.1.81`

>(NOTE: It's assumed you have a DNS server that can handle this entry. It's also assumed that you already
> have TLS certificates and CA certs for this domain. In my case, I'm using a wildcard cert for the `*.heavymetalcloud.lan` domain,
> which is issued by a CA and Subordinate cert chain. I have the cert files for everything including the private key for
> the leaf cert)

### Set up CA certificates in Kong Manager
From the Kong manager GUI, go to the following menu item:

**CA Certificates**
- Click `New CA Certificate`
- **Cert** - Paste your CA cert (in PEM format)
- Click the `Save` button

Repeat this process if your cert chain contains any subordinate (intermediate) certificates.

### Create a Leaf Certificate in Kong Manager
The Leaf certificate is the actual TLS cert of the end point. It was issued by the CA certs that
we just set up in the last section.

From the Kong manager GUI, go to the following menu item:

**Certificates**
- Click `New CA Certificate`
- **Cert** - Paste your CA cert (in PEM format)
- **Key** - Paste your cert key (in PEM format)
- **SNI** - Paste the domain name of the leaf certificate. In my example, this would be `kong-dp-client-example.heavymetalcloud.lan`
- Click the `Save` button

>(IMPORTANT!!!! The SNI name MUST be the same as the domain name of the endpoint!!)

>(NOTE: By entering an SNI, you're also creating an SNI entry in Kong Manager. )

### Set up the Gateway and Route in Kong Manager
From the Kong manager GUI, go to the following menu item:

**Gateway Services**
- Click `New Gateway Service`
- **Name** - You can call the gateway anything, like `example gateway`
- Click the `Protocol, Host, Port and Path` radio button
- **Protocol** - `HTTP` - Our test client uses HTTP from within the k8s cluster
- **Host** - We'll use the internal k8s hostname for our Test Client, `my-nginx-test.example.svc.cluster.local`
- **Path** - We'll use the root path here `/`
- **Port** - `80` - The Nginx Test client uses port 80 internally to k8s
- Click the `Save` button

Next, let's create a Route that will be associated with the Gateway Service.  
From the Kong manager GUI, go to the following menu item:

**Routes**
- Click `New Route`
- **Name** - You can call the route anything, like `example route`
- **Service** - This is a dropdown that should contain the Gateway service we just created
- **Protocols** - (DEFAULT) - We'll leave this at `HTTP, HTTPS`
- **Paths** - We'll use `/` for this example.
- Click `Hosts`
- **Hosts** - We'll enter the DNS host that we set up earlier. This is the external hostname. `kong-dp-client-example.heavymetalcloud.lan`
- Click `Methods`
- **Methods** - You can just select all the methods here. Although we will only be testing `GET`
- Click `SNIs`
- **SNI** - Enter the public facing domain name here, that we added in the cert section. In my case this is `kong-dp-client-example.heavymetalcloud.lan`
- Click the `Save` button

### Testing with Curl or a Browser
Now that everything is wired up, you should be able to test the service end-to-end from your
workstation. Run the following command or put the URL in your browser

```shell
curl -k https://kong-dp-client-example.heavymetalcloud.lan
```

You should see the NGINX Welcome page. Now you know Kong is set up and working completely in
Hybrid mode!