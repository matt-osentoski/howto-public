# Install Juptyer Hub using Helm
Jupyter is a notebook, often used for developing and deploying analytics code.  These instructions are derived from: 

[https://zero-to-jupyterhub.readthedocs.io/en/latest/setup-jupyterhub.html](https://zero-to-jupyterhub.readthedocs.io/en/latest/setup-jupyterhub.html)


## Install the Jupyter Hub Chart
```
helm repo add jupyterhub https://jupyterhub.github.io/helm-chart/
helm repo update
helm upgrade --install jhub jupyterhub/jupyterhub --namespace jupyter --version=0.8.0 --values config.yaml
```

### Logging in
To view the IP address that is exposed, run the following command:
```
kubectl --namespace=jupyter get svc proxy-public
```

The username and password will be `admin`

## Remove the chart
```
helm del --purge jhub
kubectl delete namespace jupyter
```