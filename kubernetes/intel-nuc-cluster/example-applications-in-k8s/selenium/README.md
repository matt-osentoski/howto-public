# Install Selenium Grid using Helm

## Modfiy the values.yaml file
For my install, I changed the following `value-overrides.yaml` settings:
```

```

## Install the Selenium Chart
>( NOTE: There appears to be a problem with the 1.0.9 version of the helm chart
use a newer or older version.  In the example below, I'm using '1.0.8')

```
helm upgrade --install --namespace selenium --values values.yaml,value-overrides.yaml selenium stable/selenium --version=1.0.8
```

## Creating a Selenium test case
### Install Selenium IDE
Use the following link to get a legitimate link to the IDE plug-in:

[https://www.seleniumhq.org/download/](https://www.seleniumhq.org/download/)

### Create the scripting code
Run the plugin and export the test case code.  Run this code against the 
Selenium Hub URL.  By default, this should be exposed using the k8s loadbalancer. 

## Remove the chart
```
helm del --purge selenium
kubectl delete namespace selenium
```