# Install MinIO using the Operator via Helm

## Install the Operator

```shell 
helm repo add minio https://operator.min.io/
kubectl create namespace minio-operator
helm install --namespace minio-operator minio-operator minio/operator
```

## Make sure Krew is install for Kubectl
Install the Kubectl Krew plugin using the instructions below.  You will have to update your PATH and reload
the shell you're using for the plugin to show up.
[https://krew.sigs.k8s.io/docs/user-guide/setup/install/](https://krew.sigs.k8s.io/docs/user-guide/setup/install/)

Then run the following command:

```shell
kubectl krew install minio
```

## Launch the Operator Management Console

Run the following command and make a note of the JWT token.  The JWT will be used to login to the console.

```shell
kubectl minio proxy -n minio-operator
```

You should now be able to access the console at:
[http://localhost:9090](http://localhost:9090)

## Create a Tenant using the Operator Management Console
Using the step listed above, login to the Operator Management Console at: [http://localhost:9090](http://localhost:9090)

>(NOTE: Again, use the JWT token to login)

- Click the `Create Tenant` button
- Fill out all the fields, selecting an existing namespace.  Also select an available StorageClass for persistent storage.
- Click `Create` after all the fields have been selected

>(IMPORTANT!!!! Make sure to download or write down the Console Credentials!!!)

## (OPTIONAL) Create a Tenant manually
MinIO can be segmented into Tenants for different storage needs.  Create a starting tenant

```shell 
kubectl create namespace minio-tenant

helm install --namespace minio-tenant tenant minio/tenant
```