### Mysql Database example using Ceph as the dynamic volume

In this directory, look for the `mysql-rook-deployment.yaml` file.  This file shows how to create a Mysql database using
Ceph with Rook.

Run the following command to create the database:
```
kubectl create -f mysql-rook-deployment.yaml
```