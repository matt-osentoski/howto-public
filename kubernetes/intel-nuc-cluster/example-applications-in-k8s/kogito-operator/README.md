# Install Kogito using the Kubernetes Operator
This document explains how to setup and install the Kogito application
using the Operator. This document assumes you already have Kafka and MongoDB
installed in your cluster.

References:
- [https://kogito.kie.org/](https://kogito.kie.org/)
- [https://github.com/kiegroup/kogito-cloud-operator](https://github.com/kiegroup/kogito-cloud-operator)
- [https://docs.jboss.org/kogito/release/1.1.0/html_single/#proc-kogito-deploying-on-kubernetes_kogito-deploying-on-openshift](https://docs.jboss.org/kogito/release/1.1.0/html_single/#proc-kogito-deploying-on-kubernetes_kogito-deploying-on-openshift)

## Install the Operator
```bash
kubectl create namespace kogito-operator-system
kubectl apply -n kogito-operator-system -f "https://github.com/kiegroup/kogito-cloud-operator/releases/download/v1.1.0/kogito-operator.yaml"

```

## Install the CLI
You can download and unpack the CLI binaries here:

[https://github.com/kiegroup/kogito-cloud-operator/releases](https://github.com/kiegroup/kogito-cloud-operator/releases)

## Uninstall Kogito Operator
```bash 
kubectl delete -n kogito-operator-system -f "https://github.com/kiegroup/kogito-cloud-operator/releases/download/v1.1.0/kogito-operator.yaml"
```