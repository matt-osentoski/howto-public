# Local Storage Chart
This chart is used to dynamically generate persistent volumes (PV) which will
be used with a storage class to mimick dynamic storage.  At some point, Kubernetes
may implement dynamic local storage, which would deprecate this document and technique.

## Create local directories
Since this isn't a true dynamic volume provisioner you will have to manually
create the Persistent Volume (PV) folders on every node.  Run the following command from 
each node:

```
## Change the the filePathPrefix directory 
cd /var/lib/

## Create all the folders.  Note '30' below corresponds the volume.count in the values.yaml
sudo mkdir -p local-storage/pv{1..30}
sudo chgrp -R hduser /var/lib/local-storage
sudo chmod -R 775 /var/lib/local-storage
```

## Install the helm chart
Modify the `values.yaml` file to match the number of nodes and volumes to be created. Run
the following command:

```
helm upgrade --install --values local-storage/values.yaml local-storage local-storage/
```

## Teardown
To remove the volumes run the following command:

```
helm del --purge local-storage
```

Next, you will have to manually delete all the PV volumes on each host using the following
command:

```
sudo rm -Rf /var/lib/local-storage
```