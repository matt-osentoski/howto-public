# Kafka / Zookeeper cluster using Bitnami Charts

## Add the Bitnami repo
```
helm repo add bitnami https://charts.bitnami.com/bitnami
```

## Create a cluster without Authentication
```
kubectl create namespace kafka
helm upgrade --install --values values-nodeport-noauth.yaml --namespace kafka bit-kafka bitnami/kafka
```

## Fix for service NodePorts
There seems to currently be a bug in the helm charts, where the NodePort objects are being configured
incorrectly.  The 'targetPort' is being assigned the same value as the 'nodePort', when it should have
the 'port' value that is open on the pod.  For example, 9094. Using k9s or a similar tool. Update each NodePort
object, changing the `targetPort` from '3109x' to '9094':

>(IMPORTANT!!! Perform this operation on all 3 NodePorts, otherwise your connection will
>only be established every 3rd connection attempt to the service load balancer)

``` 
  ports:
  - name: tcp-kafka
    nodePort: 31090
    port: 9094
    protocol: TCP
    ### Change the target port from 3109x to 9094
    #targetPort: 31090
    targetPort: 9094
```

## Installing additional Kafka services
The Bitnami helm charts only include ZookKeeper and Kafka.  To add additional
confluent services like KSQL or Kafka Connect, you can run the following commands,
pointing to the Kafka advertised broker addresses.

>NOTE: The following examples will assume your advertised broker addresses are:
>```
>192.168.6.6:31090, 192.168.6.7:31091, 192.168.6.8:31092
>```

### Clone the Confluent Helm chart repo
Unfortunately, the individual confluent components aren't accessible from a chart repository. 
You will have to clone the repo and work from there...
```
git clone https://github.com/confluentinc/cp-helm-charts.git
```

### Values files
The values files for the individual services live in the cp-values directory from this README.md file.
You can make changes to each service in these files

### Kafka Schema Registry
```
helm upgrade --install --namespace kafka  --values cp-values/cp-schema-registry-values.yaml cp-schema-registry cp-helm-charts/charts/cp-schema-registry
```

### Kafka Connect
```
helm upgrade --install --namespace kafka  --values cp-values/cp-kafka-connect-values.yaml cp-kafka-connect cp-helm-charts/charts/cp-kafka-connect
```

### KSQL
```
helm upgrade --install --namespace kafka  --values cp-values/cp-ksql-values.yaml cp-ksql cp-helm-charts/charts/cp-ksql-server
```
