# Custom Zookeeper Helm Chart. Based on Bitnami

>(IMPORTANT!!!!  At this point I'm using the confluent charts for my kafka stack.  This
chart has a good example showing how to create nodeports dynamically from multiple zookeeper
brokers.  /templates/svc-nodeports.yaml  For that reason, I'm keeping this example around.)

This Helm chart depends on the Bitnami Zookeeper chart and adds static Nodeport services for
each ZK server that is configured.

(Reference: [https://github.com/bitnami/charts/tree/master/bitnami/zookeeper](https://github.com/bitnami/charts/tree/master/bitnami/zookeeper))


## Build and install the chart
>(NOTE: When changes are made to the `values.yaml` file, you may have to rerun
the `helm dependency update` command)

```
# Change directory into the 'zookeeper' chart folder

helm repo add bitnami https://charts.bitnami.com/bitnami
helm dependency update
helm upgrade --install --values values.yaml --namespace kafka zk .
```

## Push the chart to the Helm Chart Museum
Assuming you've setup a chart museum server with a repo name of `museum`, run
the following command to push this chart into your repo:

```
## This assumes you're currently in the 'zookeeper' directory

cd .. 
helm push zookeeper\ chartmuseum 
```

