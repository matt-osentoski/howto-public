# Install Kafka (Confluent stack) using dynamic volume provisioning
This document describes the steps to install the confluent stack using Helm.

Author: Matt Osentoski  
E-mail: matt.osentoski@gmail.com

## Dependencies
### Storage Class
It's assumed you already have a Kubernetes Storage class setup.  This example will use the
storage class created using Rook.io / Ceph as the storage system.  It is also assumed that
the storage class name that was created is called `rook-ceph-block`.

See the following README file for details around installing Rook.io and Ceph:
https://bitbucket.org/matt-osentoski/howto-public/src/master/kubernetes/intel-nuc-cluster/dynamic-volume-provisioning/storage-classes/rook.io/README.md

### Helm
Helm should already be setup on your cluster.  If not, take a look at the following README:
https://bitbucket.org/matt-osentoski/howto-public/src/master/kubernetes/intel-nuc-cluster/helm/README.md


## Install the confluent stack using Helm.
>(NOTE: These instructions are derived from: https://github.com/confluentinc/cp-helm-charts/tree/master/charts/cp-kafka)

### (Optional) Label specific nodes for use with Kafka servers
This step allows you to specify which nodes will be used for the kafka servers.  You will have to 
define the 'nodeSelector' in your Helm values file to correspond to these values.

>(NOTE: This only has to be performed one time against the cluster.)

```
kubectl label nodes worker1 apptype=kafka
kubectl label nodes worker2 apptype=kafka
kubectl label nodes worker3 apptype=kafka
```

### Add the Confluent Repository to Helm
Run the following command to pull down the chart repo:
```
helm repo add confluentinc https://confluentinc.github.io/cp-helm-charts/
helm repo update
```

### Update the `cp-helm-values.yaml` file
Update the values file in this directory to match your environment.  Each service (besides zookeeper and kafka) can
be enabled or disabled.  They are all enabled by default and assume you have added the Node labels in the step above.

The following entries may need to be changed in your configuration:
```yaml
cp-zookeeper:
  persistence:
    dataDirStorageClass: "rook-ceph-block"
    dataLogDirStorageClass: "rook-ceph-block"
  nodeSelector:
    apptype: kafka
    
cp-kafka:   
  persistence:
    storageClass: "rook-ceph-block"
  configurationOverrides:
    "default.replication.factor": 3
    "advertised.listeners": |-
      EXTERNAL://192.168.6.$((6 + ${KAFKA_BROKER_ID})):$((31090 + ${KAFKA_BROKER_ID}))  
  nodeSelector:
    apptype: kafka  
    
cp-schema-registry:
  kafka:
    bootstrapServers: "PLAINTEXT://192.168.6.6:31090,PLAINTEXT://192.168.6.7:31091,PLAINTEXT://192.168.6.8:31092"
  nodeSelector:
    apptype: kafka  
    
cp-kafka-rest:
  nodeSelector:
    apptype: kafka
    
cp-kafka-connect: 
  kafka:
    bootstrapServers: "PLAINTEXT://192.168.6.6:31090,PLAINTEXT://192.168.6.7:31091,PLAINTEXT://192.168.6.8:31092"
  nodeSelector:
    apptype: kafka 

cp-ksql-server:    
  kafka:
    bootstrapServers: "PLAINTEXT://192.168.6.6:31090,PLAINTEXT://192.168.6.7:31091,PLAINTEXT://192.168.6.8:31092"
  nodeSelector:
    apptype: kafka      
```

### Install the helm chart
```
kubectl create namespace kafka
helm upgrade --install --values cp-helm-values.yaml --namespace kafka cp-kafka confluentinc/cp-helm-charts
```

>(NOTE: The `cp-helm-values.yaml` is located in the same directory as this README file)

### Exposing Zookeeper externally is no longer required
In previous versions of Kafka, certain operations like creating custom topics required direct access to Zookeeper.
With the latest versions you can use the Kafka brokers to delegate these tasks. For this reason, there's no longer a
need to have external access to Zookeeper.  If external access is desired, create a node port to the Zookeeper servers.

### Exposing Kafka Connect's REST service
Currently, the helm charts don't have an option to externally expose the REST service from Kafka connect.  To
add a load balancer service, run the following command:

```
kubectl expose deployment cp-kafka-cp-kafka-connect --type=LoadBalancer --namespace=kafka --name=cp-kafka-cp-kafka-connect-srv-lb
```

### Default Topics for KSQL
KSQL Server requires that a few topics be setup. Run the following commands from a Kafka client:

>(NOTE: If these topics are not setup, the KSQL service will continuously restart the pod)

```
kafka-topics --bootstrap-server 192.168.6.6:31090 --create --replication-factor 3 --partitions 1 --topic users --config retention.ms=-1
kafka-topics --bootstrap-server 192.168.6.6:31090 --create --replication-factor 3 --partitions 1 --topic pageviews --config retention.ms=-1
```




## Kafka Troubleshooting
### Pods keep restarting
When in doubt, try increasing the memory for a service in the cp-helm-values.yaml file.  For example:
```yaml
cp-ksql-server:
  # Try increasing the 'limits' values
  resources:
    limits:
     cpu: 1
     memory: 2048Mi
    requests:
     cpu: 100m
     memory: 128Mi
```

### Troubleshooting Kafka Access
If you have issues accessing Kafka external from kubernetes, try downloading the kafkacat tool:

https://github.com/edenhill/kafkacat

Here's an example command, based on the sample IP/Port we discovered in the previous steps:
```
kafkacat -L -b 192.168.6.2:31090
```

The output is:
```
Metadata for all topics (from broker 0: 192.168.6.2:31090/0):
 3 brokers:
  broker 2 at 192.168.6.2:31092
  broker 1 at 192.168.6.3:31091
  broker 0 at 192.168.6.2:31090
 1 topics:
  topic "__confluent.support.metrics" with 1 partitions:
    partition 0, leader 1, replicas: 1,0, isrs: 1,0
```

Here we can see that the brokers that are advertised.  These brokers are using the Kubernets Node IP address, so we should
have external access assuming our computer has a route to this IP address range.  If there is a problem, typically the broker IP
address will show a `10.x.x.x` address that is internal to the kubernetes cluster and unreachable from an external machine.  In
this case, verify that you have the correct NodePort settings enabled:

https://github.com/confluentinc/cp-helm-charts/tree/master/charts/cp-kafka#external-access

### Troubleshooting Zookeeper problems
If you encounter an error similar to the following you may need to increase the `tickTime` setting in the zookeeper chart values.yaml file:
```
[2019-03-14 14:35:55,327] WARN Failed to resolve address: confluent-cp-zookeeper-1.confluent-cp-zookeeper-headless.confluent (org.apache.zookeeper.server.quorum.QuorumPeer)
java.net.UnknownHostException: confluent-cp-zookeeper-1.confluent-cp-zookeeper-headless.confluent Name or service not known
        at java.net.Inet4AddressImpl.lookupAllHostAddr(Native Method)
    
```

Add a setting like this in your `values.yaml` file:
```yaml
cp-zookeeper:
  # Fixes a bug where the zookeeper servers can't find each other and require more time for discovery
  tickTime: 10000
```

## Remove Kafka (Cleanup)
Run the following commands to remove all traces of Kafka from the Kubernetes cluster
```
helm delete --purge cp-kafka
helm delete --purge cp-schema-registry
helm delete --purge cp-kafka-connect
helm delete --purge cp-ksql-server
helm delete --purge cp-kafka-rest
kubectl delete namespace kafka
```