# Install Kafka (Confluent stack) using dynamic volume provisioning
## WARNING: This document is deprecated and for historical purposes.  Use the README.md in the parent directory instead.

This document describes the steps to install the confluent stack using Helm.  This assumes that
you will be installing all the Kafka components separately as individual helm projects instead of the umbrella chart.

Author: Matt Osentoski  
E-mail: matt.osentoski@gmail.com

## Dependencies
### Storage Class
It's assumed you already have a Kubernetes Storage class setup.  This example will use the
storage class created using Rook.io / Ceph as the storage system.  It is also assumed that
the storage class name that was created is called `rook-ceph-block`.

See the following README file for details around installing Rook.io and Ceph:
https://bitbucket.org/matt-osentoski/howto-public/src/master/kubernetes/intel-nuc-cluster/dynamic-volume-provisioning/storage-classes/rook.io/README.md

### Helm
Helm should already be setup on your cluster.  If not, take a look at the following README:
https://bitbucket.org/matt-osentoski/howto-public/src/master/kubernetes/intel-nuc-cluster/helm/README.md


## Install the confluent stack using Helm.
>(NOTE: These instructions are derived from: https://github.com/confluentinc/cp-helm-charts/tree/master/charts/cp-kafka)

### (Optional) Label specific nodes for use with Kafka servers
This step allows you to specify which nodes will be used for the kafka servers.  You will have to 
define the 'nodeSelector' in your Helm values file to correspond to these values.

>(NOTE: This only has to be performed one time against the cluster.)

```
kubectl label nodes nuc-node3 apptype=kafka
kubectl label nodes nuc-node4 apptype=kafka
kubectl label nodes nuc-node5 apptype=kafka
```

### Clone the Confluent helm repository
Run the following command to pull down the chart repo:
```
git clone https://github.com/confluentinc/cp-helm-charts.git
```

Install the helm chart, pointing to the Storage class:
```
helm upgrade --install --values cp-helm-values.yaml --namespace kafka cp-kafka confluent/cp-helm-charts
```

>(NOTE: The `cp-helm-values.yaml` is located in the same directory as this README file)

>(NOTE2: If you get an error that looks like this: **Error: incompatible versions client[v2.8.2] server[v2.6.2]**  You will have to
upgrade helm/tiller using the following command: `helm init --upgrade`)


## External access to Kafka
Accessing Kafka outside the cluster is a little bit tricky.  You have to setup the `cp-helm-values.yaml` file to enable NodePorts.
This is already setup in the file.  Once this is done, you will have one node port server per Kafka server.

To determine the port that the NodePort services are using, run the following command:
```
kubectl get service --namespace kafka
```

Most of the ports should be in the 30000 range. For example, and output might look like this:
```
hduser@nuc-name1:~$ kubectl get service --namespace kafka
NAME                             TYPE        CLUSTER-IP       EXTERNAL-IP   PORT(S)             AGE
cp-kafka                         ClusterIP   10.105.120.125   <none>        9092/TCP            37m
cp-kafka-0-nodeport              NodePort    10.98.243.248    <none>        19092:31090/TCP     37m
cp-kafka-1-nodeport              NodePort    10.101.120.34    <none>        19092:31091/TCP     37m
cp-kafka-2-nodeport              NodePort    10.104.63.123    <none>        19092:31092/TCP     37m
cp-kafka-cp-zookeeper            ClusterIP   10.99.33.64      <none>        2181/TCP            37m
cp-kafka-cp-zookeeper-headless   ClusterIP   None             <none>        2888/TCP,3888/TCP   37m
cp-kafka-headless                ClusterIP   None             <none>        9092/TCP            37m
```

From this point, you will need to determine the IP address of the node.  If we use `cp-kafka-0-nodeport` as an example,
the port that will be exposed is `31090`.  Next, let's lookup the IP address:
>(NOTE: `cp-kafka-0` is the name of the pod and derived from the NodePort name `cp-kafka-0-nodeport`

```
kubectl describe pods --namespace kafka cp-kafka-0 | grep Node
```
with an output of:
```
Node:               nuc-data1/192.168.6.2
Node-Selectors:  <none>

```

We now know that the external IP/port for the broker is `192.168.6.2:31090`.  Next we can run Kafka tests externally using this
IP/Port combo to verify connectivity.


## External Access to Zookeeper
To setup external access to Zookeeper, you will have to setup one or more `NodePort` services to the Zookeeper `Stateful Set`.
In this directory is an example service file. (`zk-nodeport-service.yaml`)

To create the service, run the following command:
```
kubectl --namespace kafka create -f zk-nodeport-service.yaml
```

## Install Registry, KSQL, Connect, and REST services
There seems to be an issue installing these additional services using the cp-helm Helm chart when using NodePorts.  I have disabled these
additional services in the cp-helm-values.yaml
file listed in the instructions above. I installed the additional kafka services one-by-one after the core kafka cluster is online and working.
This may change at a later date.  In which case, you will be able to ignore the steps below and just run a single helm install.

To setup the additional kafka service, you'll first have to obtain the broker IP address and ports from the NodePort services
that were created.  Once you have these addresses, run the following commands:
```
## Install the Kafka Registry service
# (NOTE: Use the Kafka nodePort address/ports below)
helm install  --name=cp-schema-registry --namespace kafka \
 --set kafka.bootstrapServers="PLAINTEXT://192.168.6.3:31092\,PLAINTEXT://192.168.6.3:31091\,PLAINTEXT://192.168.6.3:31090" \
 cp-helm-charts/charts/cp-schema-registry

## Install the Kafka connect server
# (NOTE: Use the Kafka nodePort address/ports for the bootstrap servers and the registry address from the service
# above for the cp-schema-registry.url)
helm install --name=cp-kafka-connect --namespace kafka \
 --set kafka.bootstrapServers="PLAINTEXT://192.168.6.2:31092\,PLAINTEXT://192.168.6.3:31091\,PLAINTEXT://192.168.6.2:31090",cp-schema-registry.url="cp-schema-registry.kafka:8081" \
 cp-helm-charts/charts/cp-kafka-connect

## Install the KSQL server
# (NOTE: You should use the internal Zookeeper and Registry address below)
#
# (IMPORTANT!!!!! For some reason, this helm chart doesn't create two topics that are required for ksql to run.
# before running this chart, create the following two topics in Kafka:
# 'users' and 'pageviews'
#
# (IMPORTANT #2 !!!! To allow external access make sure 'ksql.headless' is set to false.  This allows the listener to
# listen on all IP addresses (0.0.0.0) using the 'external.enabled=true' setting to build a Load balancer service.
# Otherwise, you will get 'connection refused' errors from the load balancer IP
#
helm install --name=cp-ksql-server --namespace kafka \
 --set ksql.headless="false",external.enabled="true",kafka.bootstrapServers="PLAINTEXT://192.168.6.2:31092\,PLAINTEXT://192.168.6.3:31091\,PLAINTEXT://192.168.6.2:31090",cp-schema-registry.url="cp-schema-registry.kafka:8081" \
 cp-helm-charts/charts/cp-ksql-server

## Install the Kafka REST service
# (NOTE: You should use the internal Zookeeper and Registry address below)
#
# (IMPORTANT!!!! Get the zookeeper IP/Port by running the following command:
# kubectl --namespace kafka describe  services cp-kafka-cp-zookeeper | grep -A1 IP
#
helm install  --name=cp-kafka-rest --namespace kafka \
 --set external.enabled="true",cp-zookeeper.url="10.111.229.82:2181",cp-schema-registry.url="cp-schema-registry.kafka:8081" \
 cp-helm-charts/charts/cp-kafka-rest
```



## Kafka Troubleshooting
If you have issues accessing Kafka external from kubernetes, try downloading the kafkacat tool:

https://github.com/edenhill/kafkacat

Here's an example command, based on the sample IP/Port we discovered in the previous steps:
```
kafkacat -L -b 192.168.6.2:31090
```

The output is:
```
Metadata for all topics (from broker 0: 192.168.6.2:31090/0):
 3 brokers:
  broker 2 at 192.168.6.2:31092
  broker 1 at 192.168.6.3:31091
  broker 0 at 192.168.6.2:31090
 1 topics:
  topic "__confluent.support.metrics" with 1 partitions:
    partition 0, leader 1, replicas: 1,0, isrs: 1,0
```

Here we can see that the brokers that are advertised.  These brokers are using the Kubernets Node IP address, so we should
have external access assuming our computer has a route to this IP address range.  If there is a problem, typically the broker IP
address will show a `10.x.x.x` address that is internal to the kubernetes cluster and unreachable from an external machine.  In
this case, verify that you have the correct NodePort settings enabled:

https://github.com/confluentinc/cp-helm-charts/tree/master/charts/cp-kafka#external-access

## Remove Kafka (Cleanup)
Run the following commands to remove all traces of Kafka from the Kubernetes cluster
```
helm delete --purge cp-kafka
helm delete --purge cp-schema-registry
helm delete --purge cp-kafka-connect
helm delete --purge cp-ksql-server
helm delete --purge cp-kafka-rest
kubectl delete namespace kafka
```