# Install Fluentd for ElasticSearch using Helm

## Modfiy the values.yaml file
For my install, I changed the following `value-overrides.yaml` settings:
```
elasticsearch:
  host: "elasticsearch-master.elastic.svc.cluster.local"
```

## Install the Fluentd Chart
```
helm repo add kiwigrid https://kiwigrid.github.io/
helm upgrade --install --namespace elastic --values values.yaml,value-overrides.yaml fluentd kiwigrid/fluentd-elasticsearch
```

## Remove the chart
```
helm del --purge fluentd
kubectl delete namespace fluentd
```