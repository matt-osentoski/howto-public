# Install Dask using Helm
Dask is a scalable analytics platform, similar to Spark, but with less complexity and
moving parts 

Reference URLs:

[https://docs.dask.org/en/latest/](https://docs.dask.org/en/latest/)

[https://github.com/dask/helm-chart](https://github.com/dask/helm-chart)

## Modfiy the values.yaml file
Customize chart settings by updating the   `value-overrides.yaml` file.

## Install the Chart
```
helm repo add dask https://helm.dask.org/
helm repo update
helm upgrade --install --namespace dask --values value-overrides.yaml dask dask/dask
```

## Remove the chart
```
helm uninstall dask --namespace dask
```