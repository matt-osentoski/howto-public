# Install Postgres with Debezium plugins already installed
Debezium with Postgres requires plugins for Protocol Buffers and JSON to function correctly.
This chart uses the Debezium docker image and wraps it in helm for easy install.

>(NOTE: This chart assumes you will be using dynamic volume provisioning and already have a storage class setup
ready to use.)

## Configure the chart
In the `postgresql-debezium-helm` folder, modify the `values.yaml` file to suit your needs

## Install / Upgrade the chart
To install or upgrade the helm chart, run the following command from this directory:

```
helm upgrade --install --namespace postgres-deb  postgresql-debezium postgresql-debezium-helm
```

## Remove Postgres (Cleanup)
To remove Postgres and all its data, run the following commands:

```
helm del --purge postgresql-debezium
kubectl delete namespace postgres-deb
```