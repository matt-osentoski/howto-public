# HAPI FHIR Starter helm chart
This Helm chart will install HAPI FHIR with PostgreSQL as a sub-chart.  

(Source: [https://github.com/hapifhir/hapi-fhir-jpaserver-starter/tree/master/charts/hapi-fhir-jpaserver](https://github.com/hapifhir/hapi-fhir-jpaserver-starter/tree/master/charts/hapi-fhir-jpaserver))

>(NOTE: You can externalize the PostgreSQL database using the values file)

## Add the Helm repo

```shell 
helm repo add hapifhir https://hapifhir.github.io/hapi-fhir-jpaserver-starter/
```

## Install the Chart using custom values

>(NOTE: Update the custom-values.yaml to make changes [https://github.com/hapifhir/hapi-fhir-jpaserver-starter/blob/master/charts/hapi-fhir-jpaserver/values.yaml](https://github.com/hapifhir/hapi-fhir-jpaserver-starter/blob/master/charts/hapi-fhir-jpaserver/values.yaml))

>(IMPORTANT!!! The chart, by default, will randomly generate the password for the DB. This could cause
> issues when upgrading the chart.  Use the `postgresqlPassword` and `replication.password` settings to avoid this
> issue.)

```shell 
kubectl create namespace fhir
```

### (Optional: Create a secret with TLS cert/private key.)
>(NOTE: If you use this, set the ingress 'enabled' flag to 'true')

>(NOTE 2: View the README file under /kubernetes/intel-nuc-cluster/certificate/authority/README.md for more
> details in the setup of a self signed cert with included (bundled) CA cert.)

```shell 
kubectl create secret tls jaroof-cert-secret --cert=bundle-jaroof.local.crt --key=jaroof.local.key --namespace fhir
```

>(IMPORTANT!!!! If you're using Windows, you might get this error 
> `Error: parse error at (hapi-fhir-jpaserver/charts/postgresql/charts/common/templates/_resources.tpl:15): unclosed action`
> If this happens, you might have to run helm from a Unix shell.)

```shell
helm upgrade --install --values custom-value-overrides.yaml --namespace fhir \
    --set postgresql.global.postgresql.auth.password=password \
    --set postgresql.auth.replicationPassword=password \
    --render-subchart-notes hapi-fhir-jpaserver hapifhir/hapi-fhir-jpaserver
```