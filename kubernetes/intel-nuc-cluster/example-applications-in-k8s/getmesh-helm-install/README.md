# Install GetMesh Content Management System (CMS)
GetMesh is a lightweight headless CMS.

The project can be found here: [https://github.com/cschockaert/getmesh-chart]()

Author: Matt Osentoski  
E-mail: matt.osentoski@gmail.com


## Dependencies
### Helm
Helm should already be setup on your cluster.  If not, take a look at the following README:
https://bitbucket.org/matt-osentoski/howto-public/src/master/kubernetes/intel-nuc-cluster/helm/README.md

### Elastic search charts
Update the helm charts, so that Elastic search is available.
```
helm repo add incubator http://storage.googleapis.com/kubernetes-charts-incubator
```

## Install GetMesh using Helm
### Clone the GetMesh Helm chart
```
git clone https://github.com/cschockaert/getmesh-chart.git
```

### Create a Java keystore file
GetMesh requires a Keystore file for the server to start.  Run the following commands to install one in the `config` directory
of the Git Repo:
```
cd getmesh-chart/config
keytool -genseckey -keystore keystore.jceks -storetype jceks -storepass secret -keyalg HMacSHA256 -keysize 2048 -alias HS256 -keypass secret
```
>(NOTE: The keystore command was referenced from: https://vertx.io/docs/vertx-auth-jwt/java/)

### Change the keystore password in the mesh config file
Run the following commands to set the keystore password
```
vi getmesh-chart/config/mesh.yml
```
Set `keystorePassword` to 'secret'

### Update Helm dependencies
Run the following commands to update any dependencies that GetMesh has.  Helm will inspect the `requirements.yaml` file in
the repo and pull down any required dependencies.
```
cd getmesh-chart
helm dependency update
```

### Install the Helm chart
>(NOTE: `custom-values.yaml` is incluced in the same folder as this README file.  Update as needed)

```
helm install --name=getmesh --namespace cms -f custom-values.yaml getmesh-chart/
```

## Create a LoadBalancer service for inbound access to the CMS
>(NOTE: `getmesh-service.yaml` is located in the same folder as this README file.)

```
kubectl --namespace cms create -f getmesh-service.yaml
```

## Teardown
To teardown and remove all traces of GetMesh, run the following commands:
```
helm del --purge getmesh
kubectl delete namespace cms
```
