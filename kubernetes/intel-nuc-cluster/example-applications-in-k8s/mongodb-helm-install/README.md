# DEPRECATED!! - Install MongoDB using Helm
>(DEPRECATED: Use the Bitnami chart or the operator to install MongoDB. Also note, at
> this time the Operator doesn't seem as stable as the Bitnami charts)

This document describes how to install MongoDB using Helm with a StorageClass as
the backing volume.

Author: Matt Osentoski  
E-mail: matt.osentoski@gmail.com

>(NOTE: This document is derived from: [https://github.com/helm/charts/tree/master/stable/mongodb](https://github.com/helm/charts/tree/master/stable/mongodb) )

## Configuration and Installation
### Make changes to the values.yaml file
The values.yaml file in this directory allow for custom settings to the MongoDB database.  Note that the following
changes have been made from the default file located here:

[https://github.com/helm/charts/blob/master/stable/mongodb/values.yaml](https://github.com/helm/charts/blob/master/stable/mongodb/values.yaml)

```yaml
service:
  type: LoadBalancer

resources: 
  limits:
    cpu: 500m
    memory: 512Mi
  requests:
    cpu: 100m
    memory: 256Mi

persistence:
  storageClass: "rook-ceph-block"
  size: 8Gi
```

### Install MongoDB using Helm
Install the helm chart, pointing to the Storage class:
```
helm upgrade --install --namespace mongodb --values values.yaml mongodb stable/mongodb
```

>(NOTE: The `values.yaml` is located in the same directory as this README file)

## Teardown
To remove all traces of MongoDB in Kubernetes, run the following commands:

```
helm del --purge mongodb
kubectl delete namespace mongodb
```