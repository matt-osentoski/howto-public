# Install KeyCloak into Kubernetes

Author: Matt Osentoski  
E-mail: matt.osentoski@gmail.com

## Update the configuration file
The Helm chart project for KeyCloak can be viewed here:
https://github.com/codecentric/helm-charts

## Install the Helm chart
Run the following commands to install Keycloak using Helm. I'm using the `value-overrides.yaml` settings
to customize the installation.
```
kubectl create namespace keycloak
helm repo add codecentric https://codecentric.github.io/helm-charts
helm upgrade --install -f value-overrides.yaml --namespace keycloak keycloak codecentric/keycloak

```

## Port forward to enable Admin access
To setup Admin access you have to access the server locally. To do this using Kubernets you can 
port-forward into the service then access the URL: [http://localhost/auth/admin](http://localhost/auth/admin)

```shell
kubectl port-forward services/keycloak-http 80:80 8443:8443 9990:9990 -n keycloak
```

Access the Keycloak pod using the port-forward URL to setup Admin access: [http://localhost/auth/admin](http://localhost/auth/admin)

## Verify the server is running
>(NOTE: This assumes the LoadBalancer IP address is 192.168.6.154 run
> `kubectl get service keycloak-http -o wide --namespace keycloak` to get this value)

To access the admin console, go to the following URL:
https://keycloak.jaroof.local/auth/admin
username: keycloak
password: keycloak

Go to the `keycloak-configuration.md` file to learn how to setup the Identity server.

## Realm Login screen
Once a realm has been setup using the `keycloak-configuration.md` instructions, mentioned above, you can access the
realm's login page using the following URL

`https://keycloak.jaroof.local:8080/auth/realms/myRealmName/account`

Where 'myRealmName' is the name of the realm you created.


