>(IMPORTANT!!!!: The instructions below don't seem to work.  The Helm chart doesn't want to bring in the init container, for some reason.
A better approach might be to create an extended docker image for Keycloak, then change the 'keycloak.image.repository'
and 'keycloak.image.tag' values in the helm chart to point to your custom Docker image.  An example, where a docker image
is extended for keycloak can be found here: `https://github.com/thedigitalgarage/keycloak-style`)


```
docker build -t 192.168.6.2:5000/mytheme:1.0.0 .
docker push 192.168.6.2:5000/mytheme:1.0.0
```

Verify the docker image was pushed to the remote repository
```
curl -X GET 192.168.6.2:5000/v2/_catalog
```

Change the `values.yaml` to use the theme container's files
```yaml
keycloak:
  extraInitContainers: |
    - name: theme-provider
      image: 192.168.6.2:5000/mytheme:1.0.0
      imagePullPolicy: IfNotPresent
      command:
        - sh
      args:
        - -c
        - |
          echo "Copying theme..."
          cp -R /mytheme/* /theme
      volumeMounts:
        - name: theme
          mountPath: /theme

  extraVolumeMounts: |
    - name: theme
      mountPath: /opt/jboss/keycloak/themes/mytheme

  extraVolumes: |
    - name: theme
      emptyDir: {}
```