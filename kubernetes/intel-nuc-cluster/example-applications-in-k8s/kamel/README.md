# Camel-K - A native k8s Camel Implementation
Camel is an Enterprise Integration Pattern framework.  Camel-K takes this a
step further and makes the framework cloud native with simple deployments and 
development.

## Install the `kamel` CLI
Follow the instructions here to install the `kamel` CLI tool for your operating
system:

[https://camel.apache.org/camel-k/latest/installation/installation.html](https://camel.apache.org/camel-k/latest/installation/installation.html)

## Install the Kamel k8s operator using the CLI
Run the following command to install kamel. 

>(NOTE: In my case, I had to point kamel to my locally hosted Docker registry server.  Your configurations
> may vary.  See this link for more details about registry configurations:
> [https://camel.apache.org/camel-k/latest/installation/registry/registry.html](https://camel.apache.org/camel-k/latest/installation/registry/registry.html))

```
kamel install --namespace kamel --registry-insecure true --registry docker.jaroof.local \
    --organization hduser --registry-secret regcred --force 
```

This assumes you have a secret created for your local Docker registry using a command that looks like this:
```
kubectl create secret docker-registry regcred \
    --namespace=kamel \
    --docker-server=docker.jaroof.local \
    --docker-username=hduser --docker-password=p455word
```

## Create a Hello World example
Using the `Basic.java` file in this directory, run the following command:

``` 

```