# Deploying applications to a Kubernetes cluster
ref: https://docs.docker.com/registry/deploying/

Author: Matt Osentoski  
E-mail: matt.osentoski@gmail.com

## Local Docker Registry server
First, you should setup a local docker registry.  Kubernetes will use this registry to pull down docker images with your application.
>(NOTE: You can skip this step if you plan on pushing to a public registry)

### Install the Docker Registry Server
On one of your Nuc servers, run the following command to setup the registry.
(In my case, I'm installing the docker registry on the master k8s server)
```
docker run -d -p 5000:5000 --restart=always --name registry registry:2
```

### Setup the Registry Server to run without SSL/TSL
Ref: https://docs.docker.com/registry/insecure/
From the machine that will be pushing the image to your local docker registry server, add the following entry into
you daemon.json file.  This file is located at:

`C:\ProgramData\docker\config\daemon.json  # Windows`
`/etc/docker/daemon.json  # Unix`

### Push images to your local Docker Registry Server
To push an image to your local Docker Registry Server, you'll first have to 'tag' the image using the URL of the
registry server, then perform the 'push' command.

`docker tag <IMAGE_NAME>:<VERSION> <REGISTRY_SERVER_URL:PORT/IMAGENAME>`
Here's an example:
```
docker tag exampleImage:1.0.0 localhost:5000/exampleImage
```

### Verify images were pushed to the local Docker Registry
Run the following comnand to view all images on your local docker registry:
```
curl -X GET http://192.168.6.2:5000/v2/_catalog
```

### View tags for an image in the registry
```
curl -X GET http://192.168.6.2:5000/v2/YOUR_IMAGE_NAME_AND_PREFIX_GO_HERE/tags/list

# For example, an image named: gbhf/k8s-example-ws
curl -X GET http://192.168.6.2:5000/v2/gbhf/k8s-example-ws/tags/list
```


