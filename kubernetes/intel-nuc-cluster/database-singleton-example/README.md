# DEPRECATED!!  Database Singleton Example
>(NOTE: This document should be considered deprecated.  If you want to run a stateful workload, like
>a database, you should use an off-the-shelf helm chart and dynamic storage, via a storageClass)

This directory has samples for creating a database using NFS as the backing file store via persistent volumes (PV) and
persistent volume claims (PVC).  MySQL will be used in this example.
>(NOTE: The PV/PVC instructions are reference in the 'volumes' folder, which is at the same level as this one.)

Author: Matt Osentoski  
E-mail: matt.osentoski@gmail.com

## Create the PV/PVC
Follow the instructions in the 'volumes' folder to create a PV/PVC from an NFS shared volume.

## Create the MySQL deployment/service
Using the `mysql-deployment.yaml` file, create the deployment/service using the following command:
```
kubectl create -f mysql-deployment.yaml
```

## Mysql configuration
Assuming you're using a Spring boot application, you can configure your Mysql URL to use 'mysql' as the domain name
```java
spring.datasource.url=jdbc:mysql://mysql:3306/k8s_example_stocks
spring.jpa.hibernate.ddl-auto=update
```

## Login to your Mysql server and create the database
```
kubectl run -it --rm --image=mysql:5.6 --restart=Never mysql-client -- mysql -h mysql -ppassword
```
Once you see the mysql> prompt type:
```mysql
CREATE DATABASE some_database_name
```