#!/bin/sh
# This script remotely shuts down all servers in the cluster
# The appropriate SSH keys are required on the remote machines for this to work smoothly

ssh -t hduser@nuc-worker1 "sudo shutdown -h now"
ssh -t hduser@nuc-worker2 "sudo shutdown -h now"
ssh -t hduser@nuc-worker3 "sudo shutdown -h now"
ssh -t hduser@nuc-worker4 "sudo shutdown -h now"
ssh -t hduser@nuc-worker5 "sudo shutdown -h now"
ssh -t hduser@pi-router "sudo shutdown -h now"
sudo shutdown -h now

