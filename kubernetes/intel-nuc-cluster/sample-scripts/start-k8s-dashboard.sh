#!/bin/sh
# This script starts up a proxy connection that allows the k8s dashboard to be accessed remotely.

kubectl proxy --address='192.168.6.2' --accept-hosts '.*' &
echo "Access the dashboard using the following URL:"
echo "http://192.168.6.2:8001/api/v1/namespaces/kubernetes-dashboard/services/http:kubernetes-dashboard:/proxy"
