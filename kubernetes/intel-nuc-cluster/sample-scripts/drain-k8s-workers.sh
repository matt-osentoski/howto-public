#!/bin/sh
# This script removes (evicts) all pods from a node. This will allow the server to safely be shut down

kubectl drain nuc-worker1 --ignore-daemonsets --force --delete-local-data &
kubectl drain nuc-worker2 --ignore-daemonsets --force --delete-local-data &
kubectl drain nuc-worker3 --ignore-daemonsets --force --delete-local-data &
kubectl drain nuc-worker4 --ignore-daemonsets --force --delete-local-data &
kubectl drain nuc-worker5 --ignore-daemonsets --force --delete-local-data &

