#!/bin/sh
# This script uncordons, or reschedules pods into the k8s cluster.  This operation should be performed
# when the cluster servers are brought back online after shutdown / maintenance.

kubectl uncordon nuc-worker1
kubectl uncordon nuc-worker2
kubectl uncordon nuc-worker3
kubectl uncordon nuc-worker4
kubectl uncordon nuc-worker5

