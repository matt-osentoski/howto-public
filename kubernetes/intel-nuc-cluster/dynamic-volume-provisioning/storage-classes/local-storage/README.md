# Local Storage class
Newer versions of Kubernetes allow storage classes using the local node storage drives instead
of cloud storage or something like Ceph.  

This is useful for applications that won't work well using storage like Ceph where the data is 
replicated 3x.  An example is ElasticSearch that already replicates its data.  Using something 
like Ceph would cause replication amplification which would slow down writes.

>(NOTE: Although this all works. It's a bit of a hack.  The PV's
must be manually created and this can be very tedious.  These instructions should be treated as
a short-term solution until Kubernetes has native dynamic volume provisioning for local storage.)

## Install using Helm
The instructions below are for creating local-storage manually. An easier approach
is to use a Helm chart to create the Persistent Volumes (PV)'s. Use the README.md file below
to created the local-storage using Helm instead:

[kubernetes/intel-nuc-cluster/example-applications-in-k8s/local-storage-helm](kubernetes/intel-nuc-cluster/example-applications-in-k8s/local-storage-helm)

## Install local-storage manifest manually
To install the local-storage storageClass, run the following command from this directory.

```
kubectl apply -f local-storage.yaml
```

## Create a local storage folder per node
On each worker node, create the following folder:

```
sudo mkdir /var/lib/local-storage
sudo chmod -R 777 /var/lib/local-storage

## For each sub PV, you will have to create a subdirectory
sudo mkdir /var/lib/local-storage/es1
sudo mkdir /var/lib/local-storage/es2
sudo mkdir /var/lib/local-storage/es3
sudo chmod -R 777 /var/lib/local-storage/es1
sudo chmod -R 777 /var/lib/local-storage/es2
sudo chmod -R 777 /var/lib/local-storage/es3
```

## Create the local-storage PV per node
Each node will require a PV to be manually created and associated with the `StorageClass` created
above.  Run the following commands from the 'pv-files' directory from this location.  Adjust the files to match your cluster
and the number of nodes.

>(NOTE: You may have to create a LOT of these PV files depending on the number of volumes your
application requires.  Note the naming convention I'm using.  You should create a similar convention. 
Hopefully, Kubernetes will have a native dynamic volume provisioning solution for local storage in the future
and this will no longer be required.)
```
cd pv-files
kubectl apply -f local-storage-pv-node1-es1.yaml
kubectl apply -f local-storage-pv-node1-es2.yaml
kubectl apply -f local-storage-pv-node1-es3.yaml
kubectl apply -f local-storage-pv-node2-es1.yaml
kubectl apply -f local-storage-pv-node2-es2.yaml
kubectl apply -f local-storage-pv-node2-es3.yaml
kubectl apply -f local-storage-pv-node3-es1.yaml
kubectl apply -f local-storage-pv-node3-es2.yaml
kubectl apply -f local-storage-pv-node3-es3.yaml
kubectl apply -f local-storage-pv-node4-es1.yaml
kubectl apply -f local-storage-pv-node4-es2.yaml
kubectl apply -f local-storage-pv-node4-es3.yaml
kubectl apply -f local-storage-pv-node5-es1.yaml
kubectl apply -f local-storage-pv-node5-es2.yaml
kubectl apply -f local-storage-pv-node5-es3.yaml
```

## Remove the PV's
Sometimes the PV's will be stuck in a `terminating` status.  To fix this issue, run the following
command, then delete the PV again:

```
kubectl patch pv local-storage-pv-node1-es1 -p '{"metadata":{"finalizers":null}}'
```

## Cleanup
First, delete the PV's from kubernetes:

```
cd pv-files
kubectl delete -f local-storage-pv-node1-es1.yaml
kubectl delete -f local-storage-pv-node1-es2.yaml
kubectl delete -f local-storage-pv-node1-es3.yaml
kubectl delete -f local-storage-pv-node2-es1.yaml
kubectl delete -f local-storage-pv-node2-es2.yaml
kubectl delete -f local-storage-pv-node2-es3.yaml
kubectl delete -f local-storage-pv-node3-es1.yaml
kubectl delete -f local-storage-pv-node3-es2.yaml
kubectl delete -f local-storage-pv-node3-es3.yaml
kubectl delete -f local-storage-pv-node4-es1.yaml
kubectl delete -f local-storage-pv-node4-es2.yaml
kubectl delete -f local-storage-pv-node4-es3.yaml
kubectl delete -f local-storage-pv-node5-es1.yaml
kubectl delete -f local-storage-pv-node5-es2.yaml
kubectl delete -f local-storage-pv-node5-es3.yaml
```

Next, you will have to manually delete the files from all nodes where the local storage
was mapped:

```
sudo rm -Rf /var/lib/local-storage/es1
sudo rm -Rf /var/lib/local-storage/es2
sudo rm -Rf /var/lib/local-storage/es3
```

## TroubleShooting
### A pod/PVC are in a pending state.
If a Pod or PVC are in a pending state waiting to start up, make sure the space allocated
to the PVC is LESS than the amount that each PV has available. 

For example, if you created the PV's manually with 5GB worth of space, but the PVC allocation is
8GB, then the PVC will not bind to the PV, leaving the pod in a pending state. The solution here
is to reduce the allocation so that it fits into the size allocated by the PV.