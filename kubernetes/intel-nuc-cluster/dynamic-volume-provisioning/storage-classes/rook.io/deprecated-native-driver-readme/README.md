# DEPRECATED!!!!!! Rook.io using Ceph for Dynamic Volume Provisioning

> (NOTE: This README file and included k8s manifest were for the original k8s native storage class drivers.
The new system uses an abstraction layer called CSI.  These documents are archived for historical reasons)

Rook is a storage orchistrator for the cloud.  In our example, we will be using Ceph as the underlying storage service.
This will allow Kubernetes to dynamically spin up volumes for applications.

This differs from using NFS via PV/PVC's.
NFS is already created and Kubernetes is just tapping into it.  With dynamic volume provisioning, we will be creating
volumes as needed dynamically using Kubernetes.

Author: Matt Osentoski  
E-mail: matt.osentoski@gmail.com

## Installation
>(NOTE: These instructions are derived from: https://github.com/rook/rook/blob/master/Documentation/ceph-quickstart.md#deploy-the-rook-operator)

## Setup the dataDirHostPath on each node
Each Kubernetes node will require a file location where the storage will be persisted. The default
location is `/var/lib/rook` which is what we will use.
Run the commands below on each node:
```
sudo mkdir /var/lib/rook
sudo chmod -R 777 /var/lib/rook
```
>(NOTE: Change the `chmod` settings above to suit your security needs)

## Download the Rook code
Pull down the Rook manifest files from GIT:
```
git clone https://github.com/rook/rook.git
```

## Install the Operator and Cluster manifest files
Next, we want to install the Rook operator and cluster into Kubernetes using the `kubectl` command.
>(NOTE: Ideally, we would be using Helm to install Rook, but at this time, I thought the scripts weren't quite ready, so
these instructions are for deploying manually)

Run the following command from the `rook` folder that was created after cloning from Git:
```
cd cluster/examples/kubernetes/ceph
kubectl create -f common.yaml
kubectl create -f operator.yaml
kubectl create -f cluster-test.yaml
```

Verify that the pods (rook-ceph-agent, rook-discovery, rook-ceph-operator, etc.) are running:
```
kubectl -n rook-ceph get pod
```

Install the cluster:
>(IMPORTANT!!!! Use the `my-cluster.yaml` in the same directory as this README file)
```
kubectl create -f my-cluster.yaml
```

Verify that the cluster pods are running:
```
kubectl -n rook-ceph get pod
```

Verify persistent storage.  Rook should have created directories under /var/lib/rook on the
data nodes.  You can verify this by running the following commands on each data node where ceph is
running:
```
cd /var/lib/rook
ls
```

## Create a Kubernetes Storage Class
Assuming you're still in the `cluster/examples/kubernetes/ceph` directory from the last step, run
the following command to setup a Strorage class.  This will allow pods to create dynamic volumes:
```
kubectl create -f storageclass.yaml
```

Verify the storage class was created:
```
kubectl -n rook-ceph get storageclass
```

Note that the storage class name is `rook-ceph-block` this will be used when referencing the storage
class in other manifest files, or helm configurations.


## Expose the Ceph dashboard
To run the Ceph dashboard, you will have to expose the service using the Metal LB load balancer.  Run the following
command:
```
kubectl -n rook-ceph expose service rook-ceph-mgr-dashboard --type=LoadBalancer --name=ceph-dashboard-service
```
View the service information using:
```
kubectl -n rook-ceph get service
```
You should see that the service you just created is running at an external IP/Port allocated by your Load balancer.  Assuming this
is `192.168.6.153:8443`, you should be able to open the dashboard from a web browser using that address.
>(IMPORTANT! Use https and not http for the url)
```html
https://192.168.6.153:8443
```

To login, you'll have to lookup the password using the following command:
```
kubectl -n rook-ceph get secret rook-ceph-dashboard-password -o yaml | grep "password:" | awk '{print $2}' | base64 --decode
```
`Admin` is the username along with the password from the command above.

## Setup Ceph Object store (to mimick S3)
You can setup Ceph to look like an S3 storage system.
>(NOTE: This is based off the following page: https://github.com/rook/rook/blob/master/Documentation/ceph-object.md)

### Create the object store for Ceph
Run the following command using the `object-store.yaml` file in this directory.  Make changes as needed.
```
kubectl create -f object-store.yaml
```

### Create the Object Store User
Run the following command using the `object-user.yaml` file in this directory.  Make changes as needed.
```
kubectl create -f object-user.yaml
```

### Retrieve the S3 Access key and secret
Run the following commands to retrieve the Access key and secret you will need to access the object store:
>(NOTE: The value `rook-ceph-object-user-my-store-my-user` might change, based on the settings you entered in the 'object-user.yaml' file)

```
kubectl -n rook-ceph get secret rook-ceph-object-user-s3-store-my-user -o yaml | grep AccessKey | awk '{print $2}' | base64 --decode
kubectl -n rook-ceph get secret rook-ceph-object-user-s3-store-my-user -o yaml | grep SecretKey | awk '{print $2}' | base64 --decode
```

### Setup external access using a load balancer
Run the following command using the `object-store-loadbalancer.yaml` file in this directory.  Make changes as needed.
```
kubectl create -f object-store-loadbalancer.yaml --namespace=rook-ceph
```

### GUI tools to access Ceph Object store
Download Cloudberry Explorer and select S3 compatible, when adding a new provider:
https://www.cloudberrylab.com/explorer/amazon-s3.aspx

Use the `AccessKey` and `SecretKey`, along with the Loadbalancer IP address/port to connect as if this were an S3 client.


## Troubleshooting CEPH
### Install the toolbox
From `/rook/cluster/examples/kubernetes/ceph` of the repo run the following command:
```
kubectl create -f toolbox.yaml
```

### CEPH Troubleshooting commands
To troubleshoot CEPH, terminal into one of the ceph pods using the following command:
```
kubectl -n rook-ceph get pods
kubectl -n rook-ceph exec -it rook-ceph-tools-b5759df6b-q6mfn bash
```
>(NOTE: rook-ceph-tools-b5759df6b-q6mfn in this case was retrieved from the previous `get pods` command)

Once terminaled inside the pod, run the following commands:
```
# Cluster status
ceph status

# Rados commands with access to the underlying file store objects

rados lspools
rados -p replicapool ls -
```




