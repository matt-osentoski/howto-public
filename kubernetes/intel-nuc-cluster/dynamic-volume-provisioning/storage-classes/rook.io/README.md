# Rook.io using Ceph for Dynamic Volume Provisioning
Rook is a storage orchestrator for the cloud.  In our example, we will be using Ceph as the underlying storage service.
This will allow Kubernetes to dynamically spin up volumes for applications.

This differs from using NFS via PV/PVC's.
NFS is already created and Kubernetes is just tapping into it.  With dynamic volume provisioning, we will be creating
volumes as needed dynamically using Kubernetes.

> (NOTE: Kubernetes is now using an abstraction layer for StorageClass drivers.  These instructions have changed
to reflect this)

>(IMPORTANT: Rook/ceph now require a separate hard drive without a file system to work correctly. If your
>Nuc nodes only have one hard drive, look at the following instructions instead: `/kubernetes/intel-nuc-cluster/dynamic-volume-provisioning/storage-classes/rook.io/deprecated-directory-storage/README.md`
>another option is to use local storage which is more low level and not true dynamic storage. However, it will still appear as a
>storage class from a kubernetes perspective. Instructions for Local storage are located here: `/kubernetes/intel-nuc-cluster/dynamic-volume-provisioning/storage-classes/local-storage/README.md`)

Author: Matt Osentoski  
E-mail: matt.osentoski@gmail.com

## Installation
>(NOTE: These instructions are derived from: https://github.com/rook/rook/blob/master/Documentation/ceph-quickstart.md#deploy-the-rook-operator)

## Verify your hard drives are setup for Rook / Ceph
Rook/Ceph requires a free drive per node that does not contain a file system. To verify your node is setup
correctly, run the command below on each node:
```
lsblk -f

### Output below
NAME                  FSTYPE      LABEL UUID                                   MOUNTPOINT
vda
└─vda1                LVM2_member       eSO50t-GkUV-YKTH-WsGq-hNJY-eKNf-3i07IB
  ├─ubuntu--vg-root   ext4              c2366f76-6e21-4f10-a8f3-6776212e2fe4   /
  └─ubuntu--vg-swap_1 swap              9492a3dc-ad75-47cd-9596-678e8cf17ff9   [SWAP]
vdb
```

In the example above, the `vda` drive can NOT be used with Ceph. The `vdb` drive can, since it has no 'FSTYPE'.

### (OPTIONAL) Cleaning out an older Ceph partition
If you have an older ceph install or just want to clean out a hard drive,
run the following commands:

First, let's zap the disk (NOTE: This might not work)
```shell 
sudo gdisk /dev/sda

Click 'x' for extra functionality
Then click 'z' to Zap the disk

Click 'Y' twice to wipe the GPT and to clear out the MBR
```

```shell 
### Next, check to see if the Ceph Filesystem is still mapped:
sudo dmsetup ls --tree

#### If you see something like this, it's still mapped:
ceph--a00eb6ee--28d7--4251--b206--6825c5ef8628-osd--data--021841c9--ca80--4ff5--ae24--65b6c50b11ee (253:0)
 └─ (8:0)
 
 
#### Remove the mapping:  (using the example above)
sudo dmsetup remove ceph--a00eb6ee--28d7--4251--b206--6825c5ef8628-osd--data--021841c9--ca80--4ff5--ae24--65b6c50b11ee

#### Now check the mapping again:
sudo dmsetup ls --tree

### it should look like this:
No devices found

#### Running this command should show an empty 'sda' drive now (NOTE: Your drive may be different
#### than 'sda'
lsblk -f
```

Finally, make sure the cleanup this directory in all nodes using Ceph:

```shell 
sudo rm -Rf /var/lib/rook/
```

## Download the Rook code
Pull down the Rook manifest files from GIT:
```
git clone https://github.com/rook/rook.git

## NOTE: Directory storage has been removed.  To use it again, use the release-1.2 branch
git checkout release-1.2
```

## Install the Operator using Helm
```shell
helm repo add rook-release https://charts.rook.io/release
helm install --create-namespace --namespace rook-ceph rook-ceph rook-release/rook-ceph 

```

Verify that the pods (rook-ceph-agent, rook-discovery, rook-ceph-operator, etc.) are running:
```
kubectl -n rook-ceph get pod
```

Install the cluster:
>(IMPORTANT!!!! Use the `my-cluster.yaml` in the same directory as this README file)

>(IMPORTANT #2 !!!!  Inside the `my-cluster.yaml` file, update the 'image' to the most recent version)

>(IMPORANT #3 !!!! Be sure to set the nodes to be used and drive names at the bottom of the `my-cluster.yaml` file)
```
kubectl create -f my-cluster.yaml
```

Verify that the cluster pods are running:
```
kubectl -n rook-ceph get pod
```

Verify persistent storage.  Rook should have created directories under /var/lib/rook on the
data nodes.  You can verify this by running the following commands on each data node where ceph is
running:
```
cd /var/lib/rook
ls
```

## Create a Kubernetes Storage Class
Assuming you're still in the `cluster/examples/kubernetes/ceph` directory from the last step, run
the following command to setup a Strorage class.  This will allow pods to create dynamic volumes:
```
cd csi/rbd
kubectl create -f storageclass.yaml
```

Verify the storage class was created:
```
kubectl -n rook-ceph get storageclass
```

Note that the storage class name is `rook-ceph-block` this will be used when referencing the storage
class in other manifest files, or helm configurations.

## OPTIONAL: Set the StorageClass you created as 'default'
You can set a storage class as default, to do so, run the following commands:
### First, Unset the existing default storageclass, if there is one
```shell 
kubectl patch storageclass MY_STORAGE_CLASS_NAME -p '{"metadata": {"annotations":{"storageclass.kubernetes.io/is-default-class":"false"}}}'
```

### Next, Set a different StorageClass as default
```shell 
kubectl patch storageclass MY_STORAGE_CLASS_NAME -p '{"metadata": {"annotations":{"storageclass.kubernetes.io/is-default-class":"true"}}}'

## For example:
kubectl patch storageclass rook-ceph-block -p '{"metadata": {"annotations":{"storageclass.kubernetes.io/is-default-class":"true"}}}'
```

## OPTIONAL: StorageClasses and CephBlock Pools to control Replication
StorageClasses have a number of parameters, one is `pool`. This points to 
a CephBlockPool which has a value spec->replicated->size.  This controls
the replicas for each block.  Create a CephBlockPool and separate storageClass
for non-replicated data.  

This is important for applications that already control replication at the 
application level. For instance, ElasticSearch or Kafka. Otherwise, the
storage will expand exponentially on your cluster.

To setup a storageclass with no replication, run the following command, using
the `storageclass-noreplication.yaml` file in the same directory as this README file:

```
kubectl create -f storageclass-noreplication.yaml
```


## Expose the Ceph dashboard
To run the Ceph dashboard, you will have to expose the service using the Metal LB load balancer.  Run the following
command:
```
kubectl -n rook-ceph expose service rook-ceph-mgr-dashboard --type=LoadBalancer --name=ceph-dashboard-service
```
View the service information using:
```
kubectl -n rook-ceph get service
```
You should see that the service you just created is running at an external IP/Port allocated by your Load balancer.  Assuming this
is `192.168.6.153:7000`, you should be able to open the dashboard from a web browser using that address.
```html
http://192.168.6.153:7000
```

To login, you'll have to lookup the password using the following command:
```
kubectl -n rook-ceph get secret rook-ceph-dashboard-password -o yaml | grep "password:" | awk '{print $2}' | base64 --decode
```
`admin` is the username along with the password from the command above.

## Setup Ceph Object store (to mimick S3)
You can setup Ceph to look like an S3 storage system.
>(NOTE: This is based off the following page: https://github.com/rook/rook/blob/master/Documentation/ceph-object.md)

### Create the object store for Ceph
Run the following command using the `object-store.yaml` file in this directory.  Make changes as needed.
```
kubectl create -f object-store-manifests/object-store.yaml
```

### Create the Object Store User
Run the following command using the `object-user.yaml` file in this directory.  Make changes as needed.
```
kubectl create -f object-store-manifests/object-user.yaml
```

### Retrieve the S3 Access key and secret
Run the following commands to retrieve the Access key and secret you will need to access the object store:
>(NOTE: The value `rook-ceph-object-user-my-store-my-user` might change, based on the settings you entered in the 'object-user.yaml' file)

```
kubectl -n rook-ceph get secret rook-ceph-object-user-s3-store-my-user -o yaml | grep AccessKey | awk '{print $2}' | base64 --decode
kubectl -n rook-ceph get secret rook-ceph-object-user-s3-store-my-user -o yaml | grep SecretKey | awk '{print $2}' | base64 --decode
```

### Setup external access using a load balancer
Run the following command using the `object-store-loadbalancer.yaml` file in this directory.  Make changes as needed.
```
kubectl create -f object-store-manifests/object-store-loadbalancer.yaml --namespace=rook-ceph
```

### GUI tools to access Ceph Object store
Download Cloudberry Explorer and select S3 compatible, when adding a new provider:
https://www.cloudberrylab.com/explorer/amazon-s3.aspx

Use the `AccessKey` and `SecretKey`, along with the Loadbalancer IP address/port to connect as if this were an S3 client.


## Tear down 
To tear down all of ceph, run the following commands:
```shell
### Change to the directory with the 'my-cluster.yaml' file before running this command.
kubectl delete -f my-cluster.yaml

helm uninstall rook-ceph --namespace rook-ceph
```

Next, let's clean up the Ceph underlying directories for metadata and OSD data.
>(IMPORTANT!!! Run this from each node where ceph data is stored)
```shell

sudo gdisk /dev/sda

Click 'x' for extra functionality
Then click 'z' to Zap the disk

Click 'Y' twice to wipe the GPT and to clear out the MBR
````

```shell
sudo dmsetup ls --tree

#### If you see something like this, it's still mapped:
ceph--a00eb6ee--28d7--4251--b206--6825c5ef8628-osd--data--021841c9--ca80--4ff5--ae24--65b6c50b11ee (253:0)
 └─ (8:0)
 
#### Remove the mapping:  (using the example above)
sudo dmsetup remove ceph--a00eb6ee--28d7--4251--b206--6825c5ef8628-osd--data--021841c9--ca80--4ff5--ae24--65b6c50b11ee
```

```shell
sudo rm -Rf /var/lib/rook/
```

## Troubleshooting CEPH
### Install the toolbox
From `/rook/cluster/examples/kubernetes/ceph` of the repo run the following command:
```
kubectl create -f toolbox.yaml
```

### CEPH Troubleshooting commands
To troubleshoot CEPH, terminal into one of the ceph pods using the following command:
```
kubectl -n rook-ceph get pods
kubectl -n rook-ceph exec -it rook-ceph-tools-b5759df6b-q6mfn bash
```
>(NOTE: rook-ceph-tools-b5759df6b-q6mfn in this case was retrieved from the previous `get pods` command)

Once terminaled inside the pod, run the following commands:
```
# Cluster status
ceph status

# Rados commands with access to the underlying file store objects

rados lspools
rados -p replicapool ls -
```




