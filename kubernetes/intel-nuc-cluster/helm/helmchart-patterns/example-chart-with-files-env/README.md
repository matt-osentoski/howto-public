# Example Helm chart with Files and Environment Vars
The `example-app` folder contains a good example of a base Helm chart that injects environment variables
and an array of files into a running container.

## Update the values and templates

### Add an Environment, Environment Secrets, and Files sections into your Values.yaml
First, let's create an area for adding environment variables and Files into the values.yaml

>(NOTE: This will be the `example-app/values.yaml`)

```yaml
### This section is for defining Environment Variables
### Take a look at the deployment.yaml and configmap.yaml template files to 
### see how these variables are injected into the pod container
env: 
  FIRST_VAR: "first environment variable"
  SECOND_VAR: "second environment variable"

### This section is for defining Secrets as Environment Variables
### NOTE: k8s secrets should first be defined.
### The array of 'name' properties is the k8s Secret name
### Take a look at the deployment.yaml template file to see how these secrets are injected into the pod container
envSecrets:
  - name: first-secret
  - name: second-secret

### This section is for defining files that will be injected into the container
### Take a look at the deployment.yaml and configmap-files.yaml template files to 
### see how these files are injected into the pod container
files:
  - name: somefile.txt
    path: /tmp
    contents: |-
      This is a test
      This file has two lines in it.
  - name: anotherfile.txt
    path: /tmp
    contents: |-
      This is another test
      This file has three lines in it.    
      Last line.
```

### Create a configmap for environment variables

>(NOTE: This will be the `example-app/templates/configmap.yaml`)

```yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: {{ include "example-app.fullname" . }}-configmap
data:
### This is used to specify custom environment variables from the values file
{{- with .Values.env }}
{{ toYaml $.Values.env | indent 2 }}
{{- end }}
```

### Create and Mount Secrets as environment variables
To utilize secrets in your containers as environment variables, first create the secret using
`kubectl`. Then make sure the `values.yaml` file has the `envSecrets` section populated with an array of 
secret names as shown below:

>(NOTE: You can reuse secrets across Pods within the same namespace.)

```shell
kubectl create secret generic first-secret --namespace my-namespace --from-literal=DB_USER=dbuser --from-literal=DB_PASSWORD=sup3rs3cr3t
kubectl create secret generic second-secret --namespace my-namespace --from-literal=MAP_API_KEY=abcdef123456
```

### Create a configmap for files

>(NOTE: This will be the `example-app/templates/configmap-files.yaml`)

```yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: {{ include "example-app.fullname" . }}-files-configmap
data:
### This section is used to include files from the values file
{{- with .Values.files }}
{{- range $.Values.files }}
  {{ .name }}: |
{{ .contents | indent 4 }}
{{- end }}
{{- end }}
```

### Update the deployment.yaml to include the environment variables, environment secrets, and files in the container

>(NOTE: This will be the `example-app/templates/deployment.yaml`)

```yaml
spec:
  template:
    spec:
      containers:
        - name: {{ .Chart.Name }}
          envFrom:
            ### This is where the environment variables from your ConfigMap will be applied to the container.
            - configMapRef:
                name: {{ template "example-app.fullname" . }}-configmap
            ### This is where the environment variables from secrets are applied to your container
            {{- with .Values.envSecrets }}
            {{- range $.Values.envSecrets }}
            - secretRef:
                name: {{ .name }}
            {{- end }}
            {{- end }}
          ### This section loops through all the files in the values.yaml file and mounts the files in the configmap-files.yaml
          ### file. The files are first mounted as a volume further below.
          {{- with .Values.files }}
          volumeMounts:
            {{- range $.Values.files }}
            - name: volume-files
              mountPath: {{ .path }}/{{ .name }}
              subPath: {{ .name }}
            {{- end }}
          {{- end }}
      ### This section mounts files from the configmap-files.yaml file. The mount name is then referenced above in the
      ### volumeMounts section of the container, to mount the file into the container.
      {{- with .Values.files }}
      volumes:
        - configMap:
            #### Note the dollar sign ($) below instead of a dot (.) since we're in a 'with' block the scope
            #### is limited to this block.  Using the dollar sign pushes the template call to global scope
            name: {{ template "example-app.fullname" $ }}-files-configmap
          name: volume-files
      {{- end }}
```

## Manually add TLS certs to the Ingress controller:
To manually add your own TLS certs to the Ingress, first create a secret from your private key and certificate:

>(NOTE: This command should be executed in a directory that contains both the tls.key and tls.crt files)

```shell
kubectl create secret tls my-app-tls --namespace my-namespace \
    --key tls.key \
    --cert tls.crt
```

Next, in your `values.yaml` file, update the Ingress section to include this secret:

```yaml
...
ingress:
  enabled: true
  annotations: 
    kubernetes.io/ingress.class: nginx
  hosts:
    - host: my.domain.local
      paths:
        - path: /
          pathType: ImplementationSpecific
  tls:
    ###### The Secret you created above goes here
    - secretName: my-app-tls  
      hosts:
        - my.domain.local
...
```


