# How to mount configuration files into a container and custom environment variables.
Docker containers typically will require Environment Variables that need to
be set on a per environment basis.  Also, there are times
you will need to mount a configuration file, but make it configurable,
based on the environment.  

This directory has an example showing how to approach these problems.

>(NOTE: I'm excluding most of the standard chart files. This is to focus on the
parts relevant to the environment variables and configuration file mounting)

## Important Parts
To wire everything up the following pieces are required:

### configmap.yaml
Create a configmap.yaml with a `data` section at the bottom with
'tpl' function applied to files and a section for custom Environment variables.

>(NOTE: Sometimes the files with custom variables will also be GoLang templates.  The way to use these templates as well
is to escape the existing `{{}}` sequences with a pattern like this `{{\`{{ var.stuff }}\`}}`  Note that this uses single backticks ) 

### Add your file that will be mounted into the container.
In this example.  I'm using the file `./files/myconfig.json`

>(NOTE: Looking in the file you will see mustache braces that allow for
golang variables which are referenced in the `values.yaml` file.)

### Update your `values.yaml` file
There should be values for all the environment variables you want to define in the container
using `env`

In the `configMapFiles` section, create sub-sections for each file and
the variables you want to override in the config file.  For example,
in `./files/myconfig.json` there is a variable for the 'amount' property.  This
maps to the `configMapFiles.myconfigJson.amount` value in `values.yaml`

>(NOTE: The naming conventions I used in the `values.yaml` file are completely
arbitrary.  You can come up with your own naming convention, if you would like) 


### Update the `deployment.yaml` file
1. Create a `envFrom` section at: `spec.template.spec.containers.envFrom`
    - This section is for all the custom Environment variables that will be added to the container.
2. Create a `volumeMounts` section at: `spec.template.spec.containers.volumeMounts`
    - This section will define the volume location inside the container and point to a `volume` section defined next.
3. Create a `volume` section at the same level as 'nodeSelector' or 'affinity`
    - This section will map the volume to the configmap created above. 
