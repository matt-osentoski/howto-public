# Helm installation
Helm is a package manager for Kubernetes.

>(NOTE: In my opinion you should not start using Helm until you understand the fundamentals of installing applications
and other Kubernetes artifacts manually using Kubectl and manifest files.  Once you understand the built-in Kubernetes
tools and processes, you can start using Helm as a convenience to deploy your applications.)

Author: Matt Osentoski  
E-mail: matt.osentoski@gmail.com

## Install the Helm client (Ubuntu)
To install Helm, run the following command
```
sudo snap install helm --classic
```

## Setup RBAC Roles for Tiller
First, setup the RBAC Roles before installing Tiller.

This step came from an answer on StackOverflow:
https://stackoverflow.com/questions/48556971/unable-to-install-kubernetes-charts-on-specified-namespace

Run the following commands on a computer with kubectl access to the cluster. The YAML files are located in the same
directory as this README file:
```
kubectl apply -f tiller-cr.yaml
kubectl create sa tiller -n kube-system
kubectl apply -f tiller-crb.yaml
helm init --service-account tiller
```

## Install Tiller
Tiller runs inside the Kubernetes cluster.  To install, run the following command
```
helm init
```

# Running Helm
## Create a new helm chart
Run the following command to setup a scaffold Helm chart
```
helm create <PROJECT_NAME>
```

## Lint the Chart to check for errors
Run the following command from the directory with the Chart.yaml file
```
helm lint .
```

## Install a release
This command will install a new Helm release onto your kubernetes cluster
Run the following command from a computer with helm access to the kubernetes cluster
```
helm install . --name <RELEASE_NAME>
```

## Install a release into a specific namespace
```
helm install . --name <RELEASE_NAME> --namespace <NAMESPACE>
```

## Install/Upgrade a release (Preferred approach)
>(NOTE: I found this to be the preferred approach to install and upgrade charts
```
helm upgrade --install --namespace <NAMESPACE> <RELEASE_NAME> <CHART_NAME_OR_LOCATION>

# Example where 'postgressql-debezium' is the release name and 'postgresql-debezium-helm' is the chart folder
helm upgrade --install --namespace postgres-namespace postgresql-debezium postgresql-debezium-helm
```

## Check the status of an install
```
helm ls --all <RELEASE_NAME>
```

## Purge a helm deployment
```
helm del --purge <RELEASE_NAME>
```

## Upgrade a Release
```
helm upgrade . --name <RELEASE_NAME> --namespace <NAMESPACE>
```
