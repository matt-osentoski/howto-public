# Backend services example
This example spins up a stack of backend services for your applicatiosn. Including:
- The Confluent stack (Kafka, etc)
- Redis for caching
- ELK stack for logging, etc.

# Run the Helmfile

>(NOTE: The `--environment` option must be before `apply`)
```
helmfile --environment dev apply
```

# Cleanup the entire stack

```
helmfile destroy
```