# Custom Charts
This folder is for apps that don't already have a public chart available.  

## Push to the Chart Museum
You will have to push these charts to your local Helm chart museum using the following command:

>(NOTE: It's assumed that 'chartmuseum' is a Helm repo that has already been setup.
```
helm push my-chart chartmuseum
```