# Backend services example 2
This example spins up a stack of backend services for your applicatiosn. Including:
- The Confluent stack (Kafka, etc)
- Redis for caching
- Elastic Search
- Kibana
- Logstash
- APM Server
- Elastic App Search

# Run the Helmfile

>(NOTE: The `--environment` option must be before `apply`)
```
helmfile --environment dev apply
```

# Cleanup the entire stack

```
helmfile destroy
```