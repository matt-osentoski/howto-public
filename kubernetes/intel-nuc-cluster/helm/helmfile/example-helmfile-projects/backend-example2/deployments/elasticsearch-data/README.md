# Data Node Configurations
The following values make this a data node:

```yaml
nodeGroup: "data"
roles:
  master: "false"
  ingest: "true"
  data: "true"
```