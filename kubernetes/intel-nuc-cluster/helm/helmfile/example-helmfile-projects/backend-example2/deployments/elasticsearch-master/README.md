# Master Node Configurations
The following values make this a master node:

```yaml
nodeGroup: "master"
roles:
  master: "true"
  ingest: "false"
  data: "false"
```