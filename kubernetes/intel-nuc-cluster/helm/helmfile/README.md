# Helmfile 
Helmfile is a utility that makes it easier to deploy multiple helm charts as a software stack to an
environment.

[https://github.com/roboll/helmfile](https://github.com/roboll/helmfile)

## Windows Installation
### Install Scoop
[https://scoop.sh/](https://scoop.sh/)

From PowerShell, run the following commands to install `scoop` if it isn't already installed:

```
Set-ExecutionPolicy RemoteSigned -scope CurrentUser

## Enter 'Y' after this command

iex (new-object net.webclient).downloadstring('https://get.scoop.sh')
```

### Install Helmfile
Run the following command from a Powershell or Command prompt

```
scoop install helmfile
```

### Install the Helm Diff plug-in
On each machine where helm will run, add the following plugin:
helm plugin install https://github.com/databus23/helm-diff

>(IMPORTANT!!:  On windows machines, you might have to run the installation using
a 'GIT BASH' shell as Administrator.  The Plugin uses linux scripts that don't run on windows correctly)

>(IMPORTANT #2!!!!!: On windows 10 machines Symlinks won't be created unless
you're running in 'developer mode'  To enable developer mode, read the folloowing link:
[https://www.howtogeek.com/292914/what-is-developer-mode-in-windows-10/](https://www.howtogeek.com/292914/what-is-developer-mode-in-windows-10/))

>(TROUBLESHOOTING: If you're still having trouble adding Helm plugins. Try
deleting the '~/.helm' directory and rerun `helm init` on Windows machines, the helm configurations
>might be located at: `C:\Users\<YOUR_USERNAME>\AppData\Local\Temp\helm`)

```
helm plugin install https://github.com/databus23/helm-diff
```

## Running Helmfile
Take a look in the `example-helmfile-projects` folder in this directory.  There are examples with a `helmfile.yaml`
at the root of each example directory.  To build a software stack using helmfile, run the following command:

```
## Change to the folder containing the helmfile

helmfile --environment dev apply
```

## Cleanup
To delete and purge all kubernetes resources associated with the helmfile, run the following command from the
directory with the `helmfile.yaml` file:

```
helmfile destroy
```