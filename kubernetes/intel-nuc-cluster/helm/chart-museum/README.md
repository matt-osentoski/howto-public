# Chart Museum
A chart museum is your own Helm chart repository.  

Author: Matt Osentoski  
E-mail: matt.osentoski@gmail.com

>(NOTE: This document is derived from the instructions at the following URL:
https://github.com/helm/charts/tree/master/stable/chartmuseum)

## Install the Chart Museum server
The Chart Museum requires a server component to act as a Helm Repository.
To install the chart museum server (An HTTP server), I will use a Helm chart and a 
StorageClass to store the charts.  You could optionally use something like S3 as the storage.

### Update the values.yaml file
Update the `values.yaml` file in this directory to reflect your deployment environment.  In
my case, I'll be using a storageClass for the backing storage.  You could use S3 or another cloud 
store.  In my case, the following lines were changed in the `values.yaml` file:

```yaml
  # NOTE: this is very important, otherwise 'helm push' operations won't work.
  DISABLE_API: false
  
  persistence:
    enabled: true
    accessMode: ReadWriteOnce
    size: 2Gi
    storageClass: "rook-ceph-block"
```

### Install the chart
Run the following commands, pointing to the `values.yaml` file you updated in the last step
```
helm repo add stable https://kubernetes-charts.storage.googleapis.com
helm upgrade --install --namespace chartmuseum --values values.yaml chartmuseum stable/chartmuseum
```

### Add LoadBalancing
To expose the chart, add a loadbalancer to the deployment

```
kubectl expose deployment chartmuseum-chartmuseum --namespace=chartmuseum --type=LoadBalancer --name=chartmuseum-lb
```

## Verify the Chart Museum is running
Assuming your load balancer setup the following IP/port `192.168.6.154:8080`, you
should be able to verify the Chart museum is running by access the following URL:

[http://192.168.6.154:8080/](http://192.168.6.154:8080/)

## Add the Chart Museum to the Helm repos
From the machine(s) where you run helm, execute the following command:
>(NOTE: Again, this assumes your load balancer is exposing the museum at 192.168.6.154:8080)

```
helm repo add chartmuseum http://192.168.6.154:8080

## You verify the repo was added by typing:
helm repo list
```

## Add the 'Helm Push' plug-in
On each machine where helm will run, add the following plugin:
https://github.com/chartmuseum/helm-push

>(IMPORTANT!!:  On windows machines, you might have to run the installation using
a 'GIT BASH' shell as Administrator.  The Plugin uses linux scripts that don't run on windows correctly)

```
helm plugin install https://github.com/chartmuseum/helm-push
```

## Test that the Museum is working
From a directory wih a helm chart, run the following command to push the chart into the Chart Museum:

```
helm push my-chart/ chartmuseum
```

### Verify the Chart was Pushed
Using the Museum URL from the previous steps, use the following URL to view all pushed Charts:

[http://192.168.6.154:8080/api/charts](http://192.168.6.154:8080/api/charts)




