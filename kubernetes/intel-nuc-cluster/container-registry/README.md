# Private Container Registry Installation
This directory contains different ways to create a private container registry for your Kubernetes cluster.

## Install Twuni
At this time, I would recommend installing via the `twuni/` folder instructions.  Twuni's container registry will run
within k8s and not require us to install Docker alongside ContainerD on the cluster. (which could potentially cause some issues.)