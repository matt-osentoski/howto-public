# Install and setup a local docker registry server
This document explains how to setup a docker registry server for local development and as an image source for Kubernetes pods

>(NOTE: This document is derived from: [https://docs.docker.com/registry/deploying/]())

Author: Matt Osentoski  
E-mail: matt.osentoski@gmail.com

## Private Docker Registry server
 Kubernetes will use this private registry to pull down docker images with your application.
>(NOTE: You can skip this step if you plan on pushing to a public registry)

### Create/Modify the Registry configuration file
Use the `config.yml` file in this directory as a starting point, then place it on the server where the registry will run.

### Install the Docker Registry Server
On one of your Nuc servers, run the following command to setup the registry.
(In my case, I'm installing the docker registry on the master k8s server)
```
sudo docker run -d -p 5000:5000 --restart=always --name registry \
             -v `pwd`/config.yml:/etc/docker/registry/config.yml \
             registry:2
```

### Setup the Registry Server to run without SSL/TSL
Ref: https://docs.docker.com/registry/insecure/
From the machine that will be pushing the image to your local docker registry server, add the following entry into
you daemon.json file.  This file is located at:

>(NOTE: You should run this command on every Kubernetes node in your cluster. Make sure to restart docker,
>or the cluster nodes after making this change.)

`C:\ProgramData\docker\config\daemon.json  # Windows`

`/etc/docker/daemon.json  # Unix`
```json
{
    "insecure-registries" : [ "192.168.6.2:5000" ]
}
```

### Push images to your local Docker Registry Server
To push an image to your local Docker Registry Server, you'll first have to 'tag' the image using the URL of the
registry server, then perform the 'push' command.

`docker tag <IMAGE_NAME>:<VERSION> <REGISTRY_SERVER_URL:PORT/IMAGENAME>`
Here's an example:
```
docker tag exampleImage:1.0.0 localhost:5000/exampleImage
```

### Verify images were pushed to the local Docker Registry
Run the following comnand to view all images on your local docker registry:
```
curl -X GET http://192.168.6.2:5000/v2/_catalog
```

### Remove an image from the local docker registry
Run the following set of commands to remove an image from the docker registry:
>(NOTE: For this to work you should have set 'storage/enabled/delete=true' in your config.yml file in the create server step.)

```
# First, determine the image name:
curl -X GET http://localhost:5000/v2/_catalog

# Next, Find the 'Docker-Content-Digest' header value:
curl -v -H "Accept: application/vnd.docker.distribution.manifest.v2+json" -X HEAD http://localhost:5000/v2/<IMAGE_NAME_FROM_LAST_STEP>/manifests/<VERSION_GOES_HERE>

# Finally, using the 'Docker-Content-Digest' header, delete the image entry
curl -X DELETE http://localhost:5000/v2/<IMAGE_NAME_FROM_LAST_STEP>/manifests/<DIGEST_GOES_HERE>
```

### Stop and remove the registry from Docker
```
sudo docker container stop registry && sudo docker container rm -v registry
```