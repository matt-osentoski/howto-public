# Install and setup a local docker registry server with encryption and security
This document explains how to setup a docker registry server for local development and as an image source for Kubernetes pods.
As time goes on, more and more applications are requiring TLS and authentication to use a docker
registry.  This document walks through this setup using a self-signed TLS certificate and
basic authentication.

>(NOTE: This document is derived from: [https://docs.docker.com/registry/deploying/]())

Author: Matt Osentoski  
E-mail: matt.osentoski@gmail.com

## (Deprecated, create a wildcard cert /CA and skip this section) Create a self-signed certificate
>(IMPORTANT!!!!!!!!!  These certificate instructions are now deprecated, you should create a wildcard TLS certificate and CA Cert
> using the instructions listed in this Git repo at: `/kubernetes/intel-nuc-cluster/certificate-authority/README.md`)

>(NOTE: These steps are derived from the following URL: [https://docs.docker.com/registry/insecure/](https://docs.docker.com/registry/insecure/) )

```
mkdir -p certs

openssl req -new -subj "/C=US/ST=MI/O=Jaroof LLC/OU=Org/CN=docker.jaroof.local" \
                  -addext "subjectAltName = DNS:docker.jaroof.local" \
                  -addext "certificatePolicies = 1.2.3.4" \
                  -newkey rsa:4096 -nodes -sha256 -keyout certs/domain.key \
                  -x509 -days 3650  -out certs/domain.crt 
  
```

The certificate and private key should now be in a directory called `certs`

>(CERT_INFORMATION: The certificate contains the public key definition. The
> public key is shared with the client and is used to 'encrypt' the data. The
> private key is NOT shared with the client. The private key 'decrypts' the data
> coming back from the client. The Certificate also includes a signature for verification.
> To crack open the cert and take a look at the contents, run the following command:
> `openssl x509 -in domain.crt  -text -noout`)

## Allow ContainerD (on Linux) to trust the certificate
If your registry is secured using TLS (it should be) then you will have to let each k8s node know about the TLS cert.
>(NOTE: domain.crt should be a bundled cert that contains both the domain TLS cert and CA cert together)

SCP the certificate to all servers in the k8s cluster.  Change the
host names where appropriate.
```shell
scp /home/hduser/domain.crt hduser@nuc-worker1:/home/hduser/domain.crt
scp /home/hduser/domain.crt hduser@nuc-worker2:/home/hduser/domain.crt
scp /home/hduser/domain.crt hduser@nuc-worker3:/home/hduser/domain.crt
scp /home/hduser/domain.crt hduser@nuc-worker4:/home/hduser/domain.crt
```

### Copy the Certificate to the correct directory
>(IMPORTANT!!! This step has to be performed on every server in the k8s cluster.)
Also, the directory that you are copying to below is Ubuntu specific>
```shell
sudo cp /home/hduser/domain.crt /usr/local/share/ca-certificates
```

### Update the CA certs and restart the ContainerD Daemon on all nodes
Run the following command on all nodes once the certificate is copied to the
appropriate directory.

```shell
sudo update-ca-certificates

sudo systemctl restart containerd
```

### Setup DNS for the certificate on all k8s nodes
On each server update the `/etc/hosts` file with the following values:

>(NOTE: If you will be using this Docker registry on any development workstations,
> setup a host entry on those machines, as well)

```
192.168.6.2     docker.jaroof.local
```

### (OPTIONAL) Allow the certificate on Windows workstations
If you use Windows (or a Mac) for development you will have to add this
self-signed cert to your certificate chain. 

For windows, copy the domain.crt file over using WinSCP, then:
`Right click the file` -> `Install certificate` 

In my case, I installed it to the `local machine`

To view the cert in windows, run this command from the command line: `certmgr. msc`
Navigate to `Intermediate Certification Authorities` -> `Certificates`

For Macs, follow these instructions: [https://docs.docker.com/docker-for-mac/#add-tls-certificates](https://docs.docker.com/docker-for-mac/#add-tls-certificates)

## Setup Authentication
### Create a Docker password file
Run the following commands to create a password file in your home directory.
```
sudo apt install apache2-utils
htpasswd -B -b -c /home/hduser/auth/htpasswd hduser p455word
```

### Stop and remove any existing Registry server
``` 
sudo docker container stop registry && sudo docker container rm -v registry
```

### Start the Registry server with Authentication and TLS
``` 
sudo docker run -d \
  --restart=always \
  --name registry \
  -v /home/hduser/auth:/auth \
  -e "REGISTRY_AUTH=htpasswd" \
  -e "REGISTRY_AUTH_HTPASSWD_REALM=Registry Realm" \
  -e REGISTRY_AUTH_HTPASSWD_PATH=/auth/htpasswd \
  -v /home/hduser//certs:/certs \
  -e REGISTRY_HTTP_ADDR=0.0.0.0:443 \
  -e REGISTRY_HTTP_TLS_CERTIFICATE=/certs/domain.crt \
  -e REGISTRY_HTTP_TLS_KEY=/certs/domain.key \
  -p 443:443 \
  registry:2
```

Verify the container is running by using this command: `sudo docker ps`

### Login and test that the registry is working
Run the following command on all k8s nodes and workstations to verify that
the docker registry is working.
``` 
## Run these commands on just one workstation
docker login docker.jaroof.local
docker pull busybox
docker tag busybox:latest docker.jaroof.local/my-busybox
docker push docker.jaroof.local/my-busybox

## Run this command on all nodes and workstations
docker login docker.jaroof.local
docker pull docker.jaroof.local/my-busybox
```

## Setup Kubernetes to use your Private Registry
### Setup a secret
First, you'll have to setup a secret with the login credentials for the docker
registry.  

>(IMPORANT!!! This will have to be setup for each namespace that uses this registry!)

``` 
kubectl create secret docker-registry regcred \
    --namespace=<your-namespace> \
    --docker-server=<your-registry-server> \
    --docker-username=<your-name> --docker-password=<your-pword> \
    --docker-email=<your-email>
    
### For example: 
### (e-mail is optional)
kubectl create secret docker-registry regcred \
    --namespace=mytest \
    --docker-server=docker.jaroof.local \
    --docker-username=hduser --docker-password=p455word
```

### (Option 1) Using Image Pull Secrets
Once the secret is setup in the namespace you want to deploy to, you can
add an `imagePullSecrets` section in the deployment/pod. 

This is kind of tedious, so I would recommend option 2 below, for a more streamlined
approach.

Example Deployment with the ImagePullSecrets:
``` 
apiVersion: apps/v1
kind: Deployment
metadata:
  name: my-app
  namespace: mytest
spec:
  template:
    spec:
      containers:
        - name: my-app
          image: docker.jaroof.local/my-app:latest
      imagePullSecrets:   ## <-- Use these two lines to setup the secret
        - name: regcred   ## <-- The secret from the last step
```

### (Option 2) Use the default service account 
This option is easier, since you only have to apply it once per namespace. It makes
use of the Default serviceaccount to apply the `imagePullSecrets` to all apps.

>(IMPORTANT!  This has to be applied to all namespaces that will make use of this docker registry)

```
 kubectl patch serviceaccount default \
  -p "{\"imagePullSecrets\": [{\"name\": \"regcred\"}]}" \
  -n <your-namespace>
```

## Using the Private Registry with Helm
With the latest templates that Helm creates, a serviceAccount is created for the project by default. 
Assuming you setup authentication to your registry using Option #2 above (Using the default serviceAccount) you'll
have to make the following changes in your `values.yaml` file for the chart:

>(NOTE: Another option would be to patch the service account created by helm with the `regcred` secret you created in
> previous steps.)

```yaml
serviceAccount:
  create: false
  name: "default"
```

## Docker commands
### Login to the Registry
```
docker login docker.jaroof.local
```

### Push images to your local Docker Registry Server
To push an image to your local Docker Registry Server, you'll first have to 'tag' the image using the URL of the
registry server, then perform the 'push' command.

`docker tag <IMAGE_NAME>:<VERSION> <REGISTRY_SERVER_URL:PORT/IMAGENAME>`
Here's an example:
```
docker tag exampleImage:1.0.0 localhost:5000/exampleImage
```

### Verify images were pushed to the local Docker Registry
Run the following comnand to view all images on your local docker registry:
```
curl -X GET http://192.168.6.2:5000/v2/_catalog
```

### Remove an image from the local docker registry
Run the following set of commands to remove an image from the docker registry:
>(NOTE: For this to work you should have set 'storage/enabled/delete=true' in your config.yml file in the create server step.)

```
# First, determine the image name:
curl -X GET http://localhost:5000/v2/_catalog

# Next, Find the 'Docker-Content-Digest' header value:
curl -v -H "Accept: application/vnd.docker.distribution.manifest.v2+json" -X HEAD http://localhost:5000/v2/<IMAGE_NAME_FROM_LAST_STEP>/manifests/<VERSION_GOES_HERE>

# Finally, using the 'Docker-Content-Digest' header, delete the image entry
curl -X DELETE http://localhost:5000/v2/<IMAGE_NAME_FROM_LAST_STEP>/manifests/<DIGEST_GOES_HERE>
```

## Teardown of the Registry server from Docker
```
sudo docker container stop registry && sudo docker container rm -v registry
```