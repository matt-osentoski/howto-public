# Setting Resource limits on Pods
On Kubernetes Pods, you have the ability to setup resource constraints.  For example, CPU and Memory limits.

>(IMPORTANT NOTE!!! If you don't set a limit, for example memory usage limit, Kubernetes could potentially use all
the available resources on the Node until the node freezes up and stops working.  I seen an example of this where a
default Kafka cluster install completely **locked up a cluster!!**)

In my opinion, a good practice is to apply a `LimitRange` to every non-system namespace in Kubernetes.  **This ensures that
a minimum memory limit is set for every pod**.  This will also prevent runaway applications from taking down the Kubernetes cluster.

In this folder is an example of a `LimitRange` that can be applied as a default to your namespaces.  To apply these defaults, run
the following command for each relevant namespace.

```
kubectl create -f resource-limit-defaults.yaml --namespace=default
```

## View limits on a pod
To view the resource limits on your pod, run the following command:

```
kubectl get pod my-pod-name --output=yaml --namespace
```

The `containers` / `resources`  section will contain the resource information


## View limits for a node
To view all resource allocations for a node, run the following command:

```
kubectl describe nodes my-node-name
```