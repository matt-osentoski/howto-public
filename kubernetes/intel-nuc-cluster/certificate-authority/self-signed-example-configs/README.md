# Certificate Authority config files
These files are derived from the repo linked below
, based on the `OPENSSL COOKBOOK` by Ivan Ristic [https://www.feistyduck.com/books/openssl-cookbook/](https://www.feistyduck.com/books/openssl-cookbook/)

>(REFERENCE: [https://github.com/ivanr/bulletproof-tls/tree/master/private-ca](https://github.com/ivanr/bulletproof-tls/tree/master/private-ca))

## Example Config Files

- **root-ca.conf** - This is the configuration file for the Root Certificate Authority (CA)
- **sub-ca.conf** - This is the Subordinate CA configuration file
- **example.com.conf** - This is the server certificate configuration, including an example of a wildcard
- **multi-domain.example.com.conf** - Similar to `example.con.conf`, this is a server certificate, that also shows how to configure multiple domains.

>(IMPORTANT: If you use multiple domain certs, You may have to allow the other domains in the `name-constraints` section
> of the `root-ca.conf` file.)

## Config file settings
You can view details about the config files from the `man` pages listed below:

```shell
man config
man ca
man req
```

The `man ca` outlines the individual settings in the config files. You can also view a web-based version
of these settings at: [http://man.he.net/?topic=ca&section=all](http://man.he.net/?topic=ca&section=all)

The `man req` shows the Distinguished naming of the cert: [http://man.he.net/?topic=req&section=all](http://man.he.net/?topic=req&section=all)