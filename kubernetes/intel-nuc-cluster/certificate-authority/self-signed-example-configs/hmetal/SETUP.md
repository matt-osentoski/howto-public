# Simple setup instructions to create certs for this domain

## Create the certs
### Directory Setup
```shell
### Create a directory for your certs. You can call this directory anything you want.
mkdir hmcloud
cd hmcloud

### Now create the sub-directories and files required
mkdir certs db private
chmod 700 private
touch db/index
openssl rand -hex 16 > db/serial
echo 1001 > db/crlnumber
```

### Copy config files to the base directory
>(NOTE: You should be in the `hmcloud/` directory)

Copy all `*.conf` files to the base directory

### CA cert
```shell
openssl req -new \
  -config root-ca.conf \
  -out root-ca.csr \
  -keyout private/root-ca.key
  
openssl ca -selfsign \
  -config root-ca.conf \
  -in root-ca.csr \
  -out root-ca.crt \
  -extensions ca_ext
```

### Create Subordinate CA
```shell
openssl req -new \
  -config sub-ca.conf \
  -out sub-ca.csr \
  -keyout private/sub-ca.key
  
openssl ca \
  -config root-ca.conf \
  -in sub-ca.csr \
  -out sub-ca.crt \
  -extensions sub_ca_ext
```

### Create the Server cert
```shell
openssl req -new \
  -config multi-domain.heavymetalcloud.lan.conf \
  -out heavymetalcloud.lan.csr \
  -keyout private/heavymetalcloud-encrypted.lan.key
  
openssl ca \
  -config sub-ca.conf \
  -in heavymetalcloud.lan.csr \
  -out heavymetalcloud.lan.crt \
  -extensions server_ext  
```

### Decrypt the Private key for some uses
```shell
openssl rsa -in private/heavymetalcloud-encrypted.lan.key -out private/heavymetalcloud.lan.key
```

