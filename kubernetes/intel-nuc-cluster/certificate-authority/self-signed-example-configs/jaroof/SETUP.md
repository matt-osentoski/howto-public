# Simple setup instructions to create certs for this domain

## Create the certs
### Directory Setup
```shell
### Create a directory for your certs. You can call this directory anything you want.
mkdir jaroof
cd jaroof

### Now create the sub-directories and files required
mkdir certs db private
chmod 700 private
touch db/index
openssl rand -hex 16 > db/serial
echo 1001 > db/crlnumber
```

### Copy config files to the base directory
>(NOTE: You should be in the `jaroof/` directory)

Copy all `*.conf` files to the base directory

### CA cert
```shell
openssl req -new \
  -config root-ca.conf \
  -out root-ca.csr \
  -keyout private/root-ca.key
  
openssl ca -selfsign \
  -config root-ca.conf \
  -in root-ca.csr \
  -out root-ca.crt \
  -extensions ca_ext
```

### Create Subordinate CA
```shell
openssl req -new \
  -config sub-ca.conf \
  -out sub-ca.csr \
  -keyout private/sub-ca.key
  
openssl ca \
  -config root-ca.conf \
  -in sub-ca.csr \
  -out sub-ca.crt \
  -extensions sub_ca_ext
```

### Create the Server cert
```shell
openssl req -new \
  -config multi-domain.jaroof.lan.conf \
  -out jaroof.lan.csr \
  -keyout private/jaroof-lan-encrypted.key
  
openssl ca \
  -config sub-ca.conf \
  -in jaroof.lan.csr \
  -out jaroof.lan.crt \
  -extensions server_ext  
```

### Decrypt the Private key for some uses
```shell
openssl rsa -in private/jaroof-lan-encrypted.key -out private/jaroof.lan.key
```

