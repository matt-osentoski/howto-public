# Setup client systems to use a Private Certificate Authority
This document describes how to setup different clients to use a self-signed cert with a
private certificate authority.  

>(NOTE: Review the README.md file in this directory to set up your own self-signed cert and CA)

## Git Client
Run the following commands to allow git to access a URL using a self-signed certificate.

In this example we will assume a Git server domain of 'gitlab.jaroof.local'. It's also assumed that you
have the CA cert in the 'C:\tmp\rootCA.pem' location.  

Run the following commands:
```bash 
git config --global http.sslBackend openssl

git config --global http.https://gitlab.jaroof.local/.sslcainfo "C:\tmp\rootCA.pem"
```