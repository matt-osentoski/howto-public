# Using a generated cert in an Ingress controller
This document describes how to create a secret containing the certificate chain and private key.  These
can be use to terminate TLS in k8s.

## Dependencies
First, make sure you read the 'README.md' in this directory and setup your own wildcard cert with the 
full certificate chain. 

## Install the cert and key as a secret.
This should be done at the location where the cert and key reside on the file system.

```shell 
kubectl create secret tls my-self-cert-secret --cert=full-chain-somedomain.local.crt --key=somedomain.local.key --namespace somenamespace
```

## Apply the secret to an Ingress controller
This example assumes you're using Helm.  Add the secret from the previous step to the Ingress configs,
including the domain name for the cert.

```yaml 
ingress:
  enabled: true
  annotations:
    kubernetes.io/ingress.class: nginx
    # kubernetes.io/tls-acme: "true"
  hosts:
    - host: www.somedomain.local
      pathType: ImplementationSpecific
      paths: ["/"]
  # -- ingress TLS config
  tls: 
    ####### This is the important line related to the secret you created in the previous step!
    - secretName: my-self-cert-secret  
      hosts:
        - www.somedomain.local
```

