# Create a private Certificate Authority  (CA)
This document will describe the steps required to create a private Certificate Authority (CA) and then generate
certificates from this CA cert.  (Including wildcard certs)  The documentation has evolved to use configuration files for
CA and subordinate CA generation. It also showcases the use of extensions that are required now by Apple Corp. etc. (extended key usages for 
client/server). For a reference you can see the new requirements by Apple: [https://support.apple.com/en-us/103769](https://support.apple.com/en-us/103769)

>(NOTE: These instructions have been updated to take the latest TLS practices into account. It is largely based on the
> excellent `OPENSSL COOKBOOK` by Ivan Ristic. [https://www.feistyduck.com/books/openssl-cookbook/](https://www.feistyduck.com/books/openssl-cookbook/)
> I would highly recommend downloading and reading this book.)

## Setup the  directory structure and config files
### Create the directory structure
```shell
### Create a directory for your certs. You can call this directory anything you want.
mkdir root-ca
cd root-ca

### Now create the sub-directories and files required
mkdir certs db private
chmod 700 private
touch db/index
openssl rand -hex 16 > db/serial
echo 1001 > db/crlnumber
```

### Create configuration files
To make CA generation easier, create configuration files.

An example set of these config files are under the directory: [self-signed-example-configs/](self-signed-example-configs/)

The config files `root-ca.conf`, `sub-ca.conf`, and any server certs like `example.com.ext` should be created at the base
of the directory structure that you setup in the previous step, for example: `root-ca/` directory.

>(REFERENCE: [https://github.com/ivanr/bulletproof-tls/tree/master/private-ca](https://github.com/ivanr/bulletproof-tls/tree/master/private-ca))

The most important lines to change in `root-ca-conf` are the following:
``` 
[default]
domain_suffix           = example.com

[ca_dn]
countryName             = "GB"
stateOrProvinceName     = "MI"
localityName            = "Detroit"
organizationName        = "Example"
commonName              = "Root CA"

[name_constraints]
permitted;DNS.0=example.com
permitted;DNS.1=example.org
```

The most important lines to change in `sub-ca.conf` are:
``` 
[default]
domain_suffix           = example.com

[ca_dn]
countryName             = "GB"
stateOrProvinceName     = "MI"
localityName            = "Detroit"
organizationName        = "Example"
commonName              = "Sub CA"
```

## Generate the Root CA
### Create the Private Key and Certificate Signing Request (CSR)
```shell
openssl req -new \
  -config root-ca.conf \
  -out root-ca.csr \
  -keyout private/root-ca.key
```

### Create the self-signed certificate
```shell
openssl ca -selfsign \
  -config root-ca.conf \
  -in root-ca.csr \
  -out root-ca.crt \
  -extensions ca_ext
```

### (OPTIONAL) Create a Certificate Revocation List (CRL) from the CA
```shell
openssl ca -gencrl \
  -config root-ca.conf \
  -out root-ca.crl
```

### (OPTIONAL) Create a key and CSR for OSCP signing
>(NOTE: Make sure in the `-subj` argument, that the Organization element `O=...` matches the Organization element in
> your Root CA Cert.)

```shell
openssl req -new \
  -newkey rsa:2048 \
  -subj "/C=US/O=Example/CN=OCSP Root Responder" \
  -keyout private/root-ocsp.key \
  -out root-ocsp.csr
```

### (OPTIONAL) Issue the OCSP Certificate from the CSR and Root CA
```shell
openssl ca \
  -config root-ca.conf \
  -in root-ocsp.csr \
  -out root-ocsp.crt \
  -extensions ocsp_ext \
  -days 30
```

### (OPTIONAL) Start the OCSP Responder service
This will start a socket service running on port `9080`

```shell
openssl ocsp \
  -port 9080 \
  -index db/index \
  -rsigner root-ocsp.crt \
  -rkey private/root-ocsp.key \
  -CA root-ca.crt \
  -text
```

### (OPTIONAL) Verify that OCSP service is working
```shell
openssl ocsp \
  -issuer root-ca.crt \
  -CAfile root-ca.crt \
  -cert root-ocsp.crt \
  -url http://127.0.0.1:9080
```

You should receive a response that looks something like this:
```
Response verify OK
root-ocsp.crt: good
        This Update: Oct  6 01:08:11 2024 GMT
```

## Generate the Subordinate CA
The subordinate CA (sometimes called intermediate certificate) is a way of issuing server certificates without having
to use the Root certificate. This reduces exposure of the Root CA's private key. It also allows for the Root CA holder
to delegate the issuing of server certs to other entities or departments.

### Generate the Subordinate CA private key and CSR
```shell
openssl req -new \
  -config sub-ca.conf \
  -out sub-ca.csr \
  -keyout private/sub-ca.key
```

### Issue the Subordinate CA cert using the Root CA
```shell
openssl ca \
  -config root-ca.conf \
  -in sub-ca.csr \
  -out sub-ca.crt \
  -extensions sub_ca_ext
```

## Generate a Server Certificate from the Subordinate CA
### Create a private key and CSR for the Server Cert.
```shell
openssl req -new \
  -config multi-domain.example.com.conf \
  -out example.com.csr \
  -keyout private/example.com.key
```

### Issue the Server Cert using the Subordinate CA
```shell
openssl ca \
  -config sub-ca.conf \
  -in example.com.csr \
  -out example.com.crt \
  -extensions server_ext
```

### Convert the Encrypted Private key to an RSA Private Key
Some applications have trouble using encrypted private keys (for example, OPN Sense, etc.)  To convert an encrypted key
to an RSA key, run the following command:

```shell
openssl rsa -in encrypted.key -out unencrypted.key
```


## Appendix
### Install the Certificate Authority on all your workstations
To prevent errors when using the certs generated below, make sure to install the CA certs into all
workstations that will be utilizing the certificates.

### Generate a simple keypair
```bash 
openssl genrsa -des3 -out rootCA.key 2048
```

### View the certificate
To view the certificate details, run the following command

```bash 
openssl x509 -in somedomain.local.crt -text -noout
```

### Chaining certificates
You can append the CA cert at the bottom of your generated domain cert to give the full cert chain. This
is sometimes required for some applications.

### View Chained Certificates
To view multiple certs chained together into one file, use a utility called `certtool`.  To install
and run, use the following commands:

```bash 
## If the tool isn't installed, run the following:
sudo apt install gnutls-bin

certtool -i < multiplecerts.pem
```