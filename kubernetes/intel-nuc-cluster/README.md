# Running a Kubernetes cluster on Intel Nuc servers
The files under this directory describe how to install, setup, and deploy applications onto a kubernetes cluster
running on Intel Nuc servers

Author: Matt Osentoski  
E-mail: matt.osentoski@gmail.com

## Dependencies
First, setup your Intel Nuc cluster, following the instructions located at:
`/nuc-cluster-setup/README.md`

## Install Kubernetes
`/kubernetes/intel-nuc-cluster/INSTALL.md` contains instructions for installing a kubernetes cluster on Intel Nuc hardware.

## Install a Container Registry
To deploy your own applications to the Kubernetes cluster, you will either have to use a public container
registry (like Docker Hub), or you can configure your own. The instructions below explain how to set up your own private
container registry within the cluster:

`/kubernetes/intel-nuc-cluster/container-registry/README.md`

>(IMPORTANT!! Make sure to setup each node's `insecure-registries` setting under `/etc/docker/daemon.json` or k8s
>will not be able to pull the image down from your private registry. You can ignore this if your registry is using TLS)

## Install a Load Balancer
Once Kubernetes is set up and running, install a Load Balancer to allow for external access
to the cluster. The instructions can be found at:

`/kubernetes/intel-nuc-cluster/loadbalancer-proxy/README.md`

## Install an Ingress Controller
Once the Load Balancer is in place and running, you can set up an Ingress Controller. The instructions
are located at:

`/kubernetes/intel-nuc-cluster/ingress/README.md`
'/kubernetes/intel-nuc-cluster/ingress/nginx-ingress-controller/README.md'

## (Optional) Install Dynamic Storage
This step could be involved, depending on if your Nuc clusters have two physical hard drives or one.
You can mimic dynamic storage using `local-storage` described in the readme listed below. This will
to avoid the complexity of a true dynamic storage class. Otherwise, pick one of the Rook/Ceph options below
based on the number of hard drives you have per node. 

```
### Local storage instructions:
/kubernetes/intel-nuc-cluster/dynamic-volume-provisioning/storage-classes/local-storage/README.md
/kubernetes/intel-nuc-cluster/example-applications-in-k8s/local-storage-helm/README.md

-OR- 

### Dynamic storage if your Nuc cluster only has one hard drive per node:
/kubernetes/intel-nuc-cluster/dynamic-volume-provisioning/storage-classes/rook.io/deprecated-directory-storage/README.md

-OR-

### Dynamic storage if your Nuc cluster has two or more hard drives per node:
/kubernetes/intel-nuc-cluster/dynamic-volume-provisioning/storage-classes/rook.io/README.md
```


