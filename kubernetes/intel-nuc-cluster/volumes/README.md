# Setting up persistent volumes in Kubernetes using NFS
This document describes the steps for connecting Kubernetes to an NFS file share. This is different
than having Kubernetes dynamically create a volume.  To learn about dynamic volume provisioning, check
out the folder under:
https://bitbucket.org/matt-osentoski/howto-public/src/master/kubernetes/intel-nuc-cluster/dynamic-volume-provisioning

## Setup NFS on Ubuntu
Follow the instructions in `ubuntu-nfs-setup.md`, in this directory to setup NFS on your Ubuntu servers.

## Create a Persistent Volume (PV)
From your Kubernetes cluster, run the following command to setup the Persistent Volume (PV).
>(NOTE: Use the `nfs-pv.yaml` file in this directory as a reference)

```
kubectl create -f nfs-pv.yaml
```

Check status of the volume:
```
kubectl get pv nfs-pv
```

## Create a Persistent Volume Claim (PVC)
From your Kubernetes cluster, run the following command to setup the Persistent Volume Claim (PVC).
>(NOTE: Use the `nfs-pvc.yaml` file in this directory as a reference)

```
kubectl create -f nfs-pvc.yaml
```

Check status of the PVC:
```
kubectl get pvc nfs-pvc
```

