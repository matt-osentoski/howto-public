# Volume Setup in Kubernetes
This document describes how to setup a shared volume that can be used with Kubernetes.  This will allow
storage to not be tied to a Node and allow for transient pods.  For this example, we will be using NFS, although other
shared volume stores can be used instead.

## Install NFS on worker nodes
On each worker node, install NFS:
```
sudo apt install nfs-kernel-server
```

## Setup the NFS export directory
```
sudo mkdir -p /srv/nfs/home
sudo chmod 777 -R /srv/nfs/home
```

## Configure the NFS mounts
Edit the exports file with volume configurations
```
sudo vi /etc/exports
```

Here's an example configuration
```
/srv/nfs/home    *(rw,sync,no_root_squash)
```

## Restart the NFS service
```
sudo systemctl start nfs-kernel-server.service
```

## Show all NFS mounted volumes
mount |grep nfs