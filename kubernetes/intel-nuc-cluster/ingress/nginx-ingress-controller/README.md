# Nginx Ingess Controller Installation
This document assumes you either have a cloud provided LoadBalancer, or you're using
MetalLB as the LoadBalancer. 

>(NOTE: To install MetalLB read the following document in this repository: `kubernetes/intel-nuc-cluster/loadbalancer-proxy/README.md`)

## Helm Installation
To install the Nginx ingress controller using Helm, run the following commands:

```
kubectl create namespace ingress-nginx
helm upgrade --install ingress-nginx ingress-nginx --repo https://kubernetes.github.io/ingress-nginx --namespace ingress-nginx
```

## Determine the External IP address
First, determine the external IP address of the nginx ingress controller service. Run the
following command:

```
kubectl get service ingress-nginx-controller --namespace ingress-nginx 
```

This should produce output similar to the following:

```
NAME                       TYPE           CLUSTER-IP      EXTERNAL-IP     PORT(S)                      AGE
nginx-ingress-controller   LoadBalancer   10.107.89.168   192.168.6.154   80:31871/TCP,443:30611/TCP   107s
```

In this case, the external IP address is `192.168.6.154` .

## Setup a Domain name
Since Ingress controllers are Layer 7, you will need to setup a domain name for the IP address
in the last section.  This can be done using a DNS server, or on Unix, with the hosts file `/etc/hosts`

On windows, the hosts file is located at: `C:\Windows\System32\drivers\etc\hosts`

>(NOTE: In Windows you will have to open an editor with Administrator privileges before opening the hosts file to
make changes)

Here is an example hosts entry.  In this example, the domain name will be:

```
192.168.6.154	k8scluster.nuc.internal
```
>(NOTE: According to RFC 5752 appendix G `internal` is a valid top level domain to use for
private networks.  If you have a public domain name, feel free to use that instead.
Ref: [https://tools.ietf.org/html/rfc6762#appendix-G](https://tools.ietf.org/html/rfc6762#appendix-G))

## Example Ingress Manifest

```yaml
apiVersion: extensions/v1beta1
kind: Ingress
metadata:
  name: chartmuseum-ingress
  annotations:
    # This will rewrite the path /chartmuseum below to / in the application
    # NOTE: The $2 is used in conjunction with the regex in the 'path' propety below.
    # In other words /charmuseum/otherstuff will translate to /otherstuff at the pod. 
    # This is important since the pod URL doesn't use a context path.
    nginx.ingress.kubernetes.io/rewrite-target: /$2
spec:
  rules:
  # This is the domain name we created in the section above.
  - host: k8scluster.nuc.internal
    http:
      paths:
      # we'll use this path with the domain name to specify the chartmuseum app
      # NOTE: The regular expression that allows URL pathing and params after the /museum path.
      - path: /chartmuseum(/|$)(.*)
        backend:
          # This is the ClusterIP service for the Chart Museum deployment
          serviceName: chartmuseum-chartmuseum
          # The ClusterIP service exposes port 8080
          servicePort: 8080
```

Save this yaml to a file called 'example-ingress.yaml' then run the following command to 
run it:

>(NOTE: This assumes the 'chartmuseum' application is running in a 'chartmuseum' namespace)

```
kubectl apply --namespace chartmuseum -f ingress-test.yaml
```
