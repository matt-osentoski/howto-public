# Ambassador installation and setup for Kubernetes
Ambassador is a microservice API Gateway for Kubernetes.  It uses Envoy proxies and can work alongside Istio for additional
Layer 7 (L7) needs.  Ambassador can handle high level load balancing needs, along with Authentication/Authorization, and
routing.

Reference URL: https://www.getambassador.io/

Author: Matt Osentoski  
E-mail: matt.osentoski@gmail.com

## Installation using Helm
Run the following command to install Ambassador on your cluster using Helm.
```
helm repo add datawire https://www.getambassador.io
kubectl create namespace ambassador
helm upgrade --install --namespace ambassador ambassador datawire/ambassador
```

## Determine the IP address for your Ambassador server
Ambassador will be exposed externally using an IP address allocated by the LoadBalancer.  To determine this IP address,
run the following command:
````
kubectl get services --namespace ambassador
````

The output will look something like this:
```
NAME                           TYPE           CLUSTER-IP      EXTERNAL-IP     PORT(S)                      AGE
my-release-ambassador          LoadBalancer   10.99.166.52    192.168.6.157   80:32538/TCP,443:31421/TCP   1d
```

You can see that the external IP address for Ambassador will be `192.168.6.157`.  This is important because you will setup
your DNS entries to point to this IP address.  The domains will then be used in the Routes you define below.

## Create an Ambassador route service
In this directory, take a look at the `example-ambassador-service.yaml` file.  This file is heavily commented with instructions
for creating a mapping for an existing app/service.  To creat this mapping, run the following command:
```
kubectl create -f example-ambassador-service.yaml
```

Once the route is setup, you should be able to access the application using the hostname in the route file.  Using my example
file it would be `http://getmesh/`.  This comes from the `host` and `prefix` entries in the Ambassador annotation.

## Debug UI
Ambassador has a web interface for debugging routes.  To run the UI, you will have to forward ports from a machine with
access to the k8s cluster.

First, find an Ambassador pod by running the following command:
```
kubectl get pods --namespace ambassador
```

This command should output something like this:
```
NAME                                     READY     STATUS    RESTARTS   AGE
my-release-ambassador-65b477b4bb-jxv96   1/1       Running   0          7h
my-release-ambassador-65b477b4bb-rtxnd   1/1       Running   0          7h
my-release-ambassador-65b477b4bb-wkc6s   1/1       Running   0          7h
```

Next, expose the UI via port forwarding from one of the pods, using the following command:
```
kubectl port-forward ambassador-6d9f98bc6c-5sppl 8877:8877 --namespace ambassador
```

You should now be able to access the Debug UI using the following URL:

http://localhost:8877/ambassador/v0/diag

