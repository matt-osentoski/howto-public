# Integrate Keycloak authentication to Ambassador

## Dependencies
Setup Keycloak using the following documents in this Git repo:
- `/kubernetes/intel-nuc-cluster/example-applications-in-k8s/keycloak/README.md`
- `/kubernetes/intel-nuc-cluster/example-applications-in-k8s/keycloak/keycloak-configuration.md`

Setup Ambassador using the following document:
- `/kubernetes/intel-nuc-cluster/ingress/ambassador/README.md`

## Enable Authentication to an exposed end point
In this example, I will be using a web service that is
exposed using Knative (with LinkerD and Ambassador as the service mesh and ingress controller, 
respectively)

### Configure a Client in KeyCloak
Follow the instructions at the link below to setup a client.  I'll summarize these steps below:

[https://www.getambassador.io/docs/latest/howtos/sso/keycloak/](https://www.getambassador.io/docs/latest/howtos/sso/keycloak/))

Log into keycloak using the instructions listed above.  Then follow these instructions
to setup a client.  This will be an endpoint that is currently exposed that we want to
secure using the KeyCloak IDP.  

- Under `Realm Settings` find the `name` of the realm.  You'll need this for the steps below.

- From the main menu, click `Clients` then click `Create` Enter the following settings and click `Save`:

``` 
Client ID: <You can put anything here this is your client identifier>
Client Protocol: "openid-connect"
Root URL: <This will be the base URL of your application. For example: https://helloworld-go.kn-apps.example.com/> 
```

- On the next screen configure the following options and click `Save`:
``` 
Access Type: "confidential"
Valid Redirect URIs: <This will be the base URL of your application. For example: https://helloworld-go.kn-apps.example.com/ or you can just use '*'> 
```

- Navigate to the `Mappers` tab in your Client and click `Create`. Configure the following options and click `Save`:

``` 
Protocol: "openid-connect".
Name: <This will be the name of the mapper.>
Mapper Type: select "Audience"
Included Client Audience: <Find the name of your Client. This will be used as the audience in the Keycloak Filter.>
```

## Setup Ambassador Filters to Integrate Keycloak

### Create a filter
The `Filter` acts as the authentication (authn) configuration. The manifest should look like this:

```yaml
---
apiVersion: getambassador.io/v2
kind: Filter
metadata:
  name: my-keycloak-filter
  namespace: {NAMESPACE_OF_YOUR_APP}
spec:
  OAuth2:
    authorizationURL: https://{KEYCLOAK_URL}/auth/realms/{KEYCLOAK_REALM}
    audience: {AUDIENCE_SETUP_FOR_YOUR_CLIENT}
    clientID: {CLIENT_ID_SETUP_IN_KEYCLOAK}
    secret: {CLIENT_SECRET}
    protectedOrigins:
    - origin: https://{PROTECTED_URL}
```

Here's an example to apply this to your cluster:
``` 
kubectl apply -f - <<EOF
---
apiVersion: getambassador.io/v2
kind: Filter
metadata:
  name: my-keycloak-filter
  namespace: kn-apps
spec:
  OAuth2:
    authorizationURL: http://192.168.6.162/auth/realms/knative-example
    audience: knative-example-app
    clientID: knative-example-app
    secret: d627c51b-a294-4729-b2c7-ca63d6f56704
    protectedOrigins:
    - origin: https://helloworld-go.kn-apps.example.com/
  ###
  ### For JWT Settings reference (https://www.getambassador.io/docs/latest/topics/using/filters/oauth2/#oauth-resource-server-settings) 
  ### NOTE: This sends headers to the upstream app
  ### JWT variables use Golang Template Strings {{ }}
  #
  #  injectRequestHeaders:
  #  - name: "X-Fixed-String"
  #    value: "Fixed String"
  #  - name: X-CLAIMS
  #    value: '{{ .token.Claims }}'
  #  - name: X-Token-String
  #    value: '{{ .token.Raw }}'
  #  - name: X-Authorization
  #    value: Authenticated {{ .token.Header.typ }}; sub={{ .token.Claims.sub }}; name={{printf "%q" .token.Claims.name }}
    
EOF
```

### Apply an Ambassador Filter Policy
The Filter Policy acts as the authorization (authz) filter. The manifest should look like this:

```yaml
---
apiVersion: getambassador.io/v2
kind: FilterPolicy
metadata:
  name: httpbin-policy
  namespace: {NAMESPACE_OF_YOUR_APP}
spec:
  rules:
    - host: "*"
      path: {URL_PATH_TO_FILTER}
      filters:
        - name: {FILTER_NAME_FROM_THE_PREVIOUS_STEP)
          arguments:
            scope:
            - "offline_access" ## Enter your scopes from Keycloak here
```

Here's an example to apply this to your cluster:

``` 
kubectl apply -f - <<EOF
apiVersion: getambassador.io/v2
kind: FilterPolicy
metadata:
  name: rootpath-policy
  namespace: kn-apps
spec:
  rules:
    - host: "*"
      path: /
      filters:
        - name: my-keycloak-filter
          arguments:
            scope:
            - "offline_access"
EOF
```