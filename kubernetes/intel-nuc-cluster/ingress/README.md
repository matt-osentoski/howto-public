# Ingress Setup
Ingress allows inbound access into your kubernetes cluster from external machines (like your PC)
Ingress often uses a LoadBalancer in the cloud or NodePorts for simple bare metal installations.

## Layer 7
Ingress differs from a LoadBalancer in that it operates at Layer 7.  Meaning, it uses URLs and
URL paths to direct traffic to a Kubernetes Service. For example, `mydomain.corp/chartmuseum` could
point to a kubernetes services that directs traffic to a Helm Chart Museum pod.

If you were simply using a LoadBalancer, you would have to point to the IP address/port allocated
from the load balancer.  For example: `192.168.5.140:8080`.  You could then manually assign
a domain name to this address.

## Benefits over directly using a LoadBalancer.
The benefit of using an Ingress as opposed to directly using a LoadBalancer is that the 
Ingress can route multiple applications over a single IP address and single loadbalancer.  Whereas
a LoadBalancer (especially in the cloud) would have to allocated a new IP address for each
application.  This could get expensive.

## Setup
To setup an Ingress, you will need an `Ingress Controller`. For example, nginx ingress controller
which will require a LoadBalancer, or NodePort to bridge the communication outside your
cluster.  In the case of a LoadBalancer, the ingress controller would use an external IP address.
You would then setup DNS for this IP address and use that domain name for your Ingress service calls.

Nginx Ingress Controller Install Instructions: [https://kubernetes.github.io/ingress-nginx/deploy/](https://kubernetes.github.io/ingress-nginx/deploy/)