# DEPRECATED - Ubuntu Image installation
>(IMPORTANT!!! This document is deprecated, us the following document instead: [../../nuc-cluster-setup/ubuntu-image-install.md](../../nuc-cluster-setup/ubuntu-image-install.md))

This document describes how to install Ubuntu from a USB drive.  For this process, I'm using
a USB image writer package called `Etcher`: https://etcher.io  

These instructions are specifically for a Kubernetes cluster that will be using Rook.io/Ceph for dynamic storage.

Author: Matt Osentoski  
E-mail: matt.osentoski@gmail.com

## Create an Ubuntu Image on USB
1. Download Etcher at https://etcher.io  
2. Download a server image of Ubuntu (I'm using 18.04.x server amd64).  https://www.ubuntu.com/download/server  
The file should end in `.iso`
3. Using Etcher from step 1, write the ISO image of Ubuntu onto a formatted USB drive.
4. Once the image has been written to the USB drive eject it from your computer.

## Install Ubuntu
1. Insert the USB drive into your NUC.  
    * If Ubuntu does not boot, check that the USB drive is USB version 2.  Some of the older NUCs can't read USB 3.
Also, you may have to setup your BIOS to allow a USB drive as a bootable device.
https://www.intel.com/content/www/us/en/support/articles/000005471/mini-pcs.html
2. Select the default `Install ubuntu server` option or wait a few seconds for the installation to start.
3. Select the default values for language and keyboard.
4. Select `Install Ubuntu`
5.  If DHCP is setup to the Internet you can continue. Otherwise, you will have to manually select an
IP address and point to your default gateway for internet access. 
(IMPORTANT!! if you aren't using DHCP, make sure the DNS server is set. In my case that is 192.168.1.254)
6. If you have a Proxy server for Internet access, enter it next. Otherwise, leave this blank and
continue.
7. Select the default mirror address `http:/archive.ubuntu.com/ubuntu`
8. MASTER SERVER ONLY!!!   Select `Use An Entire Disk` for the partitioner.

    **Worker Nodes, should use the following:  Select 'Manual' for the partitioner**
    * Under 'Available Devices' Select the drive -> Make Boot device
    * Under 'Available Devices' Select the drive -> Add Partition 
        * Size: 60G
        * Format: ext4
        * Mount: /
    * Under 'Available Devices' Select the drive -> Add Partition
        * Size: 51.288G
        * Format: Leave Unformatted
9. Select the drive to format and install then continue.  
10. You will be prompted to verify if you want to erase the contents of the drive.  Select `Continue`
11. Enter Profile settings (NOTE: you can tweak these settings to match your environment):
    * Your name: `hduser`
    * Your Server's Name: `nuc-node1`
    * Pick a username: `hduser`
    * Choose a password: `password`
    * Confirm your password: `password`
12. Check the box to `Install OpenSSH server`.  Leave the other settings default.
13. The server will now begin the install of Ubuntu.  This will take a few minutes.
14. With the installation complete, remove the USB drive and select `Reboot Now`
15. Verify that you can login to the server using the credentials from step 11. above.   
16. Verify that you can SSH into the server remotely using the IP address you used/entered in step 5. 

At this point Ubuntu should be installed and ready to setup.  (See the `required/ubuntu-setup.md` document for details)