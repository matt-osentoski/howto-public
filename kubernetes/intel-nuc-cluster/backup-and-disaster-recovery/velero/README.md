# Velero
Velero is a backup and disaster recovery tool for k8s. Backups are stored using an object store.

>(NOTE: Velero currently requires an object store, similar to S3.  In this example, I'll be using
Ceph's S3 compatible object store.)

This document is derived from the following URLS:

[https://www.objectif-libre.com/en/blog/2020/01/10/kubernetes-backup-stateful-apps/](https://www.objectif-libre.com/en/blog/2020/01/10/kubernetes-backup-stateful-apps/)

[https://github.com/vmware-tanzu/helm-charts/tree/master/charts/velero](https://github.com/vmware-tanzu/helm-charts/tree/master/charts/velero)

## Dependencies
This document assumes you have Rook.io/Ceph running on the cluster with the Object Store setup.
It's also assumed that the store name is: `s3-store`  The internal URL to the Object store, based on
the store name, should be: `http://rook-ceph-rgw-s3-store.rook-ceph.svc.cluster.local`

If any of these values are different, adjust the `values-override.yaml` to accommodate the changes.

## Client install
https://github.com/vmware-tanzu/velero/releases

## Server install using Helm
### Add the VMWare repo to Helm
```
helm repo add vmware-tanzu https://vmware-tanzu.github.io/helm-charts
```

## Create the Velero namespace
```
kubectl create namespace velero
```

## Update the Velero credentials file
Update the `velero-creds` file to include the 'access key id' and 'secret access key' from your Ceph Object Store.

## Create a secret
>(NOTE: This uses the 'velero-creds' file located in the directory of this README)

```
kubectl create secret generic s3-velero-creds --namespace velero --from-file cloud=velero-creds
```

## Create an Object Bucket
In your S3 Object store, create a bucket called `velero-backup`

## Install the Helm chart
```
helm upgrade --install --values values-override.yaml --namespace velero velero vmware-tanzu/velero
```

## Show backups
To view existing backups, run the following command:
``` 
kubectl -n velero get backups
```

## Backup

### Entire Cluster
To backup the entire cluster, run the following command 
``` 
velero backup create <backup_name>
```

### Backup Specific Resources
To backup specific resources, run one of the following commands:
``` 
velero backup create <backup_name> --include-namespaces testing
velero backup create <backup_name> --include-namespaces testing --snapshot-volumes
velero backup create <backup_name> --include-namespaces testing --ttl 2h
velero backup create <backup_name> --include-namespaces testing --include-resources pods,deployments
velero backup create <backup_name> --include-namespaces testing --exclude-resources pods
velero backup create <backup_name> --exclude-namespaces testing
velero backup create <backup_name> --exclude-recourses configmaps
```

### Verify the Backup
Run the following commands to view and debug the backup:
``` 
velero backup describe <backup_name>
velero backup logs <backup_name>
```

## Scheduling
### View existing schedules
``` 

```

### Create a schedule
```
velero schedule create <schedule_name> --schedule="* * * * *"
velero schedule create <schedule_name> --schedule="/30"
velero schedule create <schedule_name> --schedule="@every 1m" --include-namespaces testing
velero schedule create <schedule_name> --schedule="@every 2d"
velero schedule create <schedule_name> --schedule="@every 1w"
```

### Delete a Schedule
``` 
velero schedule delete <schedule_name>
velero schedule delete --all
```

## Restore
### View existing Restores
``` 
velero restore get
```

### Create a Restore
``` 
velero restore create <restore_name> --from-backup <backup_name>
velero restore create <restore_name> --from-backup <backup_name> --namespace-mappings <original_ns>:<new_namespace>
```


### Delete a Restore
``` 
velero restore delete <restore_name>
```


