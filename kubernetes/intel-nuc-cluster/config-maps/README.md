# Config Maps
Config maps allow application configurations to be externalized to the kubernetes cluster.  This is very similar to
Consul, Zookeeper, etcd, etc.
(Ref: https://kubernetes.io/docs/tasks/configure-pod-container/configure-pod-configmap/#define-container-environment-variables-using-configmap-data)

Author: Matt Osentoski  
E-mail: matt.osentoski@gmail.com

## Configuration Example
The example application within this repo has a sample showing how Config Maps can be used to override application.properties
configurations in a Spring boot app.

## Sample Config map
Below is a sample config map that maps the `hello.world` key to the value `Config from kubernetes!!!!`
```yaml
kind: ConfigMap
apiVersion: v1
metadata:
  name: k8s-example-ws-config
data:
  hello.world: Config from kubernetes!!!
```

## Map a config map key/value to an environment variable
You can take the key/value pair above and expose it as an environment variable in your pods.  Below is a snippit
from a deployment manifest showing where this config would go:
```yaml
apiVersion: v1
   kind: Deployment
   metadata:
     name: my-deployment-name
   spec:
     containers:
       - name: my-container
         image: someNamespace/someContainer
         # This section will set an ENV variable in the container from a config Map
         env:
           # Define the environment variable (NOTE: Springboot maps this to 'hello.world')
           - name: HELLO_WORLD
             valueFrom:
               configMapKeyRef:
                 # The ConfigMap containing the value you want to assign to SPECIAL_LEVEL_KEY
                 name: k8s-example-ws-config
                 # Specify the key associated with the value. (NOTE: This is the key in the config map, not to be
                 # confused with the application property that will be in the spring boot app.  That property name
                 # will be derived from the env. variable above which will be converted from HELLO_WORLD to
                 # hello.world in spring boot.)
                 key: hello.world
```

