exit_after_auth = true
pid_file = "/home/vault/pidfile"

auto_auth {
    method "approle" {
        config = {
            role_id_file_path = "/etc/vault/role-id.txt",
            secret_id_file_path = "/etc/vault/secret-id.txt"
        }
    }

    sink "file" {
        config = {
            path = "/home/vault/.vault-token"
        }
    }
}