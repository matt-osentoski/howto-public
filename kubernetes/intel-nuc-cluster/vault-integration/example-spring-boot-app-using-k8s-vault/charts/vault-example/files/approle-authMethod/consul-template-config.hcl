vault {
  renew_token = false
  vault_agent_token_file = "/home/vault/.vault-token"
  retry {
    backoff = "1s"
  }
}

template {
  destination = "/etc/secrets/config.properties"
  contents = <<EOH
{{- with secret "secret/myapp/config" }}
database.user={{ .Data.data.DB_USER }}
database.password={{ .Data.data.DB_PASSWORD }}
{{- end }}
  EOH
}