package org.jaroof.vaultexample.config;

import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.configuration.reloading.FileChangedReloadingStrategy;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import java.io.File;

/**
 * Reloadable external properties file.
 * Derived from: https://www.baeldung.com/spring-reloading-properties
 */
@Configuration
public class Secrets {

    @Bean
    @ConditionalOnProperty(name = "spring.config.location", matchIfMissing = false)
    public PropertiesConfiguration propertiesConfiguration(@Value("${spring.config.location}") String path)
            throws Exception {
        String filePath = new File(path).getAbsolutePath();
        PropertiesConfiguration configuration = new PropertiesConfiguration(
                new File(filePath));
        configuration.setReloadingStrategy(new FileChangedReloadingStrategy());
        return configuration;
    }
}
