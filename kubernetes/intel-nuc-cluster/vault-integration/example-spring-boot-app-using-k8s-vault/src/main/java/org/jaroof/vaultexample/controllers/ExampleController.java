package org.jaroof.vaultexample.controllers;

import org.apache.commons.configuration.PropertiesConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ExampleController {

    private PropertiesConfiguration propertiesConfiguration;

    public ExampleController(PropertiesConfiguration propertiesConfiguration) {
        this.propertiesConfiguration = propertiesConfiguration;
    }

    @RequestMapping("/")
    public String home() {
        String env = propertiesConfiguration.getString("database.password", "default value");

        return "Environment variable is: " + env;
    }

}
