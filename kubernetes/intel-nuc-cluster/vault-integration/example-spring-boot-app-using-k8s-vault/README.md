# Example project exploring different configurations for accessing Vault secrets.

**References**

[https://learn.hashicorp.com/vault/identity-access-management/vault-agent-k8s](https://learn.hashicorp.com/vault/identity-access-management/vault-agent-k8s)

[https://github.com/hashicorp/vault-guides/tree/master/identity/vault-agent-k8s-demo](https://github.com/hashicorp/vault-guides/tree/master/identity/vault-agent-k8s-demo)

## Build (including docker imege build and push)
```
mvn clean install
```

## Kubernetes install using Helm
To deploy to Kubernetes using Helm/Tiller, run the following commands below:

```
cd charts/vault-example
helm upgrade --install --namespace examples -f values.yaml vault-example .
```

## Remove the application in Kubernetes
To remove all traces of the application run the following commands:

```
helm del --purge vault-example
kubectl delete namespace examples
```