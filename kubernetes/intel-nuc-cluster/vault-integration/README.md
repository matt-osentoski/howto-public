# Vault integration with Kubernetes
This document describes how to connect and integrate Vault Server into your
Kubernetes secrets.

## Run the Vault server

### Download the Vault binary
https://www.vaultproject.io/downloads.html

### Set the Environment variable
Assuming you're using HTTP and not HTTPS, run the following command to set your
environment variable:

```
export VAULT_ADDR='http://127.0.0.1:8200'
```

### Start the server
Run the following command to start the Vault server in 'dev' mode

>(NOTE: The `-dev-listen-address` allows the server to accept requests from all IP addresses the
server uses)

```
vault server -dev -dev-listen-address="0.0.0.0:8200"
```

## Kubernetes Configurations

### Create a service Account
```
kubectl create serviceaccount vault-auth --namespace examples
```

### Create a ClusterRoleBinding object for a namespace
Modify the `k8s-manifests/ClusterRoleBinding.yaml` file.  Change the `namespace` values to match your system
then run the following command:

```
kubectl apply -f k8s-manifests/ClusterRoleBinding.yaml
```

### Determine the values required to enable Kubernetes Auth in Vault
In addition to the `SA_JWT_TOKEN` and `SA_CA_CRT` variables you will need to determine
the server URL of your cluster.  This can typically be found in your `~/.kube/config` file.

>(NOTE: In this case we're using the 'examples' namespace.  Change this to an appropriate namespace for your system.)

```
export VAULT_SA_NAME=$(kubectl --namespace=examples get sa vault-auth -o jsonpath="{.secrets[*]['name']}")
export SA_JWT_TOKEN=$(kubectl  --namespace=examples get secret $VAULT_SA_NAME -o jsonpath="{.data.token}" | base64 --decode; echo)
export SA_CA_CRT=$(kubectl  --namespace=examples get secret $VAULT_SA_NAME -o jsonpath="{.data['ca\.crt']}" | base64 --decode; echo)
```

## Kubernetes Authentication Method
### Enable Kubernetes Auth in Vault
```
export VAULT_ADDR='http://127.0.0.1:8200'
vault auth enable kubernetes
```

### Configuration

>(NOTE: `kubernetes_host` value can be found in your `~/.kube/config` file.)

```
vault write auth/kubernetes/config \
  token_reviewer_jwt="$SA_JWT_TOKEN" \
  kubernetes_host="https://192.168.6.2:6443" \
  kubernetes_ca_cert="$SA_CA_CRT"
```

### Create a Policy
Take a look at the `example-policy.hcl` file in this directory.  Change as needed.  Run the following command
to add this policy into Vault.

```
vault policy write example-policy example-policy.hcl
```

### Setup a role for Kubernetes Auth

>(NOTE: That we're using the `examples` namespace in this case.  Change this value to your namespace, as needed)

```  
vault write auth/kubernetes/role/examplerole \
    bound_service_account_names=vault-auth \
    bound_service_account_namespaces=examples \
    policies=example-policy \
    ttl=24h
```

### Test that everything is working
Now that the previous steps are configured, you can create a pod using the `vault-auth` serviceAccount.
This will create a file in the container called: `/var/run/secrets/kubernetes.io/serviceaccount/token`
that contains the Vault token that can be used to retrieve secrets.

Create the following temporary container to see this working.  
>(NOTE: We're using the 'examples' namespace where the serviceAccount was created)

```
kubectl --namespace=examples run tmp --rm -i --tty --serviceaccount=vault-auth --image alpine
```

This command will launch a container with a terminal prompt.  From the prompt, type the following command to 
view the token:

```
cat /var/run/secrets/kubernetes.io/serviceaccount/token
```

## (Optional) AppRole Authentication Method
AppRole is an alternative to the Kubernetes Auth method.  If you're already
using the Kubernetes Auth Method, you can probably skip this section.

### Enable Kubernetes Auth in Vault
```
vault auth enable approle
```

### Setup a role for AppRole Auth
>(NOTE: `secret_id_ttl` determines when you have to renew your secret_id for this role_id)
```
vault write auth/approle/role/example-role \
    secret_id_ttl=365d \
    token_num_uses=10 \
    token_ttl=1440m \
    token_max_ttl=2880m \
    policies=example-policy \
    secret_id_num_uses=40
```

### Retrieve the Role ID
```
vault read auth/approle/role/example-role/role-id
```

### Retrieve the Secret ID
```
vault write -f auth/approle/role/example-role/secret-id
```

### Login using Role ID / Secret ID to obtain a Vault token
First, create a file called `payload.json` with the role id / secret id

```json
{
  "role_id": "5b5817f5-7db1-31ea-1943-1dbecf797ab3",
  "secret_id": "4e692046-914a-e2d4-0cc7-08de52401b07"
}
```

Next, make a curl call using the JSON file created above:

```shell
curl --request POST --data @payload.json http://127.0.0.1:8200/v1/auth/approle/login
```
In the response under 'client_token' will be the Vault token that you can use to retrieve
secrets, etc.

### (Optional) Retrieve the Role ID using curl
```
curl --header "X-Vault-Token: ..." \
    http://127.0.0.1:8200/v1/auth/approle/role/my-role/role-id
```

### (Optional) Retrieve the Secret ID using curl
```
curl --header "X-Vault-Token: ..." \
    --request POST \
     http://127.0.0.1:8200/v1/auth/approle/role/my-role/secret-id
```

## Create secrets
### Create some Key/Value pairs
Next, we'll create a Database username and password as vault secrets

```
vault kv put secret/myapp/config DB_USER='dbuser' \
        DB_PASSWORD='toomanysecrets' \
        ttl='5s'
```

### Verify the Key/Value pairs
Run the following command to view the key/values pairs you created in the last step:

```
vault kv get secret/myapp/config
```

### Verify the Key/Value pairs using curl
```
curl -X GET -H "X-Vault-Token:s.ABCdefg1234567890" http://127.0.0.1:8200/v1/secret/data/myapp/config
```