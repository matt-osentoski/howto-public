# OpenWhisk Serverless Platform
OpenWhisk is Serverless cloud platform that runs on Kubernetes.  This document will describe how to install
and setup OpenWhisk using Helm charts.

Author: Matt Osentoski  
E-mail: matt.osentoski@gmail.com

## Installation using Helm
>(NOTE: This document assumes you already have Halm installed and Tiller setup on your Kubernetes cluster. If not,
take a look at the instructions in the 'helm' folder two directories up from this document.)

The instructions are derived from the following URL: [https://github.com/apache/incubator-openwhisk-deploy-kube/tree/master/helm/openwhisk]() 

### Setup Node labels
Mark the nodes that can accept OpenWhisk pods by creating labels.  For each node in your cluster that will be running
Open Whisk, run the following command:

```
kubectl label nodes <INVOKER_NODE_NAME> openwhisk-role=invoker

# For example: kubectl label nodes nuc-data1 openwhisk-role=invoker
```

### Clone the Helm chart from Git
Clone the helm chart using the following command:

```
git clone https://github.com/apache/incubator-openwhisk-deploy-kube.git
```

### Modify the `values.yaml` file.
In this directory is a `values.yaml` file which can be altered to suit your needs.  The changes I made to the file are
listed below:

```yaml
whisk:
    ingress:
        apiHostProto: "http"
        type: LoadBalancer
k8s:
    persistence:
       enabled: true
       hasDefaultStorageClass: false
       explicitStorageClass: rook-ceph-block

```

### Install the helm chart
To install the helm chart, navigate to the helm chart location in the Git repository and point to the `values.yaml` file that we updated
above.  In this case we will rename the `values.yaml` to `my-values.yaml` to avoid confusion with the original values.yaml file in the Git Repo:

```
cd incubator-openwhisk-deploy-kube/helm
helm install openwhisk --namespace openwhisk --name openwhisk -f my-values.yaml
```

## Setup the OpenWhisk Command Line Interface (CLI)

### Download the CLI tool for your operating system
[https://openwhisk.apache.org/documentation.html]()

### Install the CLI tool per the instructions
Make sure the `wsk` commnad is available from your terminal via the PATH.

[https://github.com/apache/incubator-openwhisk-cli/releases]()

### Determine the Auth key value
The CLI tooling requires that a `~/.wskprops` file is setup with the default values to 
your OpenWhisk cluster.  The first step in setting this up is to determine your 'Auth Key' 
value.  Below are a few ways to find this value:

**Option #1:**

Assuming you used Helm to install the cluster, the `values.yaml` file has an entry `whisk.auth.guest`.  This
value will be a long string delimited by a colon.  For example: 
```yaml
guest: 23bc46b1-71f6-4ed5-8c54-816aa4f8c502:123zO3xZCLrMN6v2BKK1dXYFpXlPkccOFqm12CdAsMgRU4VrNZ9lyGVCGuMDGIwP
```
is the value in the yaml file, the auth Key value will be: 
```
23bc46b1-71f6-4ed5-8c54-816aa4f8c502:123zO3xZCLrMN6v2BKK1dXYFpXlPkccOFqm12CdAsMgRU4VrNZ9lyGVCGuMDGIwP
```

**Option #2:**

You can also obtain the value from a Kubernetes secret.  Run the following command to retrieve the guest auth key:
```
kubectl get secret --namespace openwhisk openwhisk-whisk.auth -o jsonpath="{.data.guest}" | base64 --decode; echo
```

Again, the string is delimited with a colon. You will use the second part as the Auth value, similar to Option #1 above.

### Determine the API Host address
Assuming you setup a node port or LoadBalancer, you should be able to run the following command to determine the 
API Host address:

```
kubectl --namespace openwhisk get service openwhisk-nginx
```

The output should look something like this:

```
NAME              TYPE           CLUSTER-IP      EXTERNAL-IP     PORT(S)                      AGE
openwhisk-nginx   LoadBalancer   10.96.236.165   192.168.6.154   80:32077/TCP,443:30963/TCP   1d
```

In this example, the API Host address will be `http://192.168.6.154:80`

### Setup the CLI using your API Host and Auth key
Using the values determined above, run the following command on your workstation to setup the OpenWhisk CLI
```
wsk property set --apihost http://192.168.6.154:80 --auth 23bc46b1-71f6-4ed5-8c54-816aa4f8c502:123zO3xZCLrMN6v2BKK1dXYFpXlPkccOFqm12CdAsMgRU4VrNZ9lyGVCGuMDGIwP --namespace guest
```

### Inspect the `~/.wskprops` file
The command from the last step should have created a file in your home directory called `~/.wskprops`.  You 
can inspect this file with a text editor to see the changes that were made.  It should look something like this:
```
AUTH=23bc46b1-71f6-4ed5-8c54-816aa4f8c502:123zO3xZCLrMN6v2BKK1dXYFpXlPkccOFqm12CdAsMgRU4VrNZ9lyGVCGuMDGIwP
APIHOST=http://192.168.6.154:80
NAMESPACE=guest
```

## Create a test action
Assuming you have the `wsk` CLI tool available in your system path, you can create a test 'Hello World' action
using the following steps. 

>(NOTE: In addition to the `wsk` command tool, there's a nice thick client app that gives a more intuitive 
interface to the wsk commands.  The tool can be downloaded here:
[https://github.com/IBM/kui]() )

### Create a Hello World action
Using the `hello.js` file in this directory, run the following command to create a test action:
```
wsk action create helloJS hello.js
```

To view the action run:
```
wsk action list
# or
wsk action get helloJS
```

To invoke (run) the action, use the following commands:
```
wsk action invoke helloJS --blocking
# or
wsk action invoke helloJS
```

To update the action into a RESTful web service, run the following command:
```
wsk action update "helloJS" --web true
```

View the URL of the web service using this command:
```
wsk action get helloJS --url
```
Using this URL, you can now test the web service.

## Create a test composition of actions
A composition is a series of actions that occur in a pipe or sequence.  In the `composition` folder in the same
directory as this README file are examples.  The flow will be:

```
input.js --> world.js --> to-rest.js
```

Run the following commands to create the actions and the composition:
```
# First, let's create the separate actions.
wsk action create inputJS input.js
wsk action create worldJS world.js
wsk action create toRestJS to-rest.js

# Now create the composition that connects the actions together into a new web action
wsk action create compositionTest --sequence inputJS,worldJS,toRestJS --web true

# Finally, lookup the URL that can be used to access the web service
wsk action get compositionTest --url
```

To test the service, use the following URL:

[http://192.168.6.154/api/v1/web/guest/default/compositionTest?name=Hello]()

