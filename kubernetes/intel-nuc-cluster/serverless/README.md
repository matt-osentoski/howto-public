# Serverless platforms on Kubernetes
This folder contains HOWTO's for serverless execution platforms that run inside Kubernetes.  These platforms should mimick
Amazon's Lambda function service.

For more information about Serverless, take a look at the link below:

https://serverless.com/