# Install Knative for serverless applications without Istio (Using Ambassador/LinkerD)
>(NOTE: These instructions are derived from: [https://knative.dev/docs/install/knative-with-operators/](https://knative.dev/docs/install/knative-with-operators/))

## (Dependency) Setup Ambassador and LinkerD
In this install, we will be using Ambassador instead of Istio as the Ingress 
controller and LinkerD as the Service Mesh.
Follow the instructions below in this Git repo to install both:

`/kubernetes/intel-nuc-cluster/ambassador/README.md`

`/kubernetes/intel-nuc-cluster/service-mesh/linkerd/README.md`

## Install the serving sub-system
>(NOTE: At this time, the Knative Operator doesn't seem to work well when not using Istio
> as the Ingress controller.  For now, I'll use standard manifest installation)

```
kubectl apply --filename https://github.com/knative/serving/releases/download/v0.21.0/serving-crds.yaml
kubectl apply --filename https://github.com/knative/serving/releases/download/v0.21.0/serving-core.yaml
```

### Setup LinkerD injection for the Knative Serving Namespace
This command will add a label to the `knative-serving` namespace. By adding this label, Ambassador
will automatically wire up the pods using Sidecar injection

``` 
kubectl patch clusterrolebinding ambassador -p '{"subjects":[{"kind": "ServiceAccount", "name": "ambassador", "namespace": "ambassador"}]}'
kubectl set env --namespace ambassador  deployments/ambassador AMBASSADOR_KNATIVE_SUPPORT=true

kubectl patch configmap/config-network \
  --namespace knative-serving \
  --type merge \
  --patch '{"data":{"ingress.class":"ambassador.ingress.networking.knative.dev"}}'

#### This may not be needed ## kubectl label namespace knative-serving linkerd.io/inject=enabled
kubectl annotate ns knative-serving linkerd.io/inject=enabled
kubectl rollout restart deploy -n knative-serving
```

### Verify the serving deployment was successful
``` 
kubectl get deployment -n knative-serving
```

## Install Eventing

``` 
kubectl apply --filename https://github.com/knative/eventing/releases/download/v0.21.0/eventing-crds.yaml
kubectl apply --filename https://github.com/knative/eventing/releases/download/v0.21.0/eventing-core.yaml
```

### Setup Kafka as an Event Channel
>(NOTE: You'll have to put your Kafka Broker address after 'REPLACE_WITH_CLUSTER_URL)
>
``` 
curl -L "https://github.com/knative-sandbox/eventing-kafka/releases/download/v0.21.0/channel-consolidated.yaml" \
 | sed 's/REPLACE_WITH_CLUSTER_URL/bit-kafka.kafka.svc.cluster.local:9092/' \
 | kubectl apply --filename -
```

### Install Broker Eventing using Kafka

``` 
kubectl apply --filename https://github.com/knative-sandbox/eventing-kafka-broker/releases/download/v0.21.0/eventing-kafka-controller.yaml
kubectl apply --filename https://github.com/knative-sandbox/eventing-kafka-broker/releases/download/v0.21.0/eventing-kafka-broker.yaml
```

### Update the Broker Eventing URL
Update the URL with your Kafka Broker URL

``` 
kubectl patch configmap/kafka-broker-config \
  --namespace knative-eventing \
  --type merge \
  --patch '{"data":{"bootstrap.servers":"bit-kafka.kafka.svc.cluster.local:9092"}}'
```

### Restart the Eventing Pods
Restart the eventing pods to let the new broker URL register with the eventing system
``` 
kubectl rollout restart deploy -n knative-eventing
```

### Verify eventing deployment was successful
``` 
kubectl get deployment -n knative-eventing
```

## Fix / Hack the Ambassador ClusterRoles
There's a bug where you have to manually update the ClusterRoles for Ambassador to allow
KNative to use the ingress.

### (Optional) Find the current ClusterRole yaml contents:
> (NOTE: This file is already included in the same directory as this README.md file. This is just
> to illustrate how to get the Yaml contents)

``` 
kubectl get clusterroles ambassador-crds -o yaml > ambassador-crd-clusterrole-change.yaml
```

### (Optional) Add the following rules to the ClusterRole for Ambassador
> (NOTE: This file is already included in the same directory as this README.md file. This is just
> to illustrate how to get the Yaml contents)

Add the following roles to the `ambassador-crd-clusterrole-change.yaml` you output
in the previous step.

``` 
  # Custom settings Start here...
  # Reference: https://stackoverflow.com/questions/60848031/knative-ambassador-ingress
  - apiGroups:
      - networking.internal.knative.dev
    resources:
      - clusteringresses
      - ingresses
    verbs:
      - get
      - list
      - watch
  - apiGroups:
      - networking.internal.knative.dev
    resources:
      - "ingresses/status"
      - "clusteringresses/status"
    verbs:
      - update
```
### Update the ClusterRole for Ambassador
``` 
kubectl apply -f ambassador-crd-clusterrole-change.yaml
```

## Install the Knative CLI
Follow the instructions below to setup the CLI for Knative:

[https://knative.dev/docs/install/install-kn/](https://knative.dev/docs/install/install-kn/)

## Deploy a hello world app for testing
### Setup an application namespace with LinkerD injection
```
kubectl create namespace kn-apps 
kubectl label namespace kn-apps linkerd.io/inject=enabled
```

### Run a Hello World app using `kn`
```
kn service create helloworld-go --namespace kn-apps --image gcr.io/knative-samples/helloworld-go --env TARGET="Go Sample v1"
```

### Add the Application namespace to LinkerD 
To add this application to the service mesh, run the following commands:

``` 
kubectl annotate ns kn-apps linkerd.io/inject=enabled
kubectl rollout restart deploy -n kn-apps
```

### Set up a Host file entry
The example service created in the last step will setup an ingress route to the following domain:
`helloworld-go.kn-apps.example.com`

To reach this URL you'll have to first find the externally facing IP address of the Ambassador Ingress controller, then create
a host entry on your local computer.

- Find the Ambassador Load balancer IP. Look for the `EXTERNAL-IP` using
the following command:

```
kubectl get service -o wide --namespace ambassador
```

- Using this IP address create an entry in your hostfile on your local machine. 
  (We'll assume the IP is 192.168.6.156 for this example)
  
```
# Ambassador / Knative ingress domains
192.168.6.156	helloworld-go.kn-apps.example.com
```

You should now be able to access your service from a browser using:
>(NOTE: The first request will be slow, since the container has to startup for the first request)

[http://helloworld-go.kn-apps.example.com](http://helloworld-go.kn-apps.example.com)

## Using a private Docker Registry
KNative is VERY,VERY picky about networking.  

### Setup Docker Registry with TLS and basic auth
To use a private docker registry you will first have to make sure it's setup using TLS and basic auth.
See the document in this Git repository under: `/kubernetes/intel-nuc-cluster/docker-registry/secure-registry-with-tls/README.md`

### Setup Static DNS entries
KNative will NOT be able to read the /etc/hosts file from each k8s node to get
the DNS entry of your Private Registry.  If you are not using a DNS server linked via
/etc/resolve.conf, then you will have to manually add the DNS entries into Core DNS.
Look at the section titled `Adding static DNS entries to k8s` in this document within this
Git repo: `/kubernetes/intel-nuc-cluster/commands.md`

### Turn off TLS checks in Knative Serving
Assuming your TLS cert for your private registry is self-signed, you will have to
disable verification.  Update the `config-deployment` configmap in the `knative-serving`
namespace.

``` 
kubectl -n knative-serving edit configmap config-deployment
```

In the `Data` section add the following entry:
>(NOTE: Do NOT put this under the '_Example' section under 'data'. This
> should live just under 'data')

```yaml
data:
  registriesSkippingTagResolving: docker.jaroof.local
```

## Change the default ingress domain name
By default Knative uses `example.com` as the base domain name. To update this, change the 
domain value in the configmap. 
>(NOTE: Using the k9s tool makes this really easy!)

```
kubectl edit cm config-domain --namespace knative-serving
```

## Dealing with Cold starts
Serverless applications will scale down to zero by default. This means that the container will
be destroyed to free up cluster resources.  This could cause a delay in the first request if the
service has scaled down to zero. For example, a service that takes 5ms when warm/hot could take 7s! during a
cold start. This would not be good for SLA's and create performance issues for apps with bursty traffic
patterns.

To deal with the cold start issues, there are two properties in the `config-autoscaler` configmap
that can be modified: 

``` 
# Scale to zero feature flag.
enable-scale-to-zero: "true"
                       
scale-to-zero-pod-retention-period: "0s"                                       
```

To update these attributes, run the following command to modify the configmap:

```
kubectl edit cm config-autoscaler --namespace knative-serving
```

## Knative teardown
To remove the Knative components from k8s run the following commands:

``` 
kubectl api-resources -o name | grep knative | xargs kubectl delete crd

kubectl -n knative-eventing delete po,svc,daemonsets,replicasets,deployments,rc,secrets --all
kubectl -n knative-serving delete po,svc,daemonsets,replicasets,deployments,rc,secrets --all

kubectl delete ns knative-serving
kubectl delete ns knative-eventing
```

### Authentication / Authorization using Ambassador
Authentication / Authorization is not setup using Knative or LinkerD. Instead, set this up
using Ambassador.  Follow the instructions in this git repo at:
`/kubernetes/intel-nuc-cluster/ingress/ambassador/keycloak-integration/README.md`
