# Install Knative for serverless applications
>(NOTE: These instructions are derived from: [https://knative.dev/docs/install/knative-with-operators/](https://knative.dev/docs/install/knative-with-operators/))

## (Dependency) Setup Istio
Istio is a dependency for Knative. In my opinion, it's better to install it separately, using the 
'default' profile, via the Operator.  Follow the instructions in this Git repo to install Istio:

`/kubernetes/intel-nuc-cluster/istio/README.md`

## Install the operator
``` 
kubectl apply -f https://github.com/knative/operator/releases/download/v0.20.0/operator.yaml
```

### Validate that the Operator is running:
```
kubectl get deployment knative-operator
```

## Install the serving sub-system

```
kubectl apply -f - <<EOF
apiVersion: v1
kind: Namespace
metadata:
 name: knative-serving
---
apiVersion: operator.knative.dev/v1alpha1
kind: KnativeServing
metadata:
  name: knative-serving
  namespace: knative-serving
EOF
```

### Verify the serving deployment was successful
``` 
kubectl get deployment -n knative-serving
kubectl get KnativeServing knative-serving -n knative-serving
```

### Setup Istio injection for the Knative Serving Namespace
This command will add a label to the `knative-serving` namespace. By adding this label, Istio
will automatically wire up the pods using Sidecar injection

``` 
kubectl label namespace knative-serving istio-injection=enabled
```

### Allow mutual TLS for Knative
This manifest will setup mutual TLS and peer authentication for Knative using Istio.

``` 
cat <<EOF | kubectl apply -f -
apiVersion: "security.istio.io/v1beta1"
kind: "PeerAuthentication"
metadata:
  name: "default"
  namespace: "knative-serving"
spec:
  mtls:
    mode: PERMISSIVE
EOF
```

## Install Eventing

``` 
kubectl apply -f - <<EOF
apiVersion: v1
kind: Namespace
metadata:
 name: knative-eventing
---
apiVersion: operator.knative.dev/v1alpha1
kind: KnativeEventing
metadata:
  name: knative-eventing
  namespace: knative-eventing
EOF
```

### Verify eventing deployment was successful
``` 
kubectl get deployment -n knative-eventing
kubectl get KnativeEventing knative-eventing -n knative-eventing
```

## Install the Knative CLI
Follow the instructions below to setup the CLI for Knative:

[https://knative.dev/docs/install/install-kn/](https://knative.dev/docs/install/install-kn/)

## Deploy a hello world app for testing
### Setup an application namespace with Istio injection
```
kubectl create namespace kn-apps 
kubectl label namespace kn-apps istio-injection=enabled
```

### Run a Hello World app using `kn`
```
kn service create helloworld-go --namespace kn-apps --image gcr.io/knative-samples/helloworld-go --env TARGET="Go Sample v1"
```

### Set up a Host file entry
The example service created in the last step will setup an ingress route to the following domain:
`helloworld-go.kn-apps.example.com`

To reach this URL you'll have to first find the externally facing IP address of the Istio Ingress controller, then create
a host entry on your local computer.

- Find the Istio Load balancer IP. Look for the `EXTERNAL-IP` for the `istio-ingressgateway` using
the following command:

```
kubectl get service  -o wide --namespace istio-system
```

- Using this IP address create an entry in your hostfile on your local machine. 
  (We'll assume the IP is 192.168.6.156 for this example)
  
```
# Istio / Knative ingress domains
192.168.6.156	helloworld-go.kn-apps.example.com
```

You should now be able to access your service from a browser using:
>(NOTE: The first request will be slow, since the container has to startup for the first request)

[http://helloworld-go.kn-apps.example.com](http://helloworld-go.kn-apps.example.com)

## Change the default ingress domain name
By default Knative uses `example.com` as the base domain name. To update this, change the 
domain value in the configmap. 
>(NOTE: Using the k9s tool makes this really easy!)

```
kubectl edit cm config-domain --namespace knative-serving
```

## Dealing with Cold starts
Serverless applications will scale down to zero by default. This means that the container will
be destroyed to free up cluster resources.  This could cause a delay in the first request if the
service has scaled down to zero. For example, a service that takes 5ms when warm/hot could take 7s! during a
cold start. This would not be good for SLA's and create performance issues for apps with bursty traffic
patterns.

To deal with the cold start issues, there are two properties in the `config-autoscaler` configmap
that can be modified: 

``` 
# Scale to zero feature flag.
enable-scale-to-zero: "true"
                       
scale-to-zero-pod-retention-period: "0s"                                       
```

To update these attributes, run the following command to modify the configmap:

```
kubectl edit cm config-autoscaler --namespace knative-serving
```

## Knative teardown
To remove the Knative components from k8s run the following commands:

``` 
kubectl delete KnativeServing knative-serving -n knative-serving
kubectl delete KnativeEventing knative-eventing -n knative-eventing
kubectl delete namespace knative-serving
kubectl delete namespace knative-eventing
kubectl delete -f https://github.com/knative/operator/releases/download/v0.20.0/operator.yaml
```