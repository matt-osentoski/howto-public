# Kustomize 
Kustomize allows for deployments of Kubernetes resources using Manifests that can be overlaid with
environment specific updates. It's an alternative to Helm.

The official documentation can be viewed here:
[https://kubernetes.io/docs/tasks/manage-kubernetes-objects/kustomization/](https://kubernetes.io/docs/tasks/manage-kubernetes-objects/kustomization/)

## Examples
Examples of Kustomize are in the `examples` folder.

### Overlays
For almost all projects you should probably use overlays. This allows you to have environment specific overrides
in separate directories with a `base` directory that contains the base Kubernetes manifests for the deploy.