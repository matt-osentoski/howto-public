# Example nodeport manifest
This comes up a lot where I manually have to create a nodeport for external 
access when a load balancer or Ingress can't be used or are not required.

## Setup the Nodeport
Modify the `example-nodeport.yaml` file and run the following command:

``` 
kubectl apply -f example-nodeport.yaml --namespace my-namespace
```