# Kubernetes Load balancer / Proxy
This document describes how to setup a load balancer in Kubernetes using Metal LB.

Author: Matt Osentoski  
E-mail: matt.osentoski@gmail.com

## Install the load balancer
Cloud installations will already have a load balancer available.  For bare metal installs like this one, using kubeadm,
you'll have to install your own load balancer.  In this case, we will be install MetalLB: https://metallb.universe.tf/installation/


Run the following command from a machine with kubectl access to the cluster:
```
kubectl apply -f https://raw.githubusercontent.com/metallb/metallb/v0.9.4/manifests/namespace.yaml
```

## Configure the ConfigMap for MetalLB
Use kubectl to apply the metal-lb.yaml file in this directory.
>(NOTE: The addresses section should contain an address space available to your nodes)

```
kubectl apply -f metal-lb.yaml
kubectl apply -f https://raw.githubusercontent.com/metallb/metallb/v0.9.4/manifests/metallb.yaml
# Run the following on the first install only

```

## Use the load balancer with a deployment
To use Metal LB with a deployment, run the following command:
```
kubectl expose deployment <DEPLOYMENT_NAME> --type=LoadBalancer --name=<SERVICE_NAME>

# For example, with a deployment called 'k8s-example-ws' you would call:
kubectl expose deployment k8s-example-ws --type=LoadBalancer --name=k8s-example-ws-service
```

## Assign multiple ports to one IP address of the load balancer
In the configuration above, a single port will be mapped.  If you need to map multiple ports to a single IP address,
use the manifest file in `example-manifests/loadbalancer.yaml` as an example and run the following command:
```
kubectl -n <NAMESPACE_GOES_HERE> apply -f loadbalanacer-yaml
```

## Create a load balancer service with an Istio sidecar
kubectl -n <NAMESPACE_GOES_HERE> apply -f <(istioctl kube-inject -f SOME_MANIFEST_FILE.yaml)