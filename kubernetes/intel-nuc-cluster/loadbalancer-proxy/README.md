# Kubernetes Load balancer / Proxy
This document describes how to setup a load balancer in Kubernetes using Metal LB.

Author: Matt Osentoski  
E-mail: matt.osentoski@gmail.com

## Dependency: Setup strict ARP mode

```shell 
kubectl edit configmap -n kube-system kube-proxy
```

The manifest should look like this, specifically `strictARP: true` and `mode: "ipvs"`:
```yaml
apiVersion: kubeproxy.config.k8s.io/v1alpha1
kind: KubeProxyConfiguration
mode: "ipvs"
ipvs:
  strictARP: true
```

## Install the load balancer
Cloud installations will already have a load balancer available.  For bare metal installs like this one, using kubeadm,
you'll have to install your own load balancer.  In this case, we will be install MetalLB: https://metallb.universe.tf/installation/


Run the following command from a machine with kubectl access to the cluster:
```
kubectl apply -f https://raw.githubusercontent.com/metallb/metallb/v0.13.12/config/manifests/metallb-native.yaml
```

## Configure the IP Address Pool and L2Advertisement for MetalLB
Use kubectl to apply the ip-address-pool.yaml file in this directory.
>(NOTE: The addresses section should contain an address space available to your nodes)
 
>(IMPORTANT!!!: The L2Advertisement is VERY important. If you forget this you won't be able to access the LB IPs from 
> outside the k8s cluster. They will simply timeout. look at the `ip-address-pool.yaml` for an example of this manifest.)

```
# Run the following on the first install only
kubectl apply -f ip-address-pool.yaml
```

## Usage
### Use the load balancer with a deployment
To use Metal LB with a deployment, run the following command:
```
kubectl expose deployment <DEPLOYMENT_NAME> --type=LoadBalancer --name=<SERVICE_NAME>

# For example, with a deployment called 'k8s-example-ws' you would call:
kubectl expose deployment k8s-example-ws --type=LoadBalancer --name=k8s-example-ws-service
```

### Assign multiple ports to one IP address of the load balancer
In the configuration above, a single port will be mapped.  If you need to map multiple ports to a single IP address,
use the manifest file in `example-manifests/loadbalancer.yaml` as an example and run the following command:
```
kubectl -n <NAMESPACE_GOES_HERE> apply -f example-manifests/loadbalanacer-yaml
```

### Create a load balancer service with an Istio sidecar
kubectl -n <NAMESPACE_GOES_HERE> apply -f <(istioctl kube-inject -f SOME_MANIFEST_FILE.yaml)

## Troubleshooting
### Timeouts accessing the Load Balancer IP

First, verify that the `L2advertisement` object is in the `metallb-system` namespace. If it's missing, run the following
command which contains the AddressPool and L2advertisment Objects.

```shell
kubectl apply -f ip-address-pool.yaml
```

### Testing ARP connectivity
Run `arping` from your client workstation to see if Layer 2 is resolving.  If you get timeouts, verify the L2advertisement
configuration in the steps above:

>(IMPORTANT! Do NOT run this command from a node that is running the k8s cluster. The arp command will be ignored)

```shell
sudo arping -I <NETWORK_INTERFACE> <LOAD_BALANCER_IP_ADDRESS>

## For example:
sudo arping -I eth0 192.168.6.150
```

### Validating ARP and IPV4 settings
If all else fails, check the ARP and IPv4 settings on each k8s node. You may have to run the following commands if
the settings are missing:

**IPv4 Settings**

```shell
## First check the settings:
cat /etc/sysctl.d/k8s.conf

## Add the settings if required
cat <<EOF | sudo tee /etc/sysctl.d/k8s.conf
net.bridge.bridge-nf-call-iptables  = 1
net.bridge.bridge-nf-call-ip6tables = 1
net.ipv4.ip_forward                 = 1
EOF

sudo sysctl --system

```

**ARP Settings**
>(NOTE: These settings are recommended by another k8s Load Balancer product called `PureLB`. Reference:
> [https://purelb.gitlab.io/docs/install/](https://purelb.gitlab.io/docs/install/))

```shell
cat <<EOF | sudo tee /etc/sysctl.d/k8s_arp.conf
net.ipv4.conf.all.arp_ignore=1
net.ipv4.conf.all.arp_announce=2

EOF
sudo sysctl --system

```