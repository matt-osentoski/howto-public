
# Install Kubernetes on an Intel Nuc cluster

Author: Matt Osentoski  
E-mail: matt.osentoski@gmail.com

## SSH and Hosts setup
### Setup the Hosts file (All Servers)
On each server update the `/etc/hosts` file with the following values:
>(NOTE: The `pi-router` entry is optional and assumes you're using a Raspberry PI as
> a router for this k8s network)

```
192.168.6.1     pi-router
192.168.6.2     nuc-control-plane
192.168.6.3     nuc-worker1
192.168.6.4     nuc-worker2
192.168.6.5     nuc-worker3
192.168.6.6     nuc-worker4
192.168.6.7     nuc-worker5
```

### Setup SSH (All Servers)
Run the following commands on each node to setup the .ssh directory and keys
```
mkdir ~/.ssh
ssh-keygen -t rsa -P ""
```

### Copy the Master server public key to the worker nodes (MASTER Server ONLY!)
Copy the public SSH key to all the worker nodes.  This will allow you to run
scripts (shutdown all nodes, etc) from master without a password prompt.

>(NOTE: These commands should only be run on the master node!)
```
scp id_rsa.pub  hduser@nuc-worker1:/home/hduser/.ssh/authorized_keys
scp id_rsa.pub  hduser@nuc-worker2:/home/hduser/.ssh/authorized_keys
scp id_rsa.pub  hduser@nuc-worker3:/home/hduser/.ssh/authorized_keys
scp id_rsa.pub  hduser@nuc-worker4:/home/hduser/.ssh/authorized_keys
scp id_rsa.pub  hduser@nuc-worker5:/home/hduser/.ssh/authorized_keys
```

## Install Kubeadm
This steps are derived from:
[https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/install-kubeadm/](https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/install-kubeadm/)

Kubeadm should be installed on all Kubernetes nodes (master and worker).  In my case, I will be using one master node
and four worker nodes running on Intel NUCs, running Ubuntu. See the following documents for installation instructions:

[../../nuc-cluster-setup/ubuntu-image-install.md](../../nuc-cluster-setup/ubuntu-image-install.md)


### Check if there is a swap partition
Run the following command to list out all partitions.  Check if there is a swap partition then disable it
using the command below:
```
sudo lsblk -o NAME,FSTYPE,SIZE,MOUNTPOINT,LABEL
```

### Turn off swap (traditional) (All Servers)
Kubernetes requires that swap be disabled to run correctly
```
sudo swapoff -a
sudo vi /etc/fstab
``` 
Comment out or remove the swap line. Then restart the server using:
```
sudo reboot
```

### Install ContainerD (All Servers)
>(NOTE: This is derived from the following urls:
> - [https://github.com/containerd/containerd/blob/main/docs/getting-started.md](https://github.com/containerd/containerd/blob/main/docs/getting-started.md)
> - [https://docs.docker.com/engine/install/ubuntu/](https://docs.docker.com/engine/install/ubuntu/)

First, setup the APT repository
```shell
# Add Docker's official GPG key:
sudo apt-get update
sudo apt-get install ca-certificates curl gnupg
sudo install -m 0755 -d /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
sudo chmod a+r /etc/apt/keyrings/docker.gpg

# Add the repository to Apt sources:
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
  $(. /etc/os-release && echo "$VERSION_CODENAME") stable" | \
  sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt-get update
```

Next, install containerd and the Docker client
```shell
sudo apt-get install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin
```

(MAYBE NOT NEEDED NOW!!!!!) Using these commands now are giving me errors when initializing k8s
>(IMPORTANT!!!! Do NOT forget this step!)
```shell
sudo rm /etc/containerd/config.toml
sudo containerd config default | sudo tee /etc/containerd/config.toml
sudo vi /etc/containerd/config.toml
```
Edit the following line:
```toml
### From 'false' to 'true'
### Reference: https://kubernetes.io/docs/setup/production-environment/container-runtimes/#containerd-systemd
SystemdCgroup = true
```

>(NOTE: If you get this error after using these commands during kubeadm init run these commands)
```shell
### If you get this error when running kubeadm init:
### `kubeadm [ERROR CRI]: container runtime is not running: output:`
### then run these commands
sudo rm /etc/containerd/config.toml
sudo systemctl restart containerd
```



```shell
# Start the containerd service.
sudo systemctl daemon-reload
sudo systemctl enable --now containerd
```

Validate that ContainerD is running:
```shell 
sudo systemctl status containerd
```
The status output should show `Active: active (running)...`.

### Setup the network bridge settings  (All Servers)
>(IMPORTANT!!!!!! This is critical to all IPv4 to work on newer versions of Ubuntu!!!)
```shell 
cat <<EOF | sudo tee /etc/modules-load.d/k8s.conf
overlay
br_netfilter
EOF

sudo modprobe -a overlay br_netfilter
 
cat <<EOF | sudo tee /etc/sysctl.d/k8s.conf
net.bridge.bridge-nf-call-iptables  = 1
net.bridge.bridge-nf-call-ip6tables = 1
net.ipv4.ip_forward                 = 1
EOF

sudo sysctl --system
```

## Additional notes on networking
>IMPORTANT!  Make sure that the master server is NOT running as a router or NAT device.  Routing/NAT should be handled by
an external router.  Otherwise, you could start to get errors like the following:

`Error: 'dial tcp 10.244.2.2:8443: i/o timeout' Trying to reach: 'https://10.244.2.2:8443/'`

### Install kubeadm, kubelet and kubectl (All Servers)
```shell
sudo apt-get update
# apt-transport-https may be a dummy package; if so, you can skip that package
sudo apt-get install -y apt-transport-https ca-certificates curl gpg
curl -fsSL https://pkgs.k8s.io/core:/stable:/v1.28/deb/Release.key | sudo gpg --dearmor -o /etc/apt/keyrings/kubernetes-apt-keyring.gpg
# This overwrites any existing configuration in /etc/apt/sources.list.d/kubernetes.list
echo 'deb [signed-by=/etc/apt/keyrings/kubernetes-apt-keyring.gpg] https://pkgs.k8s.io/core:/stable:/v1.28/deb/ /' | sudo tee /etc/apt/sources.list.d/kubernetes.list

sudo apt-get update
sudo apt-get install -y kubelet kubeadm kubectl
sudo apt-mark hold kubelet kubeadm kubectl
```

### Restart the Kubelet process (All Servers)
>(NOTE: This step might be optional)
```shell
sudo systemctl daemon-reload
sudo systemctl restart kubelet
```

## Initialize your master server  (MASTER Server ONLY!)
```
sudo kubeadm init --pod-network-cidr=10.244.0.0/16 --apiserver-advertise-address=192.168.6.2
```

>(NOTE: 192.168.6.2 in this case is the network IP of the NUC Ethernet port, where the API will be exposed from the host machine)

Once the command has completed successfully, there should be output with a command you will run from your worker nodes.
It will start with:
`sudo kubeadm join...`
After the next two steps, you will run this command from all worker nodes.

## Copy K8s configs to the local linux user  (MASTER Server ONLY!)
This will allow you to run kubectl as the current unix user instead of using 'sudo'
```
mkdir -p $HOME/.kube
sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config
```

## Add the Pod network (MASTER Server ONLY!)
>(IMPORTANT!!!! You may have to restart the Nodes before this works! You may also have to run the command below a few times too!!)

>(IMPORTANT 2!!!!! It seems like it can take a long time for container images to download. Give the control plane about a half hour
> to download images and maybe try restarting a few times. This may apply to worker nodes as well. Be patient!!!)

In this case we will be using Flannel as the Network Add-on:
https://github.com/coreos/flannel
```
kubectl apply -f https://raw.githubusercontent.com/flannel-io/flannel/master/Documentation/kube-flannel.yml
```

### (OPTIONAL) Troubleshooting
If you waited about a half hour but the control-plane is still unstable it could be the Cgroup setting in containerd. By unstable,
I mean that kubectl commands produce the following error (or similar API access errors) randomly 
`The connection to the server 192.168.6.2:6443 was refused - did you specify the right host or port` 
If that's the case, try this again (ALL SERVERS):

```shell
sudo rm /etc/containerd/config.toml
sudo containerd config default | sudo tee /etc/containerd/config.toml
sudo vi /etc/containerd/config.toml
```
Edit the following line:
```toml
### From 'false' to 'true'
### Reference: https://kubernetes.io/docs/setup/production-environment/container-runtimes/#containerd-systemd
SystemdCgroup = true
```
Then restart containerD and Kubernetes
```shell
sudo systemctl restart containerd
sudo systemctl enable --now containerd
sudo systemctl restart kubelet
```

## Setup the worker nodes.
In the 'Initialize your master server' step above an output for `sudo kubeadm join...` was listed. Run that command from
all your worker nodes.  Here's an example:
```
sudo kubeadm join 192.168.6.2:6443 --token 3pbbpe.wf5367f9sg4bzuom --discovery-token-ca-cert-hash sha256:f7517ba5b2a2dfe2ea6031339d3c9883ae831e092786d031fafd4e6460bbe131
```

>(TROUBLESHOOTING: If you run into issues with this command, try restarting all the nodes a few times.  This seems to be touchy especially
> after installing flannel)

## Verify that the worker nodes are online
From your master node, run:
```
kubectl get nodes
```
You should see all your worker nodes in a 'READY' status

If you run into issues or get the following error message: `The connection to the server 192.168.6.2:6443 was refused - did you specify the  right host or port?
`, Try restarting the kubelet process)

To Restart Kubelet run the following commands:
```shell 
### NOTE: This is only required if you can't view the nodes using 'kubectl get nodes'
sudo systemctl stop kubelet
sudo systemctl start kubelet
```

## Install Metric Server for CPU/Memory usage
Install the Metric server to show the CPU and memory usage when using k9s or the
`kubectl top pod` command
>(NOTE: This assumes you have the Helm 3.x binary installed on your client)

>(NOTE2: Make sure every node has a /etc/hosts entries for each node/ip)
```
kubectl create namespace metrics
helm repo add metrics-server https://kubernetes-sigs.github.io/metrics-server/
helm upgrade --install --namespace metrics --set args={"--kubelet-insecure-tls=true, --kubelet-preferred-address-types=InternalIP\,Hostname\,ExternalIP"} metrics-server metrics-server/metrics-server
```

## (OPTIONAL) Mark nodes as high performance
If your Node hardware isn't consistent, you can mark some nodes as high performing. Run the following commands
updating only the high performance nodes:
```
kubectl label nodes nuc-worker2 nodetype=highperf
kubectl label nodes nuc-worker3 nodetype=highperf
kubectl label nodes nuc-worker4 nodetype=highperf
```

To deploy to a high performance node using Helm (assuming the default helm values template was used), run the
helm install command following 'set' argument:
```
### This example shows a MySQL install
helm upgrade --install --namespace mysql --values values.yaml --set nodeSelector.nodetype=highperf mysql-binlog stable/mysql
```

## Install the UI
### Install and run the UI pod
First, grab the dashboard manifest file:
```
wget https://raw.githubusercontent.com/kubernetes/dashboard/v2.0.0-rc5/aio/deploy/alternative.yaml
```

Next, Update the file adding the following line in the `deployment` args section:
```
spec:
      containers:
      - name: kubernetes-dashboard
        image: k8s.gcr.io/kubernetes-dashboard-amd64:v1.10.1
        ports:
        - containerPort: 8443
          protocol: TCP
        args:
          - --enable-skip-login       ### <---- This line
          - --disable-settings-authorizer    ### <---- and this line too!
```
Finally, update the clusterRoleBinding section
```
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: kubernetes-dashboard
  namespace: kubernetes-dashboard
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: cluster-admin    ### <---- update this line
subjects:
  - kind: ServiceAccount
    name: kubernetes-dashboard
    namespace: kubernetes-dashboard
```

Now, create the kubernetes objects:
```
kubectl -f alternative.yaml
```

To startup the UI server, run the follow:
```
kubectl proxy --address='192.168.6.2' --accept-hosts '.*'
```

From your web browser, go to:

[http://192.168.6.2:8001/api/v1/namespaces/kubernetes-dashboard/services/http:kubernetes-dashboard:/proxy](http://192.168.6.2:8001/api/v1/namespaces/kubernetes-dashboard/services/http:kubernetes-dashboard:/proxy)

## Add a new cluster to your ~/.kube/config file
To have multiple clusters setup in your `~/.kube/config`  file, perform the following steps:

- First, login to the master node of the cluster you want to join, then look at the `~/.kube/config` file
- In my case I have the following sections:
    - cluster: certificate-authority-data
    - contexts: context: cluster
    - contexts: context: user
    - contexts: name
    - Users: user: client-certificate-data
    - Users: user: client-key-data
    
To set this up on your local laptop, you can either copy these sections into your local `~/.kube/config` file,
or run the following commands.  

>(NOTE: There might be naming conflicts.  In this case, you can change the name of the username, clustername, etc.
you just have to keep the certificates and keys)

Before you begin, make a backup of the `~/.kube/config` files.  Run the following commands on your 
local laptop.  The square brackets are areas where you should copy certs, keys, etc. from the `~/.kube/config` file
on the master server over to your local config file:
```
kubectl config --kubeconfig=config set-cluster [CLUSTER NAME GOES HERE] --server https://192.168.6.2:6443
kubectl config --kubeconfig=config set clusters.[CLUSTER NAME GOES HERE].certificate-authority-data [CERTIFICATE GOES HERE]

kubectl config --kubeconfig=config set-credentials [USERNAME GOES HERE]
kubectl config --kubeconfig=config set users.[USERNAME GOES HERE].client-certificate-data [CLIENT CERT GOES HERE]
kubectl config --kubeconfig=config set users.[USERNAME GOES HERE].client-key-data [CLIENT KEY GOES HERE]

kubectl config --kubeconfig=config set-context [CLUSTER NAME GOES HERE] --cluster=[CLUSTER NAME GOES HERE] --user=[USERNAME GOES HERE]
```

>(NOTE: An example batch script for windows using these commands can be found at: [kubernetes/intel-nuc-cluster/sample-scripts/windows/k8s-config-setup.cmd](kubernetes/intel-nuc-cluster/sample-scripts/windows/k8s-config-setup.cmd) )


## Startup / Shutdown the Cluster
For each of the worker nodes, run the following command before shutting down the server:
```
kubectl drain <node name>
```
On server startup, run the following command for each worker node:
```
kubectl uncordon <node name>
```


## Troubleshooting
If you run into an issue, you can always rebuild the master server by running:
```
sudo kubeadm reset
sudo kubeadm init --pod-network-cidr=10.244.0.0/16 --apiserver-advertise-address=192.168.6.2
```
Run the following command to check the routing tables on the master server:
```
sudo iptables -t nat -n -L
```
To remove old Flannel bridge IPs, etc
```
# View IP interfaces
ip link
# Remove interfaces
sudo ip link delete cni0
sudo ip link delete flannel.1
```


