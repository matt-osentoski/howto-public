# Install LinkerD
LinkerD is a lightweight service mesh for Kubernetes.  
>(IMPORTANT! LinkerD does NOT include an ingress controller.  You will have to install Ambassador or something
> similar for (North/South) traffic management.  LinkerD only deals with internal cluster traffic (East/West))

References: 
  * [https://linkerd.io/2/getting-started](https://linkerd.io/2/getting-started)

Author: Matt Osentoski  
E-mail: matt.osentoski@gmail.com

## Dependency (Ambassador)
LinkerD does not include an ingress controller.  Install Ambassador before beginning the LinkerD installation.  See the README
file in this Git repository at: `/kubernetes/intel-nuc-cluster/ingress/ambassador/README.md`

## Install the LinkerD CLI
Follow the instructions at:

[https://linkerd.io/2/getting-started/#step-1-install-the-cli](https://linkerd.io/2/getting-started/#step-1-install-the-cli)

### Verify the CLI is installed
Run the following command:

``` 
linkerd version
```

## Install the LinkerD Server
### Cluster pre-check
Validate that your k8s cluster is ready for the LinkerD installation by running the following command:
``` 
linkerd check --pre

## The ouput should end with:
Status check results are √
```

### Install the server
```
linkerd install | kubectl apply -f -
```

### Verify all the server components are running
```
linkerd check 
# Or
kubectl -n linkerd get deploy

## The ouput should end with:
Status check results are √
```

### View LinkerD stats (Using a tool similar to the Unix Top)
```
linkerd -n linkerd top deploy/linkerd-web
```

## Integrate LinkerD with Ambassador
Run the following commands to integrate Ambassador and LinkerD using Proxy Injection via the
namespace.  
>(NOTE: Ambassador should already be running and installed in the `ambassador` namespace.)

```
## Setup Ambassador request headers with LinkerD
kubectl apply -f - <<EOF
---
apiVersion: getambassador.io/v2
kind: Module
metadata:
  name: ambassador
  namespace: ambassador
spec:
  config:
    add_linkerd_headers: true
EOF

## Next, setup LinkerD Proxy Injection to the 'ambassador' namespace
kubectl annotate ns ambassador linkerd.io/inject=enabled

## Restart all pods once this is done
kubectl rollout restart deploy -n ambassador
```




## LinkerD Proxy Injection
For LinkerD to work, a sidecar is required to run side-by-side other containers in a pod.
An easy way to implement this is to add an annotation to the namespace.

### Setup injection with namespaces
Run this command for each namespace you would like to bind to LinkerD. Don't forget to
restart all pods in the namespace, after setting the annotation in the namespace.
In this example, the application pods are running in a namespace called `my-custom-namespace`

```
kubectl annotate ns my-custom-namespace linkerd.io/inject=enabled
# Restart the pods in the namespace
kubectl rollout restart deploy -n my-custom-namespace

## Or ##

## You can also setup multiple namespaces at once. In this example, I'm
## Setting the 'default' namespace and 'my-custom-namespace' at the same time
kubectl annotate ns default my-custom-namespace linkerd.io/inject=enabled
# Then restart all the pods in both namespaces
kubectl rollout restart deploy -n default
kubectl rollout restart deploy -n my-custom-namespace
```

## Expose the LinkerD / Grafana Dashboard using a Load Balancer
To expose the dashboard using a Load Balancer, run the following command:
``` 
## Add the Load Balancer
kubectl expose deployment linkerd-web --namespace linkerd --type=LoadBalancer --name=linkerd-web-lb

## Find the External IP
kubectl get service linkerd-web-lb -o wide  --namespace linkerd
```

### Update the Linkerd-web deployment for inbound access
Once the Load Balancer is setup, you'll have to edit the `deployment/linkerd-web` manifest
Add the IP address of the Load Balancer exposing the LinkerD Web Portal in the following section:
`spec/template/spec/containers/o/args/enforced-host`

>(NOTE: Don't forget to add the port number.  Assuming an IP address of `192.168.6.157` my entry looked like this:
> ` - -enforced-host=^(localhost|192\.168\.6\.157:8084|127\.0\.0\.1|linkerd-web\.linkerd\.svc\.cluster\.local|linkerd-web\.linkerd\.svc|\[::1\])(:\d+)?$`)

### Access the LinkerD-web portal
Assuming the IP address was `192.168.6.157` from the Load Balancer, you can access the portal
at the following URL:

http://192.168.6.157:8084/

### Authentication / Authorization using Ambassador
Authentication / Authorization is not setup using LinkerD. Instead, set this up
using Ambassador.  Follow the instructions in this git repo at:
`/kubernetes/intel-nuc-cluster/ingress/ambassador/keycloak-integration/README.md`

## Uninstall LinkerD
To uninstall LinkerD, use the CLI and run the following command:

```shell 
linkerd uninstall | kubectl delete -f -
```
