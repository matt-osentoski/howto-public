# Istio installation and setup for Kubernetes
Istio is a cloud service mesh for Kubernetes.  It allows tracing, authentication, authorization, and many more cross cutting
concerns for your Kubernetes cluster.

References: 
  * [https://istio.io/docs/setup/kubernetes/getting-started/](https://istio.io/docs/setup/kubernetes/getting-started/)
  * [https://istio.io/latest/docs/setup/getting-started/#download](https://istio.io/latest/docs/setup/getting-started/#download)
  * [https://istio.io/latest/docs/setup/install/istioctl/](https://istio.io/latest/docs/setup/install/istioctl/)
  * [https://istio.io/latest/docs/setup/install/helm/](https://istio.io/latest/docs/setup/install/helm/)

Author: Matt Osentoski  
E-mail: matt.osentoski@gmail.com

## Install Istio CLI 
Even though we will be using Helm for the Istio install, let's install the CLI first, just to have it.

```
cd ~
curl -L https://istio.io/downloadIstio | sh -
```

## Add the Istio CLI to your path
Add the following settings to `/etc/bash.bashrc` (for all users)

-or-

`~.profile` (for the current user)

```
ISTIO_HOME=/home/hduser/istio-X.X.X
export ISTIO_HOME
export PATH="$PATH:$ISTIO_HOME/bin"
```

## Add the export above into your environment
To update the $PATH from the previous step, run the following command, otherwise you can log out and back into the terminal
```
source /etc/bash.bashrc

# -or-

source ~/.profile
```

## Create an Istio System namespace
```
kubectl create namespace istio-system
```

## Install Istio using the Helm

### Installation
```shell
helm repo add istio https://istio-release.storage.googleapis.com/charts
helm repo update

helm install istio-base istio/base -n istio-system
```

### Verify that Istio base is installed
```shell
helm ls -n istio-system
```

### Install the Istio Discovery Chart
```shell

## Default
helm install istiod istio/istiod -n istio-system --wait

## If using a Custom CA Cert for communicating with Keycloak or other IAM system 
#### IMPORANT!! Make sure to update the `pilot.jwksResolverExtraRootCA` value in the values file with 
#### appropriate CA cert, if using this method.
helm install istiod istio/istiod -n istio-system --values istiod-values.yaml --wait
```

### Validate the Discovery Installation
```shell
helm ls -n istio-system

### Check the status (The NOTES should say: '"istiod" successfully installed!')
helm status istiod -n istio-system

### Check that the service is running
kubectl get deployments -n istio-system --output wide
```

### (OPTIONAL) Install the ingress
```shell
kubectl create namespace istio-ingress

helm install istio-ingress istio/gateway -n istio-ingress --wait
```

To learn more about setting up Ingress gateways and routes, read the following document: [ingress-egress/README.md](ingress-egress/README.md)

### (OPTIONAL) Install the Egress
```shell
istioctl install  \
  --set components.egressGateways[0].name=istio-egressgateway \
  --set components.egressGateways[0].enabled=true
```

## Install the Istio Dashboard
### Install Grafana with the Istio plugin
```shell
kubectl apply -f https://raw.githubusercontent.com/istio/istio/release-1.17/samples/addons/grafana.yaml 
```

### Install Prometheus for Istio
```shell
 kubectl apply -f https://raw.githubusercontent.com/istio/istio/release-1.17/samples/addons/prometheus.yaml
```

### Install Kiali for Observability
```shell
kubectl apply -f https://raw.githubusercontent.com/istio/istio/release-1.17/samples/addons/kiali.yaml
```

### Expose the Kiali dashboard using a Load Balancer
```
kubectl -n istio-system expose deployment kiali --type=LoadBalancer --name=kiali-lb-service
```

Use the following command to find the externally facing IP address:
``` 
kubectl -n istio-system get services
```

### Access the Dashboard.
Assuming the IP address is '192.168.6.157', you can access the dashboard using the following URL:

[http://192.168.6.157:20001/kiali](http://192.168.6.157:20001/kiali)


# Installing the Istio sidecar to pods
For Istio to work, a sidecar is required for each pod.  There are a number of ways to manually or automatically add the sidecars.
(Reference: https://istio.io/docs/setup/kubernetes/sidecar-injection/#manual-sidecar-injection)

## Add Istio label to your application namespace
Adding a label to your namespace will allow Istio to automatically bind to pods in the namespace.
```
kubectl label namespace default istio-injection=enabled
```
Verify that the namespace is `enabled` for Istio injection
```
kubectl get namespace -L istio-injection
```
Next, restart or redeploy all pods in that namespace to bind the sidecars

# Networking considerations if using Keycloak (or Similar)
If you will be using Keycloak for authentication and it's using a custom DNS that is manually
created, you will have to add a DNS entry for the server in k8s. This can be done by updating the
configmap of your CoreDNS subsystem in the `default` namespace.

To do this you can reference the `Adding static DNS entries to k8s` section in the `commands.md` file
located here: [../../commands.md](../../COMMON_COMMANDS.md)

## Self-Signed TLS when using Keycloak (or Similar)
In addition to the DNS updates, you will also need a valid CA cert when communicating to Keycloak for authentication
using Istio.  If you missed this step during the initial installation of Istio, you can always add the additional
CA cert by running this command:

```shell
### NOTE: Make sure to add your CA cert into the `istiod-values.yaml` file first.

helm upgrade --namespace istio-system --values istiod-values.yaml istiod istio/istiod
```

# Other
## Outbound network access
Istio will block outbound access to external (outside the k8s cluster) resources like databases, message queues, etc.
To get gain access to these resources, you will have to create a serviceEntry manifest.

For an example that accesses an external kafka server, look at the manifest under:
`examples/csv-batch-loader-serviceEntry.yaml` from this directory.

## Delete a service entry
To delete a service entry that was created in the last step, run the following command:
```
kubectl delete serviceentry <SERVICE_ENTRY_NAME> -n default
```

# Teardown
To remove the Istio install, run the following commands:
```
helm ls -n istio-system
helm delete istio-ingress -n istio-ingress
kubectl delete namespace istio-ingress
helm delete istiod -n istio-system
kubectl delete namespace istio-system
```

## Removing resources that might be stuck
Sometimes lingering k8s resources can get stuck.  Run the following commands to clean these up:

```
istioctl manifest generate | kubectl delete -f -
kubectl delete ns istio-system --grace-period=0 --force 

or try this:
kubectl patch crd/istiooperators.install.istio.io -p '{"metadata":{"finalizers":[]}}' --type=merge
Then:
kubectl delete istiooperators.install.istio.io -n istio-system onprem-istiocontrolplane
```