# Setup an OIDC / Oauth2 server with Istio integration
This document describes how to integrate an OIDC/Oauth2 server with Istio for authentication (authn) and authorization (authz)
For this example, we will be using the Keycloak server for OIDC/Oauth2.

## Install the OIDC / Oauth2 server
Follow the instructions in the `keycloak/README.md` off this directory for Keycloak installation instructions.
References:
https://www.keycloak.org/
https://github.com/helm/charts/tree/master/stable/keycloak

