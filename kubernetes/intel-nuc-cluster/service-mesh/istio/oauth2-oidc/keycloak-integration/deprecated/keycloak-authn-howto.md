# Configure the KeyCloak server for Authentication using Istio
This document is based on the following URLs:
- https://www.keycloak.org/docs/latest/getting_started/index.html
- https://istio.io/help/ops/security/end-user-auth/

The KeyCloak server should already be running. If not, look at the 
`kubernetes/intel-nuc-cluster/example-applications-in-k8s/keycloak/README.md` file for instrucions.

Author: Matt Osentoski  
E-mail: matt.osentoski@gmail.com

## Login to the admin console
http://192.168.6.154:8080
username: keycloak
password: password

## Create a Realm
(From Stackoverflow)
A security realm is a mechanism used for protecting Web application resources. It gives you the ability to protect a
resource with a defined security constraint and then define the user roles that can access the protected resource.

1. From the Master drop-down menu on the upper left-hand side, click `Add Realm`.
2. Type `example` in the Name field and click Create.

# Users and Roles
## Create a user
1. From the left-hand menu, select `Users`
2. Click the 'Add user' button on the right-hand side
3. Enter a 'Username' and click 'Save'
4. Click the 'Credentials' tab and enter a password
5. Slide the 'Temporary' button to 'off'
6. Press the 'Enter' button.

>(NOTE: This is important, there is no save button on this tab you have to hit enter to save the changes)

## Verify the User can login
Go to the following URL:
>(NOTE: Your IP address may be different)

http://192.168.6.154:8080/auth/realms/fhir/account

Login with the user you created in the last step.

## Create a role
1. From the left-hand menu, select `Roles`
2. Click the 'Add Role' button on the right-hand side
3. Enter a role name and description
4. Click 'Save'

## Add a user to a role
1. From the left-hand menu, select `Users`
2. Click 'View All Users' and select the user you created in the section above and click 'Edit'
3. Click the 'Role Mappings' tab
4. Select the role you want to add then click 'Add Selected'

# Clients
## Register a client application
1. From the left-hand menu, select `Clients`
2. Click the 'Create' button on the right-hand side
3. Enter an identifier the application under 'Client Id'  (ex: hapi-fhir)
4. For 'Client Protocol', select 'openid-connect'
5. In the 'Root URL' field enter the base path of your application. (ex: http://192.168.6.151:8080/fhir/)
6. Click 'Save'

# Retrieve the JWT Token
## Using curl
Run the following command to retrieve your JWT `id_token`:
>(NOTE: The 'scope' and 'response_type' parameters are required to retrieve the 'id_token')

```
curl -X POST http://192.168.6.154:8080/auth/realms/fhir/protocol/openid-connect/token \
    -d grant_type=password \
    -d client_id=hapi-fhir \
    -d username=someRealmUser \
    -d password=password \
    -d scope=openid \
    -d response_type=id_token
```
You can also pipe the output into a tool called `jq` to make reading the JSON easier. For example:
```
curl -X POST http://192.168.6.154:8080/auth/realms/fhir/protocol/openid-connect/token \
    -d grant_type=password \
    -d client_id=hapi-fhir \
    -d username=readuser \
    -d password=password \
    -d scope=openid \
    -d response_type=id_token | jq '.'
```

# Apply Authentication and Verify
## Apply an authentication policy to a Kubernetes Service
To secure a Kubernetes service, you'll have to apply a policy. Take a look at the `authn-policy.yaml` file, make changes
for your application, and run the following command:
```
kubectl -n default apply -f authn-policy.yaml
```

## Test that authentication works
To verify that authentication is working, first, make a call to the Kubernetes service without a bearer token:
>(NOTE: The URL below is for my Kubernetes service called `hapi-fhir`, referenced as the 'target' in the `authn-policy.yaml` file)

```
curl -X GET http://192.168.6.150:8080/fhir/
```
You should receive the following error: `Origin authentication failed`

Next, add a HTTP header with the bearer token (access_token) from the step above:
```
curl -X GET http://192.168.6.150:8080/fhir/ \
    -H "Authorization: Bearer eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICIxUXFYRzlZWFJMQlhCYk5kZHBjS09pWklrYkJlZVVzWktkc2RiaXUzYV8wIn0.eyJqdGkiOiIwOTk1MmZiOC04ZTRjLTQ1YTYtOTRiNC0yZjlkMDA3NzYzYjAiLCJleHAiOjE1NDA1NzI4MDAsIm5iZiI6MCwiaWF0IjoxNTQwNTcyNTAwLCJpc3MiOiJodHRwOi8vMTkyLjE2OC42LjE1NDo4MDgwL2F1dGgvcmVhbG1zL2ZoaXIiLCJhdWQiOiJoYXBpLWZoaXIiLCJzdWIiOiJhOGJhOWNmYi05NzA2LTRkNjYtYTFkMy02MWZhMWM1NmU3YWQiLCJ0eXAiOiJJRCIsImF6cCI6ImhhcGktZmhpciIsImF1dGhfdGltZSI6MCwic2Vzc2lvbl9zdGF0ZSI6ImMyNDQ3MjZiLWE4MzgtNGZhYi05ZDBhLTgyM2YyNDg4NTJiMSIsImFjciI6IjEiLCJlbWFpbF92ZXJpZmllZCI6ZmFsc2UsInByZWZlcnJlZF91c2VybmFtZSI6InJlYWR1c2VyIn0.keiykucSD71P-CSlL16eMA8xadJpmHgY3jk9yA9P442_o-e1u-Z5MWLt9KJz-5VedQcXFCwYk9ACMgX9vNPhM2P5lSGjB9DI763rt266Gvs_9UapbmADPIxgpoIfP0P1paS1vggNHXpcGB12uQJTwpxvxRiKhLTt0ySfngOgZRLcVpMNcmchW-7i-CYGdU2MQff8W14Fxl_iBJtYOkcmhAVG6izElhAc35AfBXBsG5ge1cAfSknDvJVWumLl0TTJjZiyrfzbyywZUYBGpwGfylPPuQfXSk3rHMJ08N7FFtseobeexOS8dvsjZP1X-YFLELHyvdpGGDHhCEnjYNjsXQ"

```
You should now see the contents of the URL

## Getting a token with client secrets
If your client in keycloak has an `Access Type` set to 'confidential', you will be required to send a 'Client Secret'
when requesting your token.  This can be done by adding a `client_secret` header to your token request.  To get the value,
perform these steps:
- Login to the KeyCloak admin console
- Click 'Clients' on the left-hand menu
- In the Settings tab, make sure 'Access Type' is set to 'confidential'
- In the Credentials tab, set 'Client Authenticator' to 'Client Id and Secret'
- Copy the UUID in the 'Secret' field

This UUID will then be used to make a POST for the token.  For example:
```
curl -X POST http://192.168.6.154:8080/auth/realms/fhir/protocol/openid-connect/token \
    -d grant_type=password \
    -d client_id=hapi-fhir \
    -d username=readuser \
    -d password=password \
    -d scope=openid \
    -d client_secret=bea9015c-a262-4ba2-9e13-1330b4770479 \
    -d response_type=id_token | jq '.'
```
In this example, we're adding a 'client_secret' header and the UUID 'bea9015c-a262-4ba2-9e13-1330b4770479' which we copied
from the Keycloak admin console tab.

## Getting a token using Postman
>(NOTE: This section is derived from: https://stackoverflow.com/questions/45841479/keycloak-jax-rs-and-postman-authorization-auth-url)

### Obtain URLs using KeyCloak's well known url
First, you'll need the `authorization_endpoint` and `token_endpoint` by using the 'well known' URL from Keycloak below:

http://localhost:8080/auth/realms/mesh/.well-known/openid-configuration

### Obtain a token using Postman
- Open Postman and put the URL of the service you want to reach in the address bar.  (This assumes the service is already protected using Keycloak.)
- Click the `Authorization` tab
- Click `Get new Access token`
- In the `Callback URL` field, enter the URL of the service you're trying to hit
- In the `Auth URL` field, enter the 'authorization_endpoint' you obtained from the well known URL above.
- In the `Access Token URL` field enter the `token_enpoint` you obtained from the well known URL above.
- For the `Client ID` field, enter the client name you setup in KeyCloak for this Realm.
- For the `Client Secret` field, enter the secret that was setup for the client in KeyCloak. (NOTE: this requires that the client 'access' type is set as 'confidential'
- Click `Request Token`
- At this point you should be redirected to a Keycloak login page.  Login with the appropriate username/password
- You should now see that the token is listed in Postman.  Scroll down and click `Use Token`

Now in the `Header` tab of Postman, you should see an `authorization` section with the Bearer token.  You can now try the
Service request, which should return a 200 HTTP code.

