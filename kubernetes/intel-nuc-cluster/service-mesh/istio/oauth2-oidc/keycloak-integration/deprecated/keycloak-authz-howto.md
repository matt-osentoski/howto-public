# Configure the KeyCloak server for Authorization using Istio
This document assumes you've already setup KeyCloak for Authentication, using the 'keycloak-authn-howto.md' file as a
guide. This document is based on the following URL:
- https://istio.io/blog/2018/istio-authorization/

Author: Matt Osentoski  
E-mail: matt.osentoski@gmail.com

## Turn on RBAC to allow authorization in the Kubernetes cluster, against a namespace
```
kubectl -n <NAMESPACE> apply -f rbac-config-ON.yaml
```

## Users and Roles
You should already have at least two users and two roles setup in KeyCloak.  If not, refer to the `keycloak-authn-howto.md`
document for instructions.  For this guide we will assume there are the following two users and roles:
- **readuser** - who is a member of role `fhir-read`
- **writeuser** - who is a member of role `fhir-write`

## Setup Scope role mappings
In the KeyCloak admin console, you will have to allow the roles you created above to be available for the client app.

1. On the Left-hand menu, click 'Clients'
2. Click the 'Scope' tab
3. Set "Full Scope Allowed" to 'Off'
4. Click each role in the 'Available Roles' box and then click 'Add Selected'

Your roles should now be available for this client.


## Setup a service roles and bindings in Istio
Service roles and bindings allow you to create authorization rules that limit URL access based on JWT token claims/scopes.
First, setup the Service Role:
```
kubectl -n default apply -f authz-service-role.yaml
```
Next, bind the service role using a service role binding:
```
kubectl -n default apply -f authz-service-role-binding.yaml
```
>(NOTE: The YAML files are located in the same directory as this document)

## Test that authorization works
To verify that authorization is working, first, obtain a bearer token:
```
curl -X POST http://192.168.6.154:8080/auth/realms/fhir/protocol/openid-connect/token \
    -d grant_type=password \
    -d client_id=hapi-fhir \
    -d username=readuser \
    -d password=password \
    -d scope=openid \
    -d client_secret=bea9015c-a262-4ba2-9e13-1330b4770479 \
    -d response_type=id_token | jq '.'
```

Next, make a call to your service with different users to verify that authorization is working correctly.

## Turn off Authorization
To turn off authorization run the following command against the rbac-config-ON.yaml file:
```
kubectl -n <NAMESPACE> delete -f rbac-config-ON.yaml
```

