# (DEPRECATED) Keycloak integration with Istio
This directory contains file and instructions for integrating KeyCloak with Istio for Oauth2
security (authn/authz)

## (Dependency) Install Keycloak
Follow the instructions in this Git repository to install Keycloak:
`kubernetes/intel-nuc-cluster/example-applications-in-k8s/keycloak/README.md`

## Setup Authentication (Authn)
Follow the instructions in the following file in this directory: `keycloak-authn-howto.md`

## Setup Authorization (Authz)
Follow the instructions in the following file in this directory: `keycloak-authz-howto.md`
