# Istio installation and setup for Kubernetes
Istio is a cloud service mesh for Kubernetes.  It allows tracing, authentication, authorization, and many more cross cutting
concerns for your Kubernetes cluster.

References: 
  * [https://istio.io/docs/setup/kubernetes/getting-started/](https://istio.io/docs/setup/kubernetes/getting-started/)
  * [https://istio.io/latest/docs/setup/getting-started/#download](https://istio.io/latest/docs/setup/getting-started/#download)
  * [https://istio.io/latest/docs/setup/install/operator/](https://istio.io/latest/docs/setup/install/operator/)

>(NOTE: You should probably install Metal LB before performing this step on a Nuc Cluster.  See the `locadbalancer-proxy`
folder one level up.)

Author: Matt Osentoski  
E-mail: matt.osentoski@gmail.com

## Dependencies
This installation uses Helm to deploy Istio.  

## Install Istio onto your master Kubernetes server
```
cd ~
curl -L https://istio.io/downloadIstio | sh -
```

## Add Istio to your path
Add the following settings to `/etc/bash.bashrc`
```
ISTIO_HOME=/home/hduser/istio-1.8.2
export ISTIO_HOME
export PATH="$PATH:$ISTIO_HOME/bin"
```

## Add the export above into your environment
To update the $PATH from the previous step, run the following command, otherwise you can log out and back into the terminal
```
source /etc/bash.bashrc
```

## Create an Istio System namespace
```
kubectl create namespace istio-system
```

## Install Istio using the Operator
### Install the Operator
```
istioctl operator init
```

### Setup namespaces to be watched by Istio
To inject sidecars into pods of a particular namespace, run the following command:

``` 
istioctl operator init --watchedNamespaces=my-namespace1,my-namespace2
```

### Install Istio using the `default` profile:
``` 
kubectl create ns istio-system

kubectl apply -f - <<EOF
apiVersion: install.istio.io/v1alpha1
kind: IstioOperator
metadata:
  namespace: istio-system
  name: onprem-istiocontrolplane
spec:
  profile: default
EOF
```

### Add the Egress gateway
Run the following command to install the Egress gateway

``` 
kubectl apply -f - <<EOF
apiVersion: install.istio.io/v1alpha1
kind: IstioOperator
metadata:
  namespace: istio-system
  name: example-istiocontrolplane
spec:
  profile: default
  components:
    pilot:
      k8s:
        resources:
          requests:
            memory: 3072Mi
    egressGateways:
    - name: istio-egressgateway
      enabled: true
EOF
```

## Verify the installation
You should see about a dozen of services/pods running for Istio with the following command:
```
kubectl get svc -n istio-system
kubectl get pods -n istio-system
```

## Install the Istio Dashboard
### Install Kiali
``` 
kubectl apply -f $ISTIO_HOME/samples/addons
kubectl rollout status deployment/kiali -n istio-system
```

### Expose the Kiali dashboard using a Load Balancer
```
kubectl -n istio-system expose deployment kiali --type=LoadBalancer --name=kiali-lb-service
```

Use the following command to find the externally facing IP address:
``` 
kubectl -n istio-system get services
```

### Access the Dashboard.
Assuming the IP address is '192.168.6.157', you can access the dashboard using the following URL:

[http://192.168.6.157:20001/kiali](http://192.168.6.157:20001/kiali)


# Installing the Istio sidecar to pods
For Istio to work, a sidecar is required for each pod.  There are a number of ways to manually or automatically add the sidecars.
(Reference: https://istio.io/docs/setup/kubernetes/sidecar-injection/#manual-sidecar-injection)

## Add Istio label to your application namespace
Adding a label to your namespace will allow Istio to automatically bind to pods in the namespace.
```
kubectl label namespace default istio-injection=enabled
```
Verify that the namespace is `enabled` for Istio injection
```
kubectl get namespace -L istio-injection
```
Next, restart or redeploy all pods in that namespace to bind the sidecars


# Other
## Outbound network access
Istio will block outbound access to external (outside the k8s cluster) resources like databases, message queues, etc.
To get gain access to these resources, you will have to create a serviceEntry manifest.

For an example that accesses an external kafka server, look at the manifest under:
`examples/csv-batch-loader-serviceEntry.yaml` from this directory.

## Delete a service entry
To delete a service entry that was created in the last step, run the following command:
```
kubectl delete serviceentry <SERVICE_ENTRY_NAME> -n default
```

# Teardown
To remove the Istio install, run the following commands:
```
kubectl delete istiooperators.install.istio.io -n istio-system onprem-istiocontrolplane
istioctl operator remove
kubectl delete namespace istio-system
kubectl delete namespace istio-operator
```

## Removing resources that might be stuck
Sometimes lingering k8s resources can get stuck.  Run the following commands to clean these up:

```
istioctl manifest generate | kubectl delete -f -
kubectl delete ns istio-system --grace-period=0 --force 

or try this:
kubectl patch crd/istiooperators.install.istio.io -p '{"metadata":{"finalizers":[]}}' --type=merge
Then:
kubectl delete istiooperators.install.istio.io -n istio-system onprem-istiocontrolplane
```