# Setting up Ingress and Egress Gateways in Istio

## Install the Kubernetes Gateway CRDs

```shell
kubectl get crd gateways.gateway.networking.k8s.io &> /dev/null || \
  { kubectl kustomize "github.com/kubernetes-sigs/gateway-api/config/crd?ref=v0.6.2&timeout=90s" | kubectl apply -f -; }

```

## Create an Ingress
>(IMPORTANT!!!! There are two different k8s APIs you can use to create a
> Gateway and the service/routing that maps to the workload:
> - The Istio API:  This will have the namespace of: `networking.istio.io/v1alpha3`
> - The K8s Gateway API: This will have a namespace of `gateway.networking.k8s.io`
>
> This can be a little confusing when looking up older documentation. You'll see
> references to `VirtualService` and `HttpRoute`. These are essentially the same thing
> except `VirtualService` uses the older Istio API and `HttpRoute` uses the newer k8s Gateway API
>
> **You should prefer the k8s Gateway API** as this is more generic across Service mesh platforms
> and is the pathway forward for Istio!
>
> In these examples, I will try to stick to the k8s Gateway API exclusively.)


### Create the Gateway

```shell
kubectl apply -f gateway-example.yaml
```

###  Create a Route

```shell
kubectl apply -f route-example.yaml
```
