# Mutual TLS (mTLS) within Istio
This document describes how to setup mTLS between Istio managed services

>(NOTE: By Default, Istio's mTLS is set to `Permissive` this allows either TLS or plain text to be transmitted between
> applications that are managed by Istio)

>(REFERENCE: [https://istio.io/latest/docs/tasks/security/authentication/mtls-migration/](https://istio.io/latest/docs/tasks/security/authentication/mtls-migration/))

## Force mTLS between Istio Workloads
To change the mTLS setting between works from `Permissive` to `Strict` run the following command:

```shell
kubectl apply --namespace example -f peer-authentication.yaml
```