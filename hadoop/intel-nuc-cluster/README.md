# Hadoop lab using an Intel NUC Cluster
This document contains instructions for creating a Hadoop big data cluster using Intel NUCs as servers.  
Author: Matt Osentoski  
E-mail: matt.osentoski@gmail.com

## Instructions
The files below can be followed in order to create a big data cluster. However, once the initial hardware/OS and Hadoop
are configured, the individual big data services (Hive, Spark, etc.) can be installed, or not, as needed.

### Hardware requirements
- 1+ Intel NUC servers.  (I'm using a combination of NUC7PJYH1 and NUC5CPYH servers)
- Memory.  I use 8GB per server (The max allowed).  **NOTE: Be sure to use the right number of memory sticks for
your particualr NUC server.  For example, the NUC7PJYH1 is dual channel and requires 2 DIMMs. Whereas
the NUC5CPYH only has room for one.**
- SSD Hard drives.  I'm using SanDisk SSD PLUS 120GB Solid State Drive - SDSSDA-120G-G27 for each NUC. 
This should provide a combination of good speed and capacity at a low price point.
- Gigabit Ethernet Switch.  Any inexpensive Gigabit Ethernet switch will work.  Just make sure to get one
with enough ports for the number of servers you plan to use.  
- Ethernet cables.  Buy enough cables for all your servers.  I chose very short cables for my install (~12" cables)

### Internet Routing considerations
Assuming your NUC cluster lives on its own private network (this is how I'm running it) the servers will need access to 
the internet for software downloads, updates, etc. To set this up, you will have to point the 'default gateway' of each
NUC device to a router that can route packets to the Internet.  (Ex: Your Internet router address provided by your ISP, etc)
  
Another option is to setup one of your NUC servers as a gateway server.  This isn't required, but outlined in the
document called `optional-nuc-gateway/ubuntu-gateway-setup.md`

### Required setup instructions
1. `/nuc-cluster-setup/nuc-hardware.md` - This file explains the hardware setup of the NUC, including preparing Ubuntu
for install
2. `/nuc-cluster-setup/ubuntu-image-install.md` Describes the process of creating an Ubuntu boot image onto a USB drive and install.
3. `/nuc-cluster-setup/ubuntu-setup.md` - This file describes the install process for ubuntu.
4. `hadoop-install.md` - Details instructions for setting up Hadoop

### Optional setup instructions
