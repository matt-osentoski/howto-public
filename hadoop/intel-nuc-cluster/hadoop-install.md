# Hadoop Installation
This document describes how to install a Hadoop cluster onto Intel NUC servers.  Add the configs to all hadoop servers unless the instructions ask for a specific server (Ex: name node)

Author: Matt Osentoski  
E-mail: matt.osentoski@gmail.com

### Install Hadoop
```
cd ~/
wget http://www-us.apache.org/dist/hadoop/common/hadoop-3.0.3/hadoop-3.0.3.tar.gz
sudo tar -xvzf hadoop-3.0.3.tar.gz -C /opt/
cd /opt
sudo ln -s hadoop-3.0.3 hadoop
sudo chown -R hduser:hadoop hadoop-3.0.3
sudo chown -R hduser:hadoop hadoop
```

### Setup environment variables
Add the following settings to /etc/bash.bashrc:
```
export HADOOP_HOME=/opt/hadoop
export PATH=$PATH:$HADOOP_HOME/bin
```

### Make the following changes to: /opt/hadoop/etc/hadoop/hadoop-env.sh
The java implementation to use. Required.
```
export JAVA_HOME=$(readlink -f /usr/bin/java | sed "s:bin/java::")
```

### Create the base directory for HDFS
```
sudo mkdir -p /hdfs/tmp
sudo chown hduser:hadoop /hdfs/tmp
sudo chmod 750 /hdfs/tmp
```

### Update the following file: 
`/opt/hadoop/etc/hadoop/core-site.xml`

Add the following configs
```xml
<configuration>
	<property>
	    <name>hadoop.tmp.dir</name>
	    <value>/hdfs/tmp</value>
	</property>
	<property>
		<name>fs.default.name</name>
		<value>hdfs://nuc-name1:9000</value>
	</property>
</configuration>
```

### Update the map reduce settings file
Update the `/opt/hadoop/etc/hadoop/mapred-site.xml` file with the following configs:

```xml
<configuration>
  <property>
    <name>mapreduce.framework.name</name>
    <value>yarn</value>
  </property>
  <property>
      <name>yarn.app.mapreduce.am.env</name>
      <value>HADOOP_MAPRED_HOME=${HADOOP_HOME}</value>
  </property>
  <property>
      <name>mapreduce.map.env</name>
      <value>HADOOP_MAPRED_HOME=${HADOOP_HOME}</value>
  </property>
  <property>
      <name>mapreduce.reduce.env</name>
      <value>HADOOP_MAPRED_HOME=${HADOOP_HOME}</value>
  </property>
</configuration>
```

### Update the following file: 
`/opt/hadoop/etc/hadoop/hdfs-site.xml`

Add the following configs:
```xml
<configuration>
	<property>
	    <name>dfs.replication</name>
	    <value>2</value> <!-- Assuming you have at least 2 datanodes -->
	</property>
	<!-- Web HDFS configuration required by HUE -->
	<property>
		<name>dfs.webhdfs.enable</name>
		<value>true</value>
	</property>
</configuration> 
```

### Add custom configurations to the yarn-site.xml file
```
vi /opt/hadoop/etc/hadoop/yarn-site.xml
```
Add custom configs
```xml
<configuration>
    <property>
        <name>yarn.resourcemanager.hostname</name>
        <value>nuc-name1</value>
        <description></description>
    </property>
    <property>
        <name>yarn.nodemanager.aux-services</name>
        <value>mapreduce_shuffle</value>
    </property>
    <property>
        <name>yarn.nodemanager.aux-services.mapreduce_shuffle.class</name>
        <value>org.apache.hadoop.mapred.ShuffleHandler</value>
    </property>
</configuration>
```


### Add the slave nodes to the 'slaves' file on the namenode.
>(NOTE!!!!!  This config is only needed on the namenode)

```
vi /opt/hadoop/etc/hadoop/workers
```
Add the slave nodes:
```
nuc-data1
nuc-data2
```

### Create the HDFS file system
```
hadoop namenode -format
```

### Update the hosts file
```
sudo vi /etc/hosts
```
Append the following values or whatever matches your cluster:
```
192.168.6.1     nuc-name1
192.168.6.2     nuc-data1
192.168.6.3     nuc-data2
```
>(NOTE!!!! make sure there isn't a 127.0.0.1 pointing to the same name as your host for the server!)
For example:  
```127.0.0.1   nuc-name1```  
This can cause issues.

### Start/test hadoop
Run the following commands on the Name node (nuc-name1)
```
/opt/hadoop/sbin/start-dfs.sh
/opt/hadoop/sbin/start-yarn.sh
```


### Test that the server is running
http://192.168.6.1:9870

------

# Misc. Operations

### Clean out any existing HDFS files
```
sudo rm -rf /hdfs/tmp/*
```

### Operations on a single node
```
### These are the newer commands and should be used instead of the deprecated ones below
$HADOOP_HOME/bin/hdfs --daemon start datanode
$HADOOP_HOME/bin/yarn --daemon start nodemanager

### NOTE: The commands below are deprecated
./hadoop-daemon.sh stop tasktracker
./hadoop-daemon.sh stop datanode
```
### use this command to restart a dead node
```
### These are the newer commands and should be used instead of the deprecated ones below
$HADOOP_HOME/bin/hdfs --daemon stop datanode
$HADOOP_HOME/bin/yarn --daemon start nodemanager

### NOTE: The commands below are deprecated
./hadoop-daemon.sh start datanode
./hadoop-daemon.sh start tasktracker
```