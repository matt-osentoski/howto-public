# Spark Installation
Spark is an analytics engine for processing big data

Author: Matt Osentoski
E-mail: matt.osentoski@gmail.com

### Run the following commands on all server that will be running Spark:
```
wget https://archive.apache.org/dist/spark/spark-2.3.1/spark-2.3.1-bin-without-hadoop.tgz
sudo mv spark-2.3.1-bin-without-hadoop.tgz /opt
cd /opt
sudo tar zxvf spark-2.3.1-bin-without-hadoop.tgz
sudo ln -s spark-2.3.1-bin-without-hadoop spark
sudo chown -R hduser:hadoop spark-2.3.1-bin-without-hadoop/
sudo chown -R hduser:hadoop spark
```

### Setup environment variables
```
sudo vi /etc/bash.bashrc
```
Add / modify the following lines:
```
export SPARK_HOME=/opt/spark
export PATH=$PATH:$SPARK_HOME/bin
```

### Update the spark environment configuration file on the master server
>(NOTE: You only have to make this change on the master server, we will copy the environment config file over in a later step)

Make a copy of the Spark environment config file from the example template:
```
cd /opt/spark/conf
cp spark-env.sh.template spark-env.sh
```
Add the following settings to this file:
```
export HADOOP_CONF_DIR=/opt/hadoop/etc/hadoop
export SPARK_WORKER_CORES=2
export SPARK_DIST_CLASSPATH=$(/opt/hadoop/bin/hadoop --config /opt/hadoop/etc/hadoop classpath)
```
>(NOTE: You can adjust the # of cores to suit your cluster)

### Configure the `spark-defaults.conf` file
Make a copy of the Spark defaults configuration file:
```
cd /opt/spark/conf
cp spark-defaults.conf.template spark-defaults.conf
```
Add the following settings to this file:
```
spark.master    yarn
```

### Configure the slaves file
Make a copy of the Spark slaves configuration file from the example template:
```
cd /opt/spark/conf
cp slaves.template slaves
```
Add a line for each slave server.
>(NOTE: Make sure to remove 'localhost' if it won't be acting as a slave server)

Below is an example:
```
nuc-data1
nuc-data2
```

### Copy Configuration files to the slave nodes. For example:
```
cd /opt/spark/conf
scp spark-env.sh hduser@nuc-hdfs-data1:/opt/spark/conf
scp spark-defaults.conf hduser@nuc-hdfs-data1:/opt/spark/conf
scp slaves hduser@nuc-hdfs-data1:/opt/spark/conf

scp spark-env.sh hduser@nuc-hdfs-data2:/opt/spark/conf
scp spark-defaults.conf hduser@nuc-hdfs-data2:/opt/spark/conf
scp slaves hduser@nuc-hdfs-data2:/opt/spark/conf
```

### Start Spark from the Master server
Run the following command:
```
/opt/spark/sbin/start-all.sh
```

### (Optional) Start a single worker node
Sometimes, a worker node won't start or will need to be started manually. To start the
node, perform the following steps:

- First, determine the spark master URL.  Go to the Spark master Web application:
[http://192.168.6.6:8080]()

At the top of the page, you will see the spark URL.  In my case it looks like this:

[spark://nuc-hdfs-name:7077]()

- Next, run the following command on the worker node server:
```
$SPARK_HOME/sbin/start-slave.sh spark://<master>:<port>

## In my case, the command would be:
$SPARK_HOME/sbin/start-slave.sh spark://nuc-hdfs-name:7077
```

### Verify that Spark is running:
http://192.168.6.6:8080/
>(NOTE: This assumes your master server is running on 192.168.6.1)

### Verify that the cluster is running, using Spark-shell
From a machine with Spark installed, run the following command:
```
$SPARK_HOME/bin/spark-shell --master spark://nuc-hdfs-name:7077 --total-executor-cores 2 --executor-memory 512m
```
From the shell run these commands
```
val rdd = sc.parallelize(Array("this", "is", "a", "test"))
rdd.count()
```

If the count of `4` doesn't come back you'll have to troubleshoot the problem.  Make sure
no other jobs are running on the cluster and also make sure that the client machine running
the driver has access to the Master and Worker nodes of the cluster.  

>(NOTE: The worker nodes will use random high port numbers to communicate with the driver, so
a range of ports will have to be opened up, if you're using firewall)

>(NOTE 2: If you're running the shell from a computer with multiple interfaces, you might have to
bind (specify) the IP address of the spark shell driver.  For example:)

`spark-shell --master spark://nuc-hdfs-name:7077  --conf spark.driver.host=192.168.6.250`