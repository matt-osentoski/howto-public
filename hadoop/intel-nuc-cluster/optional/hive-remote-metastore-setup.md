# Hive Remote Metastore Setup
The Hive Metastore can (and probably should) be run on a remote database.  This allows for
concurrent connections and allows non-Hive systems access to the Metastore tables.

Author: Matt Osentoski  
E-mail: matt.osentoski@gmail.com

>(NOTE: This document is derived from the Hive WIKI instructions and the following URL: 
[https://www.cloudera.com/documentation/enterprise/5-9-x/topics/cdh_ig_hive_metastore_configure.html#topic_18_4_1__title_508](https://www.cloudera.com/documentation/enterprise/5-9-x/topics/cdh_ig_hive_metastore_configure.html#topic_18_4_1__title_508) ) 

## Dependencies
### Hive
This document assumes that you already have Hive installed using the `hive-install.md` file in this 
directory

### Mysql 
It's also assumed that you already have a Mysql database running externally that is accessible to
the Hadoop cluster where Hive is installed.

## Setup Hive to connect to a remote database
### Install the Mysql driver
Run the following commands:

```
cd ~
wget https://dev.mysql.com/get/Downloads/Connector-J/mysql-connector-java-8.0.16.tar.gz
tar zxvf mysql-connector-java-8.0.16.tar.gz
cd mysql-connector-java-8.0.16
cp mysql-connector-java-8.0.16.jar $HIVE_HOME/lib
```

### Setup the Hive configuration file.
First, make a backup of the original `hive-site.xml` file:
```
cp $HIVE_HOME/conf/hive-site.xml $HIVE_HOME/conf/bkup_hive-site.xml
```

Next, make the following changes/additions to the `$HIVE_HOME/conf/hive-site.xml` file. 

>(NOTE: Be sure to search the hive-site.xml file to make sure the elements below don't already exist,
before adding them to the file)

```
<property>
  <name>javax.jdo.option.ConnectionURL</name>
  <value>jdbc:mysql://192.168.6.156:3306/metastore</value>
  <description>the URL of the MySQL database</description>
</property>

<property>
  <name>javax.jdo.option.ConnectionDriverName</name>
  <value>com.mysql.jdbc.Driver</value>
</property>

<property>
  <name>javax.jdo.option.ConnectionUserName</name>
  <value>root</value>
</property>

<property>
  <name>javax.jdo.option.ConnectionPassword</name>
  <value>password</value>
</property>

<property>
  <name>datanucleus.autoCreateSchema</name>
  <value>false</value>
</property>

<property>
  <name>hive.metastore.uris</name>
  <value>thrift://192.168.6.6:9083</value>
  <description>IP address (or fully-qualified domain name) and port of the metastore host</description>
</property>

<property>
    <name>hive.metastore.schema.verification</name>
    <value>true</value>
</property>

<property>
      <name>hive.server2.transport.mode</name>
      <value>binary</value>
  </property>

  <property>
      <name>hive.server2.thrift.http.port</name>
      <value>10001</value>
  </property>

  <property>
      <name>hive.server2.thrift.http.path</name>
      <value>cliservice</value>
  </property>
  
  <property>
    <name>hive.server2.authentication</name>
    <value>NOSASL</value> 
</property>

<!-- VERY Important setting -->
 <property>
    <name>hive.server2.enable.doAs</name>
    <value>false</value> 
</property>

<!-- VERY Important setting -->
<property>
    <name>hive.metastore.event.db.notification.api.auth</name>
    <value>false</value>
</property>
```

### Initialize the Metastore datbase
Run the following command to setup the metastore database in Mysql

```
$HIVE_HOME/bin/schematool -dbType mysql -initSchema
```

You should now see a `metastore` database populated with tables in your Mysql database.

## Run Hive in Remote mode
### Start up the thrift service
```
$HIVE_HOME/bin/hive --service metastore
```

### Start up Hiveserver2
```
$HIVE_HOME/bin/hive --service hiveserver2
```

### Verify that the Web UI is running
Go to the following URL:

[http://192.168.6.6:10002/](http://192.168.6.6:10002/)


### Verify that you can access the database using `beeline`
Run the following commands:
```
$HIVE_HOME/bin/beeline
## Inside beeline, run the following command:

!connect jdbc:hive2://192.168.6.6:10000/default;auth=noSasl hive password org.apache.hive.jdbc.HiveDriver
```

## Troubleshooting
### Hive logs
Hive logs are usually stored in:

`/tmp/<USER>/hive.log`

