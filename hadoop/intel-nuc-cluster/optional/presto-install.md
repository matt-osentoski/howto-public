# Presto installation
Presto is a distributed SQL query engine for Big Data.  (Amazon's Athena service uses
Presto behind the scenes. )

Presto should be installed on all Hadoop cluster nodes with configuration changes for the master node
(called the `coordinator` node in Presto terminology) and worker nodes. 
  
Author: Matt Osentoski  
E-mail: matt.osentoski@gmail.com

>(NOTE: These instructions are derived from the following URL:
[http://prestodb.github.io/docs/current/installation/deployment.html](http://prestodb.github.io/docs/current/installation/deployment.html)
)

## Dependencies
Make sure Python is installed on your nodes:

```
sudo apt install python
```

## Run the following commands:

```
wget https://repo1.maven.org/maven2/com/facebook/presto/presto-server/0.219/presto-server-0.219.tar.gz
sudo mv presto-server-0.219.tar.gz /opt
cd /opt
sudo tar zxvf presto-server-0.219.tar.gz
sudo ln -s presto-server-0.219 presto
sudo chown -R hduser:hadoop presto-server-0.219
sudo chown -R hduser:hadoop presto
sudo mkdir /var/presto
sudo chown -R hduser:hadoop /var/presto
```

## Setup environment variables
```
sudo vi /etc/bash.bashrc
```
Add / modify the following lines:
```
export PRESTO_HOME=/opt/presto
```

## Create the configuration files
Presto configuration files will live in an `etc` folder from the installation root.  Run
the following commands:

```
mkdir /opt/presto/etc
```

Next, create the following three files with contents below. The `config.properties` file will
vary depending on the node.  Master node configurations will be different than worker nodes:

### Create `$PRESTO_HOME/etc/node.properties`
>(NOTE: Set `node.id` to a unique UUID value)

```
node.environment=dev
node.id=4ffcdec0-78f6-11e9-8f9e-2a86e4085a59
node.data-dir=/var/presto/data
```

### Create `$PRESTO_HOME/etc/jvm.config`

```
-server
-Xmx6G
-XX:+UseG1GC
-XX:G1HeapRegionSize=32M
-XX:+UseGCOverheadLimit
-XX:+ExplicitGCInvokesConcurrent
-XX:+HeapDumpOnOutOfMemoryError
-XX:+ExitOnOutOfMemoryError
```

### Create `$PRESTO_HOME/etc/config.properties` on the Coordinator node ONLY

>(NOTE: I'm changing the HTTP port from 8080 to `8989` to avoid port collisions with other services)

```
coordinator=true
node-scheduler.include-coordinator=true
http-server.http.port=8989
query.max-memory=12GB
query.max-memory-per-node=1GB
query.max-total-memory-per-node=4GB
discovery-server.enabled=true
discovery.uri=http://192.168.6.6:8989
```

### Create `$PRESTO_HOME/etc/config.properties` on each of the Worker nodes

```
coordinator=false
http-server.http.port=8989
query.max-memory=12GB
query.max-memory-per-node=1GB
query.max-total-memory-per-node=4GB
discovery.uri=http://192.168.6.6:8989
```

## Start Presto
Run the following command from each node:

```
$PRESTO_HOME/bin/launcher start
```

## Verify that Presto is running
Since I'm using `8989` as the port that Presto is listening on, and the coodinator 
node is running on `192.168.6.6`, then the following URL will display the Presto query 
dashboard:

[http://192.168.6.6:8989/ui/](http://192.168.6.6:8989/ui/)


