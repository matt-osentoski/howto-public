# Hive installation
Hive is a SQL-based data warehouse that sits on top of Hadoop.
  
Hive should be installed on a machine on the hadoop cluster.  In this example, I will
be using the namenode, however, you would probably want to put it on a datanode in a more serious install.
>(NOTE: If you will be using Tez as the execution engine for Hive, the 'tez-install.md' README will be a dependency
you should install before installing Hive)

Author: Matt Osentoski  
E-mail: matt.osentoski@gmail.com

### Run the following commands:
>(NOTE: Some execution engines, like Spark, will require that the library Jars are installed on all machines. To be safe,
you should perform the following steps on all nodes that will be processing Hive jobs)

```
wget http://www-us.apache.org/dist/hive/hive-3.0.0/apache-hive-3.0.0-bin.tar.gz
sudo mv apache-hive-3.0.0-bin.tar.gz /opt
cd /opt
sudo tar zxvf apache-hive-3.0.0-bin.tar.gz
sudo ln -s apache-hive-3.0.0-bin hive
sudo chown -R hduser:hadoop apache-hive-3.0.0-bin/
sudo chown -R hduser:hadoop hive
```

### Setup environment variables
```
sudo vi /etc/bash.bashrc
```
Add / modify the following lines:
```
export HADOOP_USER_CLASSPATH_FIRST=true
export HIVE_HOME=/opt/hive
export PATH=$PATH:$HIVE_HOME/bin
```

### Setup hive directories
>(NOTE: Make sure Hadoop is already running. Also, this step only has to be performed once.)

```
hadoop fs -mkdir /tmp
hadoop fs -mkdir -p /user/hive/warehouse
hadoop fs -chmod g+w /tmp
hadoop fs -chmod g+w /user/hive/warehouse
```

### Setup the configuration directories:
```
cd $HIVE_HOME/conf
cp hive-default.xml.template hive-site.xml
cp hive-env.sh.template hive-env.sh
```

### Modify the hive-site.xml file
```
cd $HIVE_HOME/conf
vi hive-site.xml
```
Make the following changes / verify existing settings:
>(NOTE!!!!!  The 'hive.execution.engine' is set for 'mr' default.  If you
have the Tez engine installed (see tez-install.md) you can change it to 'tez', this
will GREATLY improve hive's performance)

```
<!-- 'mr' is the default map reduce engine, you will probably want to change this to 'spark' in the section below -->
<property>
    <name>hive.execution.engine</name>
    <value>mr</value>
</property>
<!-- NOTE: The default value for the properties below will error out due to pathing issues. -->
<property>
    <name>hive.exec.local.scratchdir</name>
    <value>/tmp/hduser</value>
    <description>Local scratch space for Hive jobs</description>
</property>
<property>
    <name>hive.downloaded.resources.dir</name>
    <value>/tmp/${hive.session.id}_resources</value>
    <description>Temporary local directory for added resources in the remote file system.</description>
</property>
<property>
    <name>hive.server2.logging.operation.log.location</name>
    <value>/tmp/${system:user.name}/operation_logs</value>
    <description>Top level directory where operation logs are stored if logging functionality is enabled</description>
</property>
<property>
    <name>hive.querylog.location</name>
    <value>/tmp/${system:user.name}</value>
    <description>Location of Hive run time structured log file</description>
</property>
```

>(NOTE: There seems to be an issue with the system finding `${system:java.io.tmpdir}` in the original hive-site.xml file.
The example config above replaces this value with '/tmp' to resolve the errors.  There may also be a problem with
`${system:user.name}`.  If this is the case you'll have to replace that with the username.  In our example that would be
'hduser')

### (Optional) Install the Hive execution JAR (When using the Tez Engine)
```
hadoop fs -mkdir -p /apps/hive/install
hadoop fs -copyFromLocal /opt/hive/lib/hive-exec-3.0.0.jar /apps/hive/install/
```

### Initialize the database
Run the following command to build the Hive metadata tables:
```
schematool -initSchema -dbType derby
```

### Run Hive
To run hive, execute the following command:
```
hive
```

# Setup Hive to use Spark as the execution engine
To use spark instead of the default map reduce engine, perform the following steps:

### Setup and start the Spark cluster
Look at the `spark-install.md` README file in this directory for instructions on setting up a Spark cluster.  Make sure this
server is running on all nodes.

### Update the Hive execution engine setting
Inside the `$HIVE_HOME/conf/hive-site.xml` file, make the following change:

```
<property>
    <name>hive.execution.engine</name>
    <value>spark</value>
</property>
```

### Delete and recreate the Hive Metastore database.
If you're using Derby, run the following command where the DB is stored:
```
rm -Rf metastore_db/
```

Recreate the database using the following command:
```
schematool -initSchema -dbType derby
```

### Update the Hadoop `yarn-site.xml` file
Make the following updates to the `$HADOOP_HOME/etc/hadoop/yarn-site.xml` file:
```
<property>
    <name>yarn.nodemanager.resource.memory-mb</name>
    <value>6144</value> <!-- 6 GB -->
</property>

<property>
    <name>yarn.scheduler.minimum-allocation-mb</name> <!-- RAM-per-container-->
    <value>2048</value>
</property>

<property>
    <name>yarn.scheduler.maximum-allocation-mb</name> <!--Max RAM-per-container-->
    <value>6144</value>
</property>
```

### Restart the Yarn process for the cluster
Run the following commands to start and stop the Yarn service on the cluster
```
$HADOOP_HOME/sbin/stop-yarn.sh
$HADOOP_HOME/sbin/start-yarn.sh
```

### Update the `hive-site.xml` file
Make the following additions to the '$HIVE_HOME/conf/hive-site.xml' file:
```
<property>
    <name>spark.master</name>
    <!-- Enter your Spark master URL here -->
    <value>spark://nuc-hdfs-name:7077</value>
</property>
<!--
<property>
    <name>spark.eventLog.enabled</name>
    <value>true</value>
</property>
<property>
    <name>spark.eventLog.dir</name>
    <value>file:///tmp/spark-events</value>
</property>
-->
<property>
    <name>spark.executor.memory</name>
    <value>512m</value>
</property>
<property>
    <name>spark.serializer</name>
    <value>org.apache.spark.serializer.KryoSerializer</value>
</property>
```

### Copy the Spark libraries into Hive
You will have to copy a few jar files from Spark into your Hive 'lib' folder.
>(NOTE: Copy these files on all Hadoop servers that will be running Hive jobs)

```
cp $SPARK_HOME/jars/scala-library-2.11.12.jar $HIVE_HOME/lib
cp $SPARK_HOME/jars/spark-core_2.11-2.4.0.jar $HIVE_HOME/lib
cp $SPARK_HOME/jars/spark-network-common_2.11-2.4.0.jar $HIVE_HOME/lib/
# This library might be optional
cp $SPARK_HOME/jars/kryo-shaded-4.0.2.jar $HIVE_HOME/lib/
```

# Setup Hive to use S3 for table storage
In these examples a Rook / Ceph filesystem that is running on Kubernetes will be used instead of
Amazon S3.  Ceph has an S3 compatible object store for testing scenarios like this without the need
for AWS.  In theory, these instructions should also work on AWS S3 with a few minor modifications
to the `fs.s3a.endpoint` properties, etc.

>(NOTE: These instructions are derived from: [https://blog.mustardgrain.com/2010/09/30/using-hive-with-existing-files-on-s3/](https://blog.mustardgrain.com/2010/09/30/using-hive-with-existing-files-on-s3/) )


## Add the AWS jars to the Hive classpath
>(NOTE: Jar files can be looked up and downloaded at [http://central.maven.org](http://central.maven.org))

```
cd $HIVE_HOME/lib
wget http://central.maven.org/maven2/org/apache/hadoop/hadoop-aws/3.2.0/hadoop-aws-3.2.0.jar
wget http://central.maven.org/maven2/com/amazonaws/aws-java-sdk-s3/1.11.563/aws-java-sdk-s3-1.11.563.jar
wget http://central.maven.org/maven2/com/amazonaws/aws-java-sdk-core/1.11.563/aws-java-sdk-core-1.11.563.jar
wget http://central.maven.org/maven2/com/amazonaws/aws-java-sdk-dynamodb/1.11.563/aws-java-sdk-dynamodb-1.11.563.jar
```

## Add the AWS jars to the Map Reduce engine classpath
In our case, we'll be using Spark as the Map Reduce engine for Hive.  The AWS jars you
added above will have to be available to Spark (or any other engine like Tez, etc)

Run the following commands on all servers where spark will be executing the Hive table:
```
cd $SPARK_HOME/jars
wget http://central.maven.org/maven2/org/apache/hadoop/hadoop-aws/3.2.0/hadoop-aws-3.2.0.jar
wget http://central.maven.org/maven2/com/amazonaws/aws-java-sdk-s3/1.11.563/aws-java-sdk-s3-1.11.563.jar
wget http://central.maven.org/maven2/com/amazonaws/aws-java-sdk-core/1.11.563/aws-java-sdk-core-1.11.563.jar
wget http://central.maven.org/maven2/com/amazonaws/aws-java-sdk-dynamodb/1.11.563/aws-java-sdk-dynamodb-1.11.563.jar
```

>(NOTE: Make sure to restart the metastore and hive2 servers after updating the jar and making
the config changes below)

## Configuration changes
Add the following lines to your `$HIVE_HOME/conf/hive-site.xml` file
```
<!-- S3 configurations -->
<property>
  <name>fs.s3a.access.key</name>
  <value>abc123abc123</value>
</property>
<property>
  <name>fs.s3a.secret.key</name>
  <value>asdf1234ASDF1234asdaf1234ASDF1234asdf1234</value>
</property>
<property>
  <name>fs.s3a.aws.credentials.provider</name>
  <value>org.apache.hadoop.fs.s3a.SimpleAWSCredentialsProvider</value>
</property>
<property>
  <name>fs.s3a.path.style.access</name>
  <value>true</value>
</property>
<!-- Enter your CEPH object server host here or AWS URL -->
<property>
  <name>fs.s3a.endpoint</name>
  <value>192.168.6.151</value>
</property>
<property>
  <name>fs.s3a.connection.ssl.enabled</name>
  <value>false</value>
  <description>Enables or disables SSL connections to S3.</description>
</property>
<property>
  <name>fs.s3a.impl</name>
  <value>org.apache.hadoop.fs.s3a.S3AFileSystem</value>
  <description>The implementation class of the S3A Filesystem</description>
</property>

```

## Example setting up an S3 backed table
Run the following command from the `hive` or `beeline` CLI tool.  
```sql
CREATE EXTERNAL TABLE mydata (key STRING, value INT)
    stored as parquet
    LOCATION 's3a://test/stuff/';
```

## Test the table
All the pieces can be tested by inserting a row into the table we created above.
Run the following command from the `beeline` CLI tool.

```sql
INSERT INTO mydata (key, value) VALUES ("test",1);
```

At this point you should see a parquet file that was created in your Ceph/s3 bucket.  You can also query
using:

```sql
SELECT * FROM mydata;
SELECT count(*) FROM mydata
```

# Troubleshooting
## Hive logs
Hive logs are usually stored in:

`/tmp/<USER>/hive.log`

## Errors
**Error**: 
