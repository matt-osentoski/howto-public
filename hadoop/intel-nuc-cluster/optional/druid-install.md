# Druid installation
Druid is a time series database that allows for near-real time analytics.
Druid is a good choice as a speed layer in Big data when paired with BI/Analytics
dashboards like Superset.

Druid is composed of 3 servers running 5 or more processes:
1. Master Server - Running the `coordinator` and `overlord` services
2. Data Server - Running the `historical` and `middle manager` services
3. Query Server - Running the `broker` services

In addition there is also an optional `tranquility` server for stream ingestion.
  
Author: Matt Osentoski  
E-mail: matt.osentoski@gmail.com

## Run the following commands:

```
wget https://www-us.apache.org/dist/incubator/druid/0.14.2-incubating/apache-druid-0.14.2-incubating-bin.tar.gz
sudo mv apache-druid-0.14.2-incubating-bin.tar.gz /opt
cd /opt
sudo tar zxvf apache-druid-0.14.2-incubating-bin.tar.gz
sudo ln -s apache-druid-0.14.2-incubating druid
sudo chown -R hduser:hadoop apache-druid-0.14.2-incubating
sudo chown -R hduser:hadoop druid
```

## Setup environment variables
```
sudo vi /etc/bash.bashrc
```
Add / modify the following lines:
```
export DRUID_HOME=/opt/druid
export PATH=$PATH:$DRUID_HOME/bin
```

## Copy Mysql JDBC Driver into the classpath
>(NOTE: Druid seems to require an older version of the Mysql driver. Version 8.x was having issues)

```
cd $DRUID_HOME/lib
wget http://central.maven.org/maven2/mysql/mysql-connector-java/5.1.38/mysql-connector-java-5.1.38.jar

```

## Create the Druid metastore database in Mysql
The character encoding of the database is VERY important. It might be best
to manually create the metadata database using the following SQL statement.

```sql
CREATE SCHEMA `druid` DEFAULT CHARACTER SET utf8mb4 ;
```

## Configuration - Common
Make the following changes to the 
`$DRUID_HOME/conf/druid/_common/common.runtime.properties` file.  

>(NOTE: Read through the file carefully, you will have to comment out sections as you
expose others.  For example, you will have to comment out the Derby database settings to
use Mysql as the metastore DB)

```
# Add the Mysql metadata extension here
druid.extensions.loadList=["druid-histogram", "druid-datasketches", "druid-lookups-cached-global","mysql-metadata-storage"]

# External Zookeeper URL
druid.zk.service.host=192.168.6.6

# Mysql Metadata store configs
# (IMPORTANT!!!!! Comment out the Derby configurations!!!)
druid.metadata.storage.type=mysql
druid.metadata.storage.connector.connectURI=jdbc:mysql://192.168.6.156:3306/druid
druid.metadata.storage.connector.user=root
druid.metadata.storage.connector.password=password

```

# Coordinator Service
The Coordinator service should live on the `Master Server`.  

## Configuration
Verify the settings in the `$DRUID_HOME/conf/druid/coordinator/runtime.properties` file.
For example, you might have to make a change to the port number of the service
if this port is already running on the server 'druid.plaintextPort'

## Run the Coordinator service
```
cd $DRUID_HOME
java `cat conf/druid/coordinator/jvm.config | xargs` -cp conf/druid/_common:conf/druid/coordinator:lib/* org.apache.druid.cli.Main server coordinator
```

## Veriy the Coordinator Service is running
The service should be running at the following URL. (Assuming the IP address is 192.168.6.7)
It might take a few minutes to start up.

>(NOTE: If you changed the 'druid.plaintextPort' in coordinator configuration, then use
that number instead of '8081' below)

[http://192.168.6.7:8081](http://192.168.6.7:8081)

# Overlord Service
The Overlord service should live on the `Master Server`. 

## Configuration
Verify the settings in the `$DRUID_HOME/conf/druid/overlord/runtime.properties` file.
For example, you might have to make a change to the port number of the service
if this port is already running on the server 'druid.plaintextPort'

## Run the Coordinator service
```
cd $DRUID_HOME
java `cat conf/druid/overlord/jvm.config | xargs` -cp conf/druid/_common:conf/druid/overlord:lib/* org.apache.druid.cli.Main server overlord
```

## Veriy the Coordinator Service is running
The service should be running at the following URL. 

[http://192.168.6.7:8090](http://192.168.6.7:8090)

# Historical Service
The Historical service should live on the `Data Server`.  This should be run on a separate
server than the Master server services

## Copy files to the Data Server
For the Data server you will need to duplicate the installation and setup steps above
on your data server.  An easier way, is to just 'rsync' the files over from the master
server.  

From the master server, run the following command:
>(NOTE: This assumes the destination server IP is 192.168.6.8)

```
rsync -az apache-druid-0.14.2-incubating 192.168.6.8:/home/hduser/apache-druid-0.14.2-incubating 
```

On the data server (the target of the rsync), run the following commands:
```
sudo mv apache-druid-0.14.2-incubating/ /opt
cd /opt
sudo ln -s apache-druid-0.14.2-incubating druid
sudo chown -R hduser:hadoop apache-druid-0.14.2-incubating
sudo chown -R hduser:hadoop druid
```

Next, update the environment variables:
```
sudo vi /etc/bash.bashrc
```
Add / modify the following lines:
```
export DRUID_HOME=/opt/druid
export PATH=$PATH:$DRUID_HOME/bin
```

## Configuration
Verify the settings in the `$DRUID_HOME/conf/druid/historical/runtime.properties` file.
For example, you might have to make a change to the port number of the service
if this port is already running on the server 'druid.plaintextPort'  

If you receive out of memory errors, you might have to reduce the number of threads that are
setup in this file:

```
# Reduced to '2' threads to use less memory
druid.processing.numThreads=2
```

## Run the Historical service
```
cd $DRUID_HOME
java `cat conf/druid/historical/jvm.config | xargs` -cp conf/druid/_common:conf/druid/historical:lib/* org.apache.druid.cli.Main server historical
```

# Middle Manager Service
The Middle Manager service should live on the `Data Server`.  This should be run on a separate
server than the Master server services

## Configuration
Verify the settings in the `$DRUID_HOME/conf/druid/middlemanager/runtime.properties` file.
For example, you might have to make a change to the port number of the service
if this port is already running on the server 'druid.plaintextPort'  

## Run the Middle Manager service
```
cd $DRUID_HOME
java `cat conf/druid/middleManager/jvm.config | xargs` -cp conf/druid/_common:conf/druid/middleManager:lib/* org.apache.druid.cli.Main server middleManager
```

# Broker Service
The Broker service should live on the `Query Server`.  This should be run on a separate
server than the Master and Data servers

## Copy files to the Data Query Server
For each query server (there can be multiple), copy your files/configuration similar to the
section `Copy files to the Data Server` above.

## Configuration
Verify the settings in the `$DRUID_HOME/conf/druid/broker/runtime.properties` file.
For example, you might have to make a change to the port number of the service
if this port is already running on the server 'druid.plaintextPort'  

## Run the Broker service
```
cd $DRUID_HOME
java `cat conf/druid/broker/jvm.config | xargs` -cp conf/druid/_common:conf/druid/broker:lib/* org.apache.druid.cli.Main server broker
```

>(NOTE: If you run into lack of memory problems in your dev environment, try tweaking 
the `$DRUID_HOME/conf/druid/broker/jvm.config` file to lower the memory requirements. You can 
also try reducing the number of 'druid.server.http.numThreads' in the 
`$DRUID_HOME/conf/druid/broker/runtime.properties` file.)
