# Tez Installation
Tez is a DAG engine, similar to Spark.  It can be used to improve the performance of Hive and Map reduce jobs

Author: Matt Osentoski  
E-mail: matt.osentoski@gmail.com

### Download and install a binary version of the Tez jar:
```
cd ~
wget http://www-us.apache.org/dist/tez/0.9.1/apache-tez-0.9.1-bin.tar.gz
sudo mv apache-tez-0.9.1-bin.tar.gz /opt
cd /opt
sudo tar zxvf apache-tez-0.9.1-bin.tar.gz
sudo ln -s apache-tez-0.9.1-bin tez
sudo chown -R hduser:hadoop apache-tez-0.9.1-bin/
sudo chown -R hduser:hadoop tez
cd /opt/tez/conf
touch tez-site.xml
```
Install the Tarball into HDFS
```
cd /opt
hadoop fs -mkdir -p /apps/tez-0.9.1-bin
hadoop fs -copyFromLocal apache-tez-0.9.1-bin.tar.gz /apps/tez-0.9.1-bin/
hadoop fs -chmod g+w /apps/tez-0.9.1-bin
```

### Update the tez-site.xml file
(First, look at the value for 'fs.default.name' in `/opt/hadoop/etc/hadoop/core-site.xml`
This URL will be used for the 'tez.lib.uris' value below):

Add the following contents into your empty 'tez-site.xml' file:
```xml
<?xml version="1.0"?>
<!-- NOTE: hdfs://nuc-name1:9000 was obtained from /opt/hadoop/etc/hadoop/core-site.xml
The remainder of the URL is from the file you copied into HDFS at the end of the step above.
-->
<configuration>
    <property>
        <name>tez.lib.uris</name>
        <value>hdfs://nuc-name1:9000/apps/tez-0.9.1-bin/apache-tez-0.9.1-bin.tar.gz</value>
        <type>string</type>
    </property>
</configuration>
```

### Setup environment variables
Add the following settings to /etc/bash.bashrc:
```
export TEZ_HOME=/opt/tez
export TEZ_CONF_DIR=$TEZ_HOME/conf
export TEZ_JARS=$TEZ_HOME
export HADOOP_CLASSPATH=${TEZ_CONF_DIR}:${TEZ_JARS}/*:${TEZ_JARS}/lib/*
```

### Change Hadoop's Map reduce settings to use Yarn and Tez
Edit the following file:
`/opt/hadoop/etc/hadoop/mapred-site.xml`
Change the following property:
```xml
<property>
    <name>mapreduce.framework.name</name>
    <value>yarn-tez</value>
</property>
```

