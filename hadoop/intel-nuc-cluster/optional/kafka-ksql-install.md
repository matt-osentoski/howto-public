# Kafka KSQL Server installation
Kafka KSQL server allows topics to be treated as streaming SQL tables

Author: Matt Osentoski  
E-mail: matt.osentoski@gmail.com

## Dependencies
This document assumes you've already installed Zookeeper and Kafka using the 
Confluent installation package.  This package includes additional servers: Registry,
SQL, REST, Connect, etc.   
 
Look at the `kafka-install.md` file in this directory to install Zookeeper and Kafka using Confluent.


## Configure KSQL Server
Make the following changes to your `$KAFKA_HOME/etc/ksql/ksql-server.properties` file. 

>(NOTE: Some settings will have to be changed per node. Ex: IP addresses, broker Id's, etc.  Those properties will be
noted in the configurations below)

```
# The IP address should be changed to match the server's IP address.
#This value will be different for each server.
# IMPORTANT!!!! Hadoop may be using the default port of 8088.
# In my case, I changed the port to '38088' instead.
listeners=PLAINTEXT://192.168.6.6:38088

kafkastore.bootstrap.servers=PLAINTEXT://192.168.6.6:9092,PLAINTEXT://192.168.6.7:9092,PLAINTEXT://192.168.6.8:9092
```

## Starting Kafka KSQL server
To start the Kafka KSQL Server, run the following command:
```
$KAFKA_HOME/bin/ksql-server-start $KAFKA_HOME/etc/ksql/ksql-server.properties
```

### Validate that the KSQL Server is running
Run the following command, which should connect to the KSQL server we just started
```
$KAFKA_HOME/bin/ksql http://192.168.6.6:8088

# or in my case. (Because Hadoop uses 8088)
$KAFKA_HOME/bin/ksql http://192.168.6.6:38088
```