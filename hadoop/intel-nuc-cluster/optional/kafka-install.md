# Kafka installation
Kafka is a distributed streaming framework.

Kafka should be installed on 3 servers in the cluster.  In this example, I will
be installing Kafka on all three hadoop servers in my cluster. For production or high volume usages, dedicated servers should
be used for Kafka and possibly even Zookeeper, depending on the number of topics in use, etc.

Author: Matt Osentoski
E-mail: matt.osentoski@gmail.com

## Installation and Setup
### Run the following commands on three servers in your cluster:
These instructions are derived from: https://kafka.apache.org/quickstart

>(NOTE: I'm using the Confluence community edition package instead of the package at kafka.apache.org.  The reason
is, it comes prepackaged with other Kafka server applications, like Kafka Connect, etc.)

```
wget http://packages.confluent.io/archive/5.2/confluent-community-5.2.1-2.12.tar.gz
sudo mv confluent-community-5.2.1-2.12.tar.gz /opt
cd /opt
sudo tar zxvf confluent-community-5.2.1-2.12.tar.gz
sudo ln -s confluent-5.2.1 kafka
sudo chown -R hduser:hadoop confluent-5.2.1/
sudo chown -R hduser:hadoop kafka
```

### Setup environment variables
```
sudo vi /etc/bash.bashrc
```
Add / modify the following lines:
```
export KAFKA_HOME=/opt/kafka
export PATH=$PATH:$KAFKA_HOME/bin
```

### Configure Kafka
Make the following changes to your `$KAFKA_HOME/etc/kafka/server.properties` file.  These changes will have to be made
on each node where Kafka is installed.
>(NOTE: Some settings will have to be changed per node. Ex: IP addresses, broker Id's, etc.  Those properties will be
noted in the configurations below)

```
# This value should be incremented on each server. For example, the next server would be 'broker.id=1'
#This value will be different for each server.
broker.id=0

# The IP address should be changed to match the server's IP address.
#This value will be different for each server.
listeners=PLAINTEXT://192.168.6.6:9092

# Change this value to a location that isn't transient on the server.  The default /tmp location is deleted on server restart
# This value can remain the same for all servers
log.dirs=/home/hduser/kafka-logs

# This value should match the IP addresses of all the zookeeper servers.  For this example, it will be the same IP as the kafka servers
# This value will be the same on all servers
zookeeper.connect=192.168.6.6:2181,192.168.6.7:2181,192.168.6.8:2181

# Timeout in ms for connecting to zookeeper
zookeeper.connection.timeout.ms=60000

# Allow topics to be deleted (defaults to true now)
# delete.topic.enable=true
```

### Configure Zookeeper
Make the following changes to your `$KAFKA_HOME/etc/kafka/zookeeper.properties` file.  These changes will have to be made
on each node where Kafka is installed.

```
# Change this value to a location that isn't transient on the server.  The default /tmp location is deleted on server restart
# This value can remain the same for all servers
dataDir=/home/hduser/zookeeper
clientPort=2181
server.1=192.168.6.6:2888:3888
server.2=192.168.6.7:2888:3888
server.3=192.168.6.8:2888:3888
tickTime=2000
initLimit=10
syncLimit=5
```

### Create the log directories
If you changed the log/data directories from the defaults, you will have to create the following directories for Kafka and Zookeeper to startup.
This change should be made to all servers running Kafka/Zookeeper.
```
mkdir /home/hduser/kafka-logs
mkdir /home/hduser/zookeeper

# The following directory will also have to be creatd if you're using startup scripts.
mkdir /opt/kafka/logs
```

### Create the `myid` file
On each server, create a text file in the `/home/hduser/zookeeper` directory.  With a unique integer for that server.
For example, our first server would have the file `/home/hduser/zookeeper/myid` with the following value:

```
1
```

The next server would contain `2`, and so on.

