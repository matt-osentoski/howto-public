# Docker installation
This document describes how to install docker on a NUC using Ubuntu 16.04.

>(NOTE: This document is derived from:
[https://docs.docker.com/install/linux/docker-ce/ubuntu/]())

Author: Matt Osentoski  
E-mail: matt.osentoski@gmail.com


### Install docker
>(NOTE: if docker is already installed, you can skip to the next step

```
sudo apt-get update
sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"
sudo apt-get update
sudo apt-get install docker-ce
```

Verify that docker is working by running the following command:
```
sudo docker version
```

