# Pig installation
Pig is a tool that simplifies map reduce jobs

Pig should be installed on a machine on the hadoop cluster.  In this example, I will
be using the namenode, however, you would probably want to put it on a datanode in a more serious install.

Author: Matt Osentoski  
E-mail: matt.osentoski@gmail.com

### Run the following commands:
```
wget http://www-us.apache.org/dist/pig/pig-0.17.0/pig-0.17.0.tar.gz
sudo mv pig-0.17.0.tar.gz /opt
cd /opt
sudo tar zxvf pig-0.17.0.tar.gz
sudo ln -s pig-0.17.0 pig
sudo chown -R hduser:hadoop pig-0.17.0/
sudo chown -R hduser:hadoop pig
```

### Setup environment variables
```
sudo vi /etc/bash.bashrc
```
Add / modify the following lines:
```
export PIG_HOME=/opt/pig
export PATH=$PATH:$PIG_HOME/bin
```

### Connect to hadoop and test pig
```
cd $PIG_HOME/bin
PIG_CLASSPATH=/opt/hadoop/etc/hadoop ./pig
```

From here you should be able to 'ls', 'cd', and 'cat' files in your HDFS cluster.