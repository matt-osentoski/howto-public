# Jupyter Installation
Jupyter is a Notebook server used by data scientists

Author: Matt Osentoski  
E-mail: matt.osentoski@gmail.com

### Dependencies
Make sure Python 3 and pip are already installed on the server.

### Install Jupyter
```
sudo python3 -m pip install --upgrade pip
sudo python3 -m pip install jupyter
```

### Run Jupyter
```
jupyter notebook --generate-config
```
Edit the Jupyter config file, changing the IP address.
```
vi ~/.jupyter/jupyter_notebook_config.py

# Uncomment the following entry and add your IP address
c.NotebookApp.ip = '192.168.6.1'
```
Start Jupyter as a service
```
nohup jupyter notebook --no-browser &> jupyter.out&
```
A URL will display with a token.  Open this URL up in a browser to begin working with Jupyter.
>(NOTE: If you started Jupyter using `nohup` the URL will be in the 'jupyter.out' file


