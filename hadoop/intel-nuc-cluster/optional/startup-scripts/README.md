# To install the startup scripts, follow these steps:

## Script placement in the cluster
IMPORTANT!!! The `hdfs`, `yarn`, and `spark` scripts only have to be place on the master server.  The `kafka` and `zookeeper`
scripts should be place on all servers

### Copy scripts to startup location
All startuup scripts should be copied/created in the following directory: `/etc/init.d`

### Set the script ownership
Verify that root is the owner and group for the scripts.  If not run the following command:
```
cd /etc/init.d
# Make these changes to all servers
sudo chown root:root kafka
sudo chown root:root zookeeper

# These changes should only happen on the master server
sudo chown root:root hdfs
sudo chown root:root yarn
sudo chown root:root spark
sudo chown root:root kafka-registry
sudo chown root:root kafka-ksql
sudo chown root:root kafka-connect
```

### Set permissions
Change the permissions of the scripts to allow the system to start/stop the process.
```
# Make these changes to all servers
sudo chmod 755 kafka
sudo chmod 755 zookeeper

# These changes should only happen on the master server
sudo chmod 755 hdfs
sudo chmod 755 yarn
sudo chmod 755 spark
sudo chmod 755 kafka-registry
sudo chmod 755 kafka-ksql
sudo chmod 755 kafka-connect
```

### Run the update scripts
>(NOTE: The first number after 'defaults' is the `START` order, the second number is the `STOP`.
During startup, the lower number will start first.  During shutdown, the higher number will stop first.)

```
# Make these changes to all servers
## Zookeeper starts before kafka and shuts down after Kafka
sudo update-rc.d zookeeper defaults 90 91
sudo update-rc.d kafka defaults 91 90

# These changes should only happen on the master server
## HDFS starts before Yarn and Spark and shuts down after both yarn/spark have shutdown.
sudo update-rc.d hdfs defaults 90 93
sudo update-rc.d yarn defaults 91 92
sudo update-rc.d spark defaults 92 91
sudo update-rc.d kafka-registry defaults 94 94
sudo update-rc.d kafka-ksql defaults 94 94
sudo update-rc.d kafka-connect defaults 94 94
```

### Test the scripts
Reboot your server a few times to make sure the scripts startup and shutdown correctly and in the right order.

## Script removal
To remove the scripts above from the system, run the following command:
>(NOTE: The '-f' is needed if the script is in /etc/init.d, which our are located)

```
update-rc.d -f zookeeper remove
update-rc.d -f kafka remove
```
