# Kafka Registry Server installation
Kafka registry server hosts object schemas using Avro.  This allows for versioned
changes to your object model, using Avro's compressed data format.

Author: Matt Osentoski  
E-mail: matt.osentoski@gmail.com

## Dependencies
This document assumes you've already installed Zookeeper and Kafka using the 
Confluent installation package.  This package includes additional servers: Registry,
SQL, REST, Connect, etc.   
 
Look at the `kafka-install.md` file in this directory to install Zookeeper and Kafka using Confluent.


## Configure Kafka Registry Server
Make the following changes to your `$KAFKA_HOME/etc/schema-registry/schema-registry.properties` file.  These changes will have to be made
on each node where Kafka is installed.
>(NOTE: Some settings will have to be changed per node. Ex: IP addresses, broker Id's, etc.  Those properties will be
noted in the configurations below)

```
# The IP address should be changed to match the server's IP address.
#This value will be different for each server.
listeners=PLAINTEXT://192.168.6.6:8081

# IMPORTANT!!!!!  The following line should be commented out, use the Kafka
# broker addresses instead
# kafkastore.connection.url=localhost:2181

kafkastore.bootstrap.servers=192.168.6.6:9092,192.168.6.7:9092,192.168.6.8:9092
```

>(NOTE: If you have problems starting the server where it can't find topics, try commenting out
the `kafka.bootstrap.servers` property and uncomment the `kafkastore.connection.url` property, populated
with the zookeeper URLs)

## Starting Kafka Registry server
To start the Kafka Registry Server, run the following command:
```
$KAFKA_HOME/bin/schema-registry-start $KAFKA_HOME/etc/schema-registry/schema-registry.properties
```

### Validate that the Schema Server is running
Access the following URL.  There should server information returned:

[http://192.168.6.6:8081/config]()