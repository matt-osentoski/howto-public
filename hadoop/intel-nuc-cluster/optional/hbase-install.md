# HBase Install
HBase is a distributed column store database that runs on top of HDFS

HBase has master and region servers. The master server is similar to the name node in Hadoop and the region
servers are similar to the data nodes of Hadoop. In this example we will install HBase on the same servers 
where Hadoop currently resides. The name node server will also contain the master Hbase server. The hadoop data
nodes will contain the Hbase region servers. 
Install the software on all the Hadoop nodes as follows:

Author: Matt Osentoski  
E-mail: matt.osentoski@gmail.com

## HBase installation

### Run the following commands on all servers that will be running HBase:
```
wget http://www-us.apache.org/dist/hbase/2.0.1/hbase-2.0.1-bin.tar.gz
sudo mv hbase-2.0.1-bin.tar.gz /opt
cd /opt
sudo tar zxvf hbase-2.0.1-bin.tar.gz
sudo ln -s  hbase-2.0.1 hbase
sudo chown -R hduser:hadoop  hbase-2.0.1/
sudo chown -R hduser:hadoop hbase
```

### Setup environment variables on all servers that will be running HBase:
```
sudo vi /etc/bash.bashrc
```
Add / modify the following lines
```
export HBASE_HOME=/opt/hbase
export PATH=$PATH:$HBASE_HOME/bin
```

-----

## HBase Configuration
>(NOTE: These configuration only apply to the Master server(s) in the cluster.)

### Add the following lines to your $HBASE_HOME/conf/hbase-site.xml file:
>(NOTE: In this example, I'm using the hdfs-gw server as my zookeeper quorum.  This server
runs Kafka which includes zookeeper.  For this example, I will use this zookeeper server for everything)

```xml
<configuration>
  <property>
    <name>hbase.cluster.distributed</name>
    <value>true</value>
  </property>
  <!-- NOTE: hdfs://nuc-name1:9000 was obtained from /opt/hadoop/etc/hadoop/core-site.xml -->
  <property>
    <name>hbase.rootdir</name>
    <value>hdfs://nuc-name1:9000/hbase</value>
  </property>
  <property>
    <name>hbase.zookeeper.quorum</name>
    <value>nuc-name1</value>  <!-- This assumes you're using a stand-alone zookeeper quorum on nuc-name1 -->
  </property>
  <property>
    <name>hbase.zookeeper.property.dataDir</name>
    <value>/tmp/zookeeper</value>
  </property>

  <!-- There's currently a bug using HBase with Hadoop 3.x  The setting below will bypass the error.  Another option
  would be to recompile HBase using -Dhadoop.profile=3.0 and remove the following setting -->
  <property>
    <name>hbase.unsafe.stream.capability.enforce</name>
    <value>false</value>
  </property>
</configuration>
```

### Create a new file called: `$HBASE_HOME/conf/backup-masters`
Enter a line with the hostname for the backup master server

3. Update the regionservers file: `$HBASE_HOME/conf/regionservers`  
a) Remove the 'localhost' entry
b) Add a line for each region server. For example:
```
nuc-data1
nuc-data2
```

### Update the `$HBASE_HOME/conf/hbase-env.sh` file to enable an external zookeeper quorum.
```
export HBASE_MANAGES_ZK=false
```

### Update the `$HBASE_HOME/conf/hbase-env.sh` file to point to the JAVA_HOME
```
export JAVA_HOME=/usr/lib/jvm/jdk-8-oracle-arm-vfp-hflt/jre/
```

### Copy the config files to all the region servers. For example:
```
scp hbase-site.xml hduser@nuc-data1:/opt/hbase/conf
scp backup-masters hduser@nuc-data1:/opt/hbase/conf
scp regionservers hduser@nuc-data1:/opt/hbase/conf
scp hbase-env.sh hduser@nuc-data1:/opt/hbase/conf

scp hbase-site.xml hduser@nuc-data2:/opt/hbase/conf
scp backup-masters hduser@nuc-data2:/opt/hbase/conf
scp regionservers hduser@nuc-data2:/opt/hbase/conf
scp hbase-env.sh hduser@nuc-data2:/opt/hbase/conf
```

### Start Hbase on the master server
```
$HBASE_HOME/bin/start-hbase.sh
```

### Test that HBase is running:
`$HBASE_HOME/bin/hbase shell`
Then run:
```
status 'simple'
```

### (Optional) Start Hbase-thrift server
```
$HBASE_HOME/bin/hbase thrift start
```