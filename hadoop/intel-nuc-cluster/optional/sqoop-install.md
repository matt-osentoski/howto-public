# Scoop Install
Scoop is used for bulk data imports into Hadoop.

Sqoop should be installed on a machine on the hadoop cluster.  In this example, I will
be using the namenode, however, you would probably want to put it on a datanode in a more serious install.

Author: Matt Osentoski  
E-mail: matt.osentoski@gmail.com

### Run the following commands:
```
wget http://www.us.apache.org/dist/sqoop/1.4.7/sqoop-1.4.7.tar.gz
sudo mv sqoop-1.4.7.tar.gz /opt
cd /opt
sudo tar zxvf sqoop-1.4.7.tar.gz
sudo ln -s sqoop-1.4.7 sqoop
sudo chown -R hduser:hadoop sqoop-1.4.7/
sudo chown -R hduser:hadoop sqoop
```

### Setup environment variables
```
sudo vi /etc/bash.bashrc
```
Add / modify the following lines
```
export SQOOP_HOME=/opt/sqoop
export PATH=$PATH:$SQOOP_HOME/bin
```

### Download the latest Mysql driver (jar file and place it in the SQOOP_HOME/lib directory

Sqoop commands:
>(NOTE: If you have problems where the import fails going from 25% to 0% in the map step, try adding the following
parameters to the URL string and reduce the defaultFetchSize as needed:)

`?dontTrackOpenResources=true&defaultFetchSize=1000&useCursorFetch=true`


### Import from mysql directly into HDFS
```
sqoop import --connect "jdbc:mysql://192.168.6.254:3306/stocks_12272015?dontTrackOpenResources=true&defaultFetchSize=100&useCursorFetch=true" --username root --password password --table stocks
```

### Import from mysql to Hive
```
sqoop import --connect "jdbc:mysql://192.168.6.254:3306/stocks_12272015?dontTrackOpenResources=true&defaultFetchSize=100&useCursorFetch=true" --username root --password password --table "stocks" --hive-import --hive-overwrite --hive-database my_database
```
(IMPORTANT NOTE!!! If you're using derby as the database, make sure no hive shell is currently open.  Otherwise, there will be a lock on the database file!)

### Using the --direct option
For some databases (Mysql), you can use '--direct' with the scoop command to use a database dump instead
of the JDBC driver.  
>(NOTE: In the case of Mysql, you will need mysql installed and the mysqldump command on the PATH of the machine
running the sqoop command for this to work.  You can install mysql-server using the following commands):

```
sudo apt-get update
sudo apt-get install mysql-server --fix-missing
```

### Import from mysql to HBase
```
sqoop import --connect "jdbc:mysql://192.168.6.254:3306/stocks_12272015?dontTrackOpenResources=true&defaultFetchSize=100&useCursorFetch=true" --username root --password password --table stocks --hbase-create-table --hbase-table stocks --column-family cf
 ```
>(NOTE: If you encounter the following error, you will have to manually create the table and column family from
Hbase's shell first:  
Exception in thread "main" java.lang.NoSuchMethodError: org.apache.hadoop.hbase.HTableDescriptor.addFamily(Lorg/apache/hadoop/hbase/HColumnDescriptor;)

### To create the table and column family do the following:
1. Open the Hbase shell:

    ```
    $HBASE_HOME/bin/hbase shell
    ```
2. Run the following commands to create the table/column family

    ```
    create 'some-table', 'some-column-family'
    ```
3. Immediately write to HDFS

    ```
    flush 'some-table'
    ```
