# Kafka Connect Server installation
Kafka connect server allows different data stores to synchronize information.

Author: Matt Osentoski  
E-mail: matt.osentoski@gmail.com

## Dependencies
This document assumes you've already installed Zookeeper and Kafka using the 
Confluent installation package.  This package includes additional servers: Registry,
SQL, REST, Connect, etc.   
 
Look at the `kafka-install.md` file in this directory to install Zookeeper and Kafka using Confluent.


## Configure the `worker.properties` file (Stand-alone mode)

There are several examples of the `worker.properties` file:
- $KAFKA_HOME/etc/kafka/connect-standalone.properties
- $KAFKA_HOME/etc/schema-registry/connect-avro-standalone.properties

Find the file that matches whether you want to serialize using Avro or JSON then copy the
file and call it `worker.properties`. For example: 
```
cp $KAFKA_HOME/etc/kafka/connect-standalone.properties $KAFKA_HOME/etc/kafka/worker.properties
```

Customize the `worker.properties` file to match your Kafka cluster environment.
```
bootstrap.servers=PLAINTEXT://192.168.6.6:9092,PLAINTEXT://192.168.6.7:9092,PLAINTEXT://192.168.6.8:9092

# Set the location of the connect offsets file.  Files in the /tmp 
# directory will be transient on reboot
offset.storage.file.filename=/home/hdfs/connect-offsets
```

## Configure `worker.properties` (Cluster mode)

There are several examples of the `worker.properties` file:
- $KAFKA_HOME/etc/kafka/connect-distributed.properties
- $KAFKA_HOME/etc/schema-registry/connect-avro-distributed.properties

Find the file that matches whether you want to serialize using Avro or JSON then copy the
file and call it `worker.properties`. For example: 
```
cp $KAFKA_HOME/etc/kafka/connect-distributed.properties $KAFKA_HOME/etc/kafka/worker.properties
```

Customize the `worker.properties` file to match your Kafka cluster environment.

```
bootstrap.servers=PLAINTEXT://192.168.6.6:9092,PLAINTEXT://192.168.6.7:9092,PLAINTEXT://192.168.6.8:9092

# Change the replication factor to 3 for redundancy 
offset.storage.replication.factor=3
```

## Add additional Connector Plugins (Stand-alone or Cluster mode)
Additional connector plugins can be place in the `$KAFKA_HOME/share/java` directory.

For example, if you wanted to add the Mysql Debezium connector, you would
download the connector here [https://debezium.io/docs/install/]() and unzip the 
archive file into the shared directory mentioned above.  Below are the steps
to add the MySQL Debezium connector plugin:

```
wget https://repo1.maven.org/maven2/io/debezium/debezium-connector-mysql/0.9.3.Final/debezium-connector-mysql-0.9.3.Final-plugin.tar.gz
tar zxvf debezium-connector-mysql-0.9.3.Final-plugin.tar.gz
mv debezium-connector-mysql $KAFKA_HOME/share/java
```

### Example Mysql Debezium property file
There are three ways to load connectors in Kafka Connect. 

>(NOTE: To install Mysql with binlog support into k8s, take a look at this dccumentation:
[https://bitbucket.org/matt-osentoski/howto-public/src/master/kubernetes/intel-nuc-cluster/dynamic-volume-provisioning/examples/mysql-helm-install-with-binlog/]()
It will allow Debezium to integrate with the database as a source connector)

1. (My preferred approach) Use the `confluent` command line tool.
2. You can start Kafka Connect just using just the `worker.properties` file without the 'source' or
'sink' property files.  Then, you would make a REST call to add the connectors:
3. You can load a property file during startup, by passing the property file as an
arguement.

>(NOTE: You can read more at the following URL: [https://docs.confluent.io/current/connect/managing/configuring.html]()

**Option #1**

First, create a config file called `mysql-source-connector-config.json`.
```
{
  "name": "mysql-fhir-source-connector", 
  "config": {
    "connector.class": "io.debezium.connector.mysql.MySqlConnector", 
    "database.hostname": "192.168.6.156",
    "database.port": "3306", 
    "database.user": "root", 
    "database.password": "password", 
    "database.server.id": "223344", 
    "database.server.name": "fhirapi",
    "database.whitelist": "fhirapi",
    "table.whitelist": fhirapi.HFJ_RES_VER",
    "database.history.kafka.bootstrap.servers": "192.168.6.6:9092", 
    "database.history.kafka.topic": "dbhistory.fhir",
    "include.schema.changes": "true" 
  }
}
```

Next, run the following command using this file:

```
## Usage:  confluent load [<connector-name> [-d <connector-config-file>]]
# Below is an example:

confluent load mysql-fhir-source-connector -d mysql-source-connector-config.json
```

**Option #2**

In this example, you would make the following REST call to add the connector, once the kafka
connect server is already running:

```
 curl -X POST -H "Content-Type: application/json" --data \
"{
  "name": "mysql-fhir-source-connector", 
  "config": {
    "connector.class": "io.debezium.connector.mysql.MySqlConnector", 
    "database.hostname": "192.168.6.156",
    "database.port": "3306", 
    "database.user": "root", 
    "database.password": "password", 
    "database.server.id": "223344", 
    "database.server.name": "fhirapi",
    "database.whitelist": "fhirapi",
    "table.whitelist": fhirapi.HFJ_RES_VER",
    "database.history.kafka.bootstrap.servers": "192.168.6.6:9092", 
    "database.history.kafka.topic": "dbhistory.fhir",
    "include.schema.changes": "true" 
  }
}" http://localhost:8083/connectors
```

**Option #3**

The JSON config file created above would be passed into `$KAFKA_HOME/bin/connect-standalone` 
during startup as a 'source' or 'sink' depending on where the data is coming from, or going to.

>(NOTE: This option only works for the standalone version of the server)


## Create Kafka Connect topics
Below are the recommended settings for creating the topics used by Kafka Connect:

```
# config.storage.topic=connect-configs
$KAFKA_HOME/bin/kafka-topics --create --zookeeper 192.168.6.6:2181 --topic connect-configs --replication-factor 3 --partitions 1 --config cleanup.policy=compact

# offset.storage.topic=connect-offsets
$KAFKA_HOME/bin/kafka-topics --create --zookeeper 192.168.6.6:2181 --topic connect-offsets --replication-factor 3 --partitions 50 --config cleanup.policy=compact

# status.storage.topic=connect-status
$KAFKA_HOME/bin/kafka-topics --create --zookeeper 192.168.6.6:2181 --topic connect-status --replication-factor 3 --partitions 10 --config cleanup.policy=compact
```

## Starting Kafka Connect server (Stand-alone mode)
To start the Kafka Connect Server, run the following command:

>(NOTE: The `source` and `sink` configurations will depend on the source and
destination data sources.  Having a Mysql source and Postgres sink would require different
property files)

```
# NOTE: The properties filea could live anywhere on your server, 
# this is ust an example.

$KAFKA_HOME/bin/connect-standalone \
  $KAFKA_HOME/etc/kafka/worker.properties \
  $KAFKA_HOME/etc/kafka/connect-file-source.properties \
  $KAFKA_HOME/etc/kafka/connect-file-sink.properties
```

**OR, using REST to setup connectors**
```
$KAFKA_HOME/bin/connect-standalone $KAFKA_HOME/etc/kafka/worker.properties 
```

## Starting Kafka Connect server (Cluster mode)
```
$KAFKA_HOME/bin/connect-distributed $KAFKA_HOME/etc/kafka/worker.properties
```

## Validate that the connect Server is running
The connect server runs on port `8083` by default.  You can test that the server is
running by gonig to the following URL:

[192.168.6.6:8083](http://192.168.6.6:8083)

This should return version information about the server currently running

## Install the `confluent-hub` command line interface (CLI)
Run the following commands to setup the confluent-hub CLI.  This will make
installing connectors easier:

```
wget http://client.hub.confluent.io/confluent-hub-client-latest.tar.gz
sudo mkdir /opt/confluent-hub
sudo mv confluent-hub-client-latest.tar.gz /opt/confluent-hub
cd /opt/confluent-hub
sudo tar zxvf confluent-hub-client-latest.tar.gz
cd /opt/confluent-hub
sudo chown -R hduser:hadoop confluent-hub
```

### Setup environment variables
```
sudo vi /etc/bash.bashrc
```
Add / modify the following lines:
```
export CONFLUENT_HUB_HOME=/opt/confluent-hub
export PATH=$PATH:$CONFLUENT_HUB_HOME/bin
```

### Load a connector using the CLI
For a list of all available connectors, look at the following page:
[https://www.confluent.io/hub/](https://www.confluent.io/hub/)

Try installing the HDFS connector:
```
## NOTE! The the CLI command should be run from your $KAFKA_HOME directory

cd $KAFKA_HOME
confluent-hub install confluentinc/kafka-connect-hdfs:5.2.1
```

Follow the instructions then restart your Kafka connect server.  Verify 
the connectors were installed using the following URL:

[http://192.168.6.6:8083/connector-plugins](http://192.168.6.6:8083/connector-plugins)

### Recommended Connector plugins
Below are the connector plugins I found helpful:
```
## IMPORTANT!!! Make sure you're in the $KAFKA_HOME directory before running the commands
cd $KAFKA_HOME
confluent-hub install confluentinc/kafka-connect-jdbc:5.2.1
confluent-hub install confluentinc/kafka-connect-avro-converter:5.2.1
confluent-hub install debezium/debezium-connector-mysql:0.9.4
confluent-hub install debezium/debezium-connector-mongodb:0.9.4
confluent-hub install confluentinc/kafka-connect-s3:5.2.1
confluent-hub install nishutayal/kafka-connect-hbase:1.0.0
confluent-hub install confluentinc/kafka-connect-cassandra:1.1.0
confluent-hub install debezium/debezium-connector-postgresql:0.9.4
```

### Setup an example Mysql Sink Connector
First, create a config file called `mysql-source-connector-config.json`. This is
a repeat of the section at the top of this readme, but should flow more smoothly with the Connector install steps:

[https://debezium.io/docs/connectors/mysql/#example-configuration](https://debezium.io/docs/connectors/mysql/#example-configuration)

>(NOTE: To obtain the `database.server.id` below, you can run the following 
query against your mysql database: `SELECT @@server_id`

```
{
  "name": "mysql-fhir-source-connector", 
  "config": {
    "connector.class": "io.debezium.connector.mysql.MySqlConnector", 
    "database.hostname": "192.168.6.156",
    "database.port": "3306", 
    "database.user": "root", 
    "database.password": "password", 
    "database.server.id": "223344", 
    "database.server.name": "fhirapi",
    "database.whitelist": "fhirapi",
    "table.whitelist": "fhirapi.HFJ_RES_VER",
    "database.history.kafka.bootstrap.servers": "192.168.6.6:9092", 
    "database.history.kafka.topic": "dbhistory.fhir",
    "include.schema.changes": "true" 
  }
}
```

Next, run the following command using this file:

>(NOTE: You can also make a REST PUT call to apply the change as well)

```
## Usage:  confluent load [<connector-name> [-d <connector-config-file>]]
# Below is an example:

confluent load mysql-fhir-source-connector -d mysql-source-connector-config.json
```

### Verify that the Sink connector is running:
Run the following commands or access the URLs from a web browser:

```
curl http://192.168.6.6:8083/connectors

## Then based on the connector name:

curl http://192.168.6.6:8083/connectors/mysql-fhir-source-connector/tasks

curl http://192.168.6.6:8083/connectors/mysql-fhir-source-connector/status
```

### Verify the Kafka topic is updating
Run the following command to verify the topic used for the connector is updating
when making database changes:

>(NOTE: The topic name is determined by the `database.server.name` in the config, appended with the
schema and table name.  For example: myName.mySchema.myTable)

```
$KAFKA_HOME/bin/kafka-console-consumer --bootstrap-server 192.168.6.6:9092 \
    --topic fhirapi.fhirapi.HFJ_RES_VER --from-beginning
```

